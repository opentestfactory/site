<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Overview

## Popular guides

<div class="grid cards" markdown>

-   :material-server-network:{ .lg .middle } __Agents__

    You can install _agents_ on any machine, and those agents can be used to run jobs in
    workflows.

    [:octicons-arrow-right-24: Learn more](agent.md)

-   :material-webhook:{ .lg .middle } __Hooks__

    You can customize the steps that run in a workflow, to adjust their behaviors or to
    enrich their results.

    [:octicons-arrow-right-24: Getting started](hooks.md)

-   :material-reflect-vertical:{ .lg .middle } __Inception__

    Sometime, you do not want to run tests, but you want to test your plugins or,
    well, tests, or some parts of them.

    The `inception` special execution environment can be used for this purpose.

    [:octicons-arrow-right-24: Learn more](inception.md)

-   :material-export:{ .lg .middle } __Outputs__

    Your jobs can depend on other jobs, and they can access the outputs produced by
    the job(s) they depends on, through _contexts_.

    [:octicons-arrow-right-24: Learn more](outputs.md)

-   :material-source-repository:{ .lg .middle } __Repositories__

    Do not repeat yourself.
    Workflows allow you to define `repository` resources, that you can share between
    jobs.

    [:octicons-arrow-right-24: Learn more](repositories.md)
</div>

## All guides, by tag

[TAGS]
