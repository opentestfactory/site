---
tags:
  - qualitygate
  - reports
  - usage
---
<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Quality gate

When a workflow is launched within a pipeline, you may need to fine-tune pipeline
failing or succeeding conditions. For instance, you may want a pipeline to fail if
there are less than 50% of successful Selenium tests, or to succeed even if all the API
tests have failed. The quality gate service provides such a possibility.

This service exposes two endpoints, `GET` and `POST`, that allow to evaluate a workflow
execution result _via_ the service level quality gate definition or the definition 
file sent by the user.

To simplify the communication with the quality gate service, two `opentf-ctl`
commands, [`get qualitygate`](../tools/opentf-ctl/qualitygate.md#get-qualitygate-workflow_id) 
and [`describe qualitygate`](../tools/opentf-ctl/qualitygate.md#describe-qualitygate-workflow_id), 
can be used to evaluate a workflow.

!!! tip
    It is also possible to apply a quality gate to a workflow directly from the 
    [`run workflow` command](../tools/opentf-ctl/basic.md#applying-a-quality-gate-to-a-completed-workflow), 
    which may be useful in some contexts. The `run workflow` command supports the same 
    options as the `get qualitygate` command.  The `--mode` option is mandatory for the 
    quality gate to be applied.

## Defining quality gate

A quality gate is a set of rules. Its definition must be a `.yaml` or `.yml` file. 
The quality gate syntax is described in the [Quality gate syntax](../impl/reference/qualitygate-syntax.md) 
chapter.

The definition file may contain several quality gates, but at least one quality gate 
with at least one rule is mandatory. Quality gates have an optional `scope` parameter 
that applies to all quality gate rules. If the quality gate scope is not set, it defaults 
to the scope specified by the [`default_scope`](../services/qualitygate.md#default-scope) parameter.

The rule is defined by three parameters:
`scope`, `threshold` and `failure-status`: two first are mandatory, `failure-status`
defaults to `[failure, blocked, error]` if not set. 

In this example, the definition file contains one quality gate with one rule limiting 
evaluated tests scope to the Cypress tests running on environments containing a `bar` tag.

```yaml
qualitygates:
- name: cypress.quality.gate
  scope: contains(test.runs-on, 'bar')
  rules:
  - name: Cypress tests
    rule:
      scope: test.technology == 'cypress'
      threshold: 75%
      failure-status: [failure, blocked, error]
```

## Using quality gate definition

The quality gate definition may be implemented on the quality gate service level, 
provided at the quality gate definitions path or sent to the `POST` endpoint.

### Service level definition

To use a service level definition, you need to pass the path of a quality gate
definition file to the environment variable `QUALITYGATE_DEFINITIONS` when launching
the orchestrator or the quality gate service.

In this case, to apply a quality gate to a workflow, you have to use its name
as the `mode` parameter value when communicating with the quality gate service.

You may use the `opentf-ctl get qualitygate` command with `--mode` (or `-m`) option:

```shell
opentf-ctl get qualitygate {workflow_id} --mode cypress.quality.gate
```

Or you may send a `GET` request to the endpoint if `opentf-ctl` is not available:

=== "Bash"

    ```bash
    curl http://orchestrator.example.com/workflows/{workflow_id}/qualitygate?mode=cypress.quality.gate \
         -H "Authorization: Bearer ${TOKEN}" 
            
    ```

=== "CMD"

    ```bat
    curl http://orchestrator.example.com/workflows/{workflow_id}/qualitygate?mode=cypress.quality.gate ^
         -H "Authorization: Bearer %TOKEN%" 
            
    ```

=== "PowerShell"

    ```powershell
    curl.exe http://orchestrator.example.com/workflows/{workflow_id}/qualitygate?mode=cypress.quality.gate `
             -H "Authorization: Bearer $Env:TOKEN" 
    ```

### Definition Files Path

Quality gate definitions can be stored in a directory specified by the `definitions_path` 
parameter of the quality gate service. By default, this is set to `/app/qualitygates` when 
using the `allinone` image. The directory can hold multiple definition files, but these have 
a lower priority compared to service-level definitions. Within the directory, definition file 
priority is determined by file name. For more details, refer to ["Default Quality Gates and Definition Files Handling"](../services/qualitygate.md#default-quality-gates-and-definition-files-handling).

To apply these quality gates, you can use the `opentf-ctl get qualitygate` command with 
the `--mode` (or `-m`) option, or send a `GET` request, in the same way as for the 
service-level definitions.

### User definition

User-provided quality gate definition can be directly sent to the service. 
The `mode` parameter value must refer to a quality gate from the posted definition 
(service level definitions are not taken into account in this case).

You may use the `opentf-ctl get qualitygate` command and pass the quality gate definition path
with the `--using` or `-u` option:

```shell
opentf-ctl get qualitygate {workflow_id} --using path/to/cypress_qualitygate.yaml --mode cypress.quality.gate
```

Or you can send to the endpoint a `POST` request with the file attached if `opentf-ctl` is not available:

=== "Bash"

    ```bash hl_lines="3"
    curl -X POST \
            -H "Authorization: Bearer ${TOKEN}" \
            -F qualitygates=@path/to/cypress_qualitygate.yaml \
            http://orchestrator.example.com/workflows/{workflow_id}/qualitygate?mode=cypress.quality.gate
    ```

=== "CMD"

    ```bat hl_lines="3"
    curl -X POST ^
            -H "Authorization: Bearer %TOKEN%" ^
            -F qualitygates=@path/to/cypress_qualitygate.yaml ^
            http://orchestrator.example.com/workflows/{workflow_id}/qualitygate?mode=cypress.quality.gate
    ```

=== "PowerShell"

    ```powershell hl_lines="3"
    curl.exe -X POST `
            -H "Authorization: Bearer $Env:TOKEN" `
            -F qualitygates='@path/to/cypress_qualitygate.yaml' `
            http://orchestrator.example.com/workflows/{workflow_id}/qualitygate?mode=cypress.quality.gate
    ```

## Workflow evaluation

Quality gates apply only to successfully completed workflows with test results.

The completed workflow evaluation is based on the quality gate rules: their scope, 
threshold and failure status list. First, for each rule, the success ratio of tests 
in scope is calculated and then compared to the rule threshold. If the success ratio 
is equal to or greater than the threshold, the rule status is set to `SUCCESS`, 
otherwise, it is set to `FAILURE`. If no test matches the rule scope, the rule 
status is set to `NOTEST`.

Second, the workflow status is evaluated. If there is at least one rule in 
`FAILURE` status, the workflow  status is `FAILURE`.  If there are no failed rules 
and at least one rule in `SUCCESS`, the workflow status is `SUCCESS`. If all rules 
evaluate to `NOTEST`, the workflow status is also `NOTEST`.

### Example of a workflow evaluation

Assuming a successfully completed workflow containing Cypress and JUnit tests, we
want to evaluate its results. All Cypress tests need to be successfully executed
and 25% of JUnit tests may fail. Some JUnit tests fail due to technical errors and 
we don't want to take them into account. Also, we want to take into account only 
tests containing `IHM_` string in their name.

First, we need to create a quality gate evaluating this execution and save it to 
the `cypress_junit_qualitygate.yaml` file.

```yaml
qualitygates:
- name: cypress.junit.quality.gate
  scope: contains(test.name, 'IHM_')
  rules:
  - name: Cypress tests
    rule:
      scope: test.technology == 'cypress'
      threshold: 100%
      failure-status: [failure, blocked, error]
  - name: JUnit tests
    rule:
      scope: test.technology == 'junit'
      threshold: 75%
      failure-status: [failure]
```

Then, supposing the quality gate service is already running, we send this definition
using `get qualitygate` command:

```bash
opentf-ctl get qualitygate a13f0572-b23b-40bc-a6eb-a12429f0143c --using cypress_junit_qualitygate.yaml --mode cypress.junit.quality.gate
```

We will get a response that contains test evaluation results and the general workflow execution
status. The JUnit tests execution result is evaluated as `SUCCESS` even if some tests have failed, 
but the general execution status is `FAILURE`, because Cypress tests did not pass the threshold.

```console
RULE,RESULT,TESTS_IN_SCOPE,TESTS_FAILED,TESTS_PASSED,SUCCESS_RATIO
Cypress tests,FAILURE,20,10,10,50.0%
JUnit tests,SUCCESS,50,10,40,80.0%
Workflow a13f0572-b23b-40bc-a6eb-a12429f0143c failed the quality gate using mode cypress.junit.quality.gate.
```
