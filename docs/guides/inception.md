---
tags:
  - workflows
  - use-cases
  - ci
---
<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Inception

Sometimes, you have processes already in place to execute your tests from your CI, and you would
like to update their statuses in your test case manager instance, but you do not want to
re-run them.

The orchestrator comes with a special execution environment, `inception`, that you can use
for this purpose.

## Example

In this example, you use Squash TM as your test cases manager.  Typically, you use your CI
to run a workflow that uses a generator job that will run the test cases you want and will
update Squash TM with the results.

But you may have processes already in place to run these tests from your CI, and you do not
want to re-run them from the orchestrator.

In this case, you already have the 3 default reports produced by Robot Framework:

- `report.html`
- `output.xml`
- `log.html`

If you have a matching test suite defined in your test cases manager, you can define the
following workflow:

```yaml title=".opentf/workflows/inception.yaml" hl_lines="3-7 9-16 18 19" linenums="1"
metadata:
  name: Publish execution results
resources:
  files:
  - report1
  - report2
  - report3
jobs:
  prepare:
    runs-on: inception
    steps:
    - uses: actions/prepare-inception@v1
      with:
        report.html: ${{ resources.files.report1 }}
        output.xml: ${{ resources.files.report2 }}
        log.html: ${{ resources.files.report3 }}
  robot:
    runs-on: inception
    needs: [prepare]
    generator: tm.squashtest.org/tm.generator@v1
    with:
      squashTMUrl: https://squashtm.example.com/squash
      squashTMAutomatedServerLogin: ${{ variables.SQUASH_USER }}
      squashTMAutomatedServerPassword: ${{ variables.SQUASH_PASSWORD }}
      testPlanUuid: ...
      testPlanType: ...
```

There are 4 parts specific to inception in this workflow.

1.  From lines 3 to 7, the `resources.files` section lists the files that will be used by the
    `prepare-inception` function.  The specified files will have to be sent with the workflow
    request.

2.  From lines 9 to 16, the `prepare` job uses the `actions/prepare-inception@v1` function.
    It makes the specified files available in the 'inception' execution environment.

3.  Line 18: the `robot` job targets the `inception` execution environment.

4.  Line 19: the `robot` job needs the `prepare` job to be executed first.  If this dependency
    is not made explicit, then the `robot` job could be executed before the `prepare` job
    and the files would not be available in the 'inception' execution environment.

And that is about it.

Assuming the above file, you can then run your workflow the usual way:

=== "Bash"

    ```bash
    curl -X POST \
         -H "Authorization: Bearer ${TOKEN}" \
         -F workflow=@.opentf/workflows/inception.yaml \
         -F report1=@report.html \
         -F report2=@output.xml \
         -F report3=@log.html \
         -F variables="SQUASH_USER=${USER}\nSQUASH_PASSWORD=${PASSWD}" \
         https://orchestrator.example.com/workflows
    ```

=== "CMD"

    ```bat
    curl -X POST ^
         -H "Authorization: Bearer %TOKEN%" ^
         -F workflow=@.opentf\workflows\inception.yaml ^
         -F report1=@report.html ^
         -F report2=@output.xml ^
         -F report3=@log.html ^
         -F variables="SQUASH_USER=%USER%\nSQUASH_PASSWORD=%PASSWD%" ^
         https://orchestrator.example.com/workflows
    ```

=== "PowerShell"

    ```powershell
    curl.exe -X POST `
         -H "Authorization: Bearer $Env:TOKEN" `
         -F workflow='@.opentf\workflows\inception.yaml' `
         -F report1='@report.html' `
         -F report2='@output.xml' `
         -F report3='@log.html' `
         -F variables="SQUASH_USER=$Env:USER`nSQUASH_PASSWORD=$Env:PASSWD" `
         https://orchestrator.example.com/workflows
    ```

Or, if you have `opentf-ctl` available in your CI, you can use the following command:

=== "Bash"

    ```bash
    opentf-ctl run workflow .opentf/workflows/inception.yaml \
               -e SQUASH_USER=${USER} \
               -e SQUASH_PASSWORD=${PASSWD} \
               -f report1=report.html \
               -f report2=output.xml \
               -f report3=log.html
    ```
=== "CMD"

    ```bat
    opentf-ctl run workflow .opentf\workflows\inception.yaml ^
               -e SQUASH_USER=%USER% ^
               -e SQUASH_PASSWORD=%PASSWD% ^
               -f report1=report.html ^
               -f report2=output.xml ^
               -f report3=log.html
    ```
=== "PowerShell"

    ```powershell
    opentf-ctl run workflow .opentf\workflows\inception.yaml `
               -e SQUASH_USER=$Env:USER `
               -e SQUASH_PASSWORD=$Env:PASSWD `
               -f report1=report.html \
               -f report2=output.xml \
               -f report3=log.html
    ```

The `-F` or `-f` options are used to specify the files that will be sent with the workflow
request.  The `-e` option is used to specify the variables that will be sent with the workflow
request.

The specified test case statuses will be updated in your test cases manager.

Please refer to "[Integrate with GitHub Actions](github-ci.md)" or  "[Integrate with GitLab CI](gitlab-ci.md)"
or  "[Integrate with Jenkins CI](jenkins-ci.md)" for more information on how to run a workflow
from a CI.

## What does it do?

The `inception` execution environment does the following things:

- It accepts all `ExecutionCommand` requests, as long as they carry the `inception` tag.  This
  differs from the usual channel plugins: they only accept `ExecutionCommand` requests that they
  can fully satisfy.

- It does not execute the `ExecutionCommand` statements.  It always returns a success status.
  But it catches the `::attach::` workflow commands and attaches the prepared files to the
  `ExecutionResult` it publishes.

- It outputs the commands it receives.

[If you were to have another execution environment that provides that `inception` tag, which
would be a very bad idea, and if the arranger decides to send the `ExecutionCommand` to that
environment, you would get the usual behavior, not the one described here.]

## Limitations

There can be any number of workflows that may use it at any given time, but, for a given workflow,
it cannot be used in parallel.

If the `::attach::` workflow commands are the result of environment-side execution, they will not
be correctly caught if they are complex or guarded.

While the test case statuses are correctly updated, some attachments may be duplicated amongst
the test cases in your test cases manager.
