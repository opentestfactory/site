---
tags:
  - workflows
  - namespaces
  - permissions
---
<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Namespaces and permissions

Workflow jobs run on execution environments.

_tags_ are used to describe execution environment capabilities.  For example, the `linux` and
`robotframework` tags may be attached to an execution environment that handles Robot Framework
workloads.

You may add tags to further describe the execution environment, such as `debian10`, `safari`,
or `aws_qa2`, but tags are only used in a 'positive' way: a job can run on any execution environment
whose tags match the requested capabilities.  A job that requests `linux` can run on any execution
environment that has this tag.

Sometimes you want to limit some execution environments to some workflows.  It can be that you
have a `preprod` environment and a `noprod` environment, and they should not be mixed, or that
you share the orchestrator instance with other departments within your company.

_Namespaces_ are used to provide this functionality.

Sometimes you want to further control access to resources.  You may want to give one of your
users read-only access to your running workflows, or you may want to prevent another one from
registering new agents.

_Access control_ grants permission to access resources.

## What is a namespace?

A namespace has a name (letters, digits, and hyphens are allowed).  It ties resources such
as workflows, execution environments, and authentication tokens.

There is no limit on the number of namespaces you can use in an orchestrator instance.

There is a default namespace, `default`, which is used if you do not specify a specific namespace.

## Using namespaces

!!! Info
    Namespaces are not enabled by default, or, more precisely, there is only one namespace
    that can be used by default, the `default` namespace.

    Please check with your administrator if you can use this feature on your orchestator
    instance. For more information, see "[enable namespaces on an orchestrator instance](../admin/access-authn-authz/enable-namespaces.md)."

### Workflows

A workflow belongs to one (and only one) namespace.  This can be defined by using the `namespace` metadata:

```yaml
metadata:
  name: my workflow
  namespace: a-namespace
jobs:
  ...
```

If no namespace is specified, the `default` namespace is assumed.

### Execution environments

Execution environments are tied to at least one namespace.  They may be accessible from more than one
namespace, or from all namespaces.

For example, if you want to make an execution environment that is accessible from the `namespace-a`
and `namespace-b` namespaces, if you are using agents, you will do something like:

```shell
opentf-agent ... --namespaces=namespace-a,namespace-b
```

If you are using SSH-based execution environments, in your environment description you will write
something like:

```yaml
pools:
  my_targets:
  - host: host.example.com
    port: 22
    username: alice
    ssh_host_keys: /data/ssh/known_hosts
    key_filename: /data/ssh/example.pem
    missing_host_key_policy: reject
    tags: [ssh, linux]
    namespaces: namespace-a,namespace-b
```

If you want to make an execution environment accessible from all namespaces, tie it to `"*"`.

If you do not specify namespaces for execution environments, they are only accessible from the `default` namespace.

### Authentication tokens

Authentication tokens that are used to interact with an orchestrator instance are also tied to namespaces.

By default, they are tied to the `default` namespace.  They may be tied to more than one namespace, or
they can have access to all namespaces.

Authentication tokens cannot be used to interact with resources that belong to a namespace they are not tied to.

For example, if your authentication token is tied to the `default` and `namespace-a` namespaces, you will be
able to run workflows in those namespaces, but not in the `namespace-b` namespace.

Similarly, when querying the available execution environments, you will only see the execution environments that
are accessible from those two namespaces.  If an execution environment is only accessible from `namespace-b`, you
will not see it.

## Permissions

!!! Info
    Access control is not enabled by default, or, more precisely, an authentication token gives access to all resources
    that belong to the namespace(s) it is tied to.

    Please check with your administrator if you can use this feature on your orchestator
    instance. For more information, see "[enable namespaces on an orchestrator instance](../admin/access-authn-authz/enable-namespaces.md)."

The main resources an orchestrator instance deals with are _workflows_, _agents_, _quality gates_, and _channels_ (execution
environments, agent- or SSH-based).

You have either no permission to access a given resource, permission to read a given resource, or full access to a
given resource.

Access control is based on policies that grant
permission to access resources.  If no policy applies, access is denied.

Permission to access a resource can be tied to a namespace, or can apply to all namespaces.

If access control is enabled on your instance, specific permissions will be associated with your
authentication tokens.

If you attempt an interaction that you have no permission for, you will receive a 'Forbidden' response.  If you think you
should have access, please check with your administrator.

There is no need to generate a new authentication token if the associated permissions change.

## Next steps

Here are some helpful resources for taking your next steps with authentication and authorization:

- ["Authentication"](../admin/access-authn-authz/authentication.md) for an in-depth view of authentication
- ["Authorization"](../admin/access-authn-authz/authorization.md) for more details about configuring authorizations
