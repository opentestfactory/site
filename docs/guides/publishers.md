<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Publishing Results

Sometimes, you may need to export the workflow execution results.  The orchestrator includes
three publisher plugins that allow to upload reports, attachments, and quality gate
results locally or to external resources: the tracker publisher, the local publisher, and
the S3 publisher.

## Tracker Publisher

The tracker publisher plugin allows you to apply the quality gate to the successfully
completed workflow and publish the quality gate results to an issue tracker.  Presently, it
supports publishing to the GitLab issues and merge requests.

The tracker publisher only handles workflows with specific labels in their metadata and
needs a tracker instances file to run.

### Instances File

To set up the tracker publisher plugin, you need to create an 'instances' configuration file.
It is a YAML file containing at least one instance and a default instance name.  The
instances file structure is described in detail under
[Tracker publisher configuration](../services/trackerpublisher.md#tracker-instances-file)

A tracker instance is defined by four mandatory parameters: `name`, `type`, 
`keep-history` and `endpoint`. It can also include `token` and `default-qualitygate` 
parameters.

In this example, the instances file contains one instance with an authentication 
token and default quality gate to apply to the workflow results. This instance 
is a GitLab instance (currently the only supported instance type). When using this 
instance to post the quality gate results, the most recent results for a given 
quality gate will be updated, as the `keep-history` parameter is set to `false`.

```yaml
default-instance: main.instance

instances:
- name: main.instance
  type: gitlab
  endpoint: http://example.com
  keep-history: false
  token: gl-xYz3920Nd9191
  default-qualitygate: my.quality.gate
```

To use the tracker publisher plugin, you need to pass your instances file path to the
environment variable `TRACKERPUBLISHER_INSTANCES` when launching the orchestrator or tracker
publisher plugin.

### Workflow Labels

If you want the workflow execution results to be handled by the tracker publisher, you must
add some tracker-specific labels under `metadata.labels` section.  The following labels are
recognized:

|Label                |Value                                                               |Required                           |
|---------------------|--------------------------------------------------------------------|-----------------------------------|
|`gitlab/project-id`  |ID of the related project.                                          |If `project-path` is not specified.|
|`gitlab/project-path`|Path of the related project.                                        |If `project-id` is not specified.  |
|`gitlab/issue-iid`   |IID of the issue the results will be published to.                  |If `mr-iid` is not specified.      |
|`gitlab/mr-iid`      |IID of the merge request the results will be published to.          |If `issue-iid` is not specified.   |
|`gitlab/keep-history`|If `true`, the previous results for a given quality gate are kept.  |Yes                                |
|`gitlab/label`       |Prefix of the label to add to the publication target.               |No                                 |
|`gitlab/instance`    |Name of the instance from the instances file.                       |No                                 |
|`qualitygate-mode`   |Quality gate to apply to the workflow.                              |No                                 |

`gitlab/project-id` or `gitlab/project-path` and `gitlab/issue-iid` or `gitlab/mr-iid` 
are mandatory. If `gitlab/instance` is not set, the `default-instance` from the 
tracker instances file will be used. You may also explicitly pass the value 
`default` to the `gitlab/instance` label.

You can use both `gitlab/issue-iid` and `gitlab/mr-iid` labels if you want the 
quality gate results to be simultaneously published to an issue and a merge request.

`gitlab/label` allows to add a label containing the quality gate status to the 
publication target. You need first to create the labels `{prefix}:Passed`, 
`{prefix}:Failed` and `{prefix}:No test` in the related GitLab project, then pass 
the `{prefix}` value to the `gitlab/label`.

`qualitygate-mode` is the name of the quality gate to apply.  If not set, the default
quality gate of the defined instance (from the 'instances' file) will be used.  If the
defined instance does not have a quality gate defined, it will default to `strict`.

### Example of a GitLab Publisher Handled Workflow Labels

The workflow with those labels in metadata will be handled by GitLab publisher 
when successfully completed. First the quality gate `daily.quality.gate` will 
be applied, then its results will be published to the specified GitLab instance, 
project and issue, updating the most recent `daily.quality.gate` results. 
(Please note that the `issue-iid`, `mr-iid` and `keep-history` values needs to be 
quoted to be correctly interpreted by the plugin.)

```yaml
metadata:
  [...]
  labels:
    gitlab/project-id: '27398247' # or `gitlab/project-path: my/project/path`
    gitlab/issue-iid: '45'
    gitlab/instance: main.instance # or `gitlab/instance: default`
    gitlab/keep-history: 'false'
    qualitygate-mode: daily.quality.gate
```

## Local Publisher

The local publisher plugin uploads reports and attachments to a target directory,
which may be mounted.

### Enabling the Plugin

The local publisher plugin is disabled by default, as it requires you to define a target
directory.

To enable the plugin, you need to edit the orchestrator instance configuration file
`/app/squashtf.yaml`:

```yaml title="/app/squashtf.yaml" hl_lines="10"
eventbus: python3 -m opentf.core.eventbus
services:
- ${{ CORE }}/core
plugins:
- ${{ CORE }}/plugins
- /app/plugins
disabled:
- dummyee
- HelloWorld
- localpublisher
```

Just remove `localpublisher` from the disabled services list and save the file.  Before
relaunching the services, you must also define a target directory to which the plugin will
upload reports and attachments.

### Defining a Target Directory

To define a target directory for the Local publisher plugin, open its configuration file
(`localpublisher.yaml` by default) and change the value of the `target` entry:

```yaml hl_lines="13"
[...]
- context:
    port: 7783
    host: 127.0.0.1
    ssl_context: disabled
    trusted_authorities:
    - /etc/squashtf/*
    logfile: localpublisher.log
    enable_insecure_login: true
    eventbus:
      endpoint: http://127.0.0.1:38368
      token: reuse
    target: /app/data # change to /path/to/your/target/directory
  name: allinone
[...]
```

Restart the orchestrator.  Workflow attachments will now be stored in the defined 
directory, under a `{workflow_id}/{filename}` key.

This directory is typically a mount point, linked to an external volume.

## S3 Publisher

The S3 publisher plugin uploads reports and attachments to any S3-compatible bucket.

It is enabled by default but will do nothing until you define S3 credentials and a target
bucket.

### Defining Target Bucket and S3 Credentials

To define a target bucket that will be used by the S3 publisher, as well as S3 credentials
file path, you need to modify the plugin configuration file: `s3publisher.yaml` by default.

In this file, you need to change the values of two entries: `bucket` and `s3credentials`:

```yaml hl_lines="13-14"
[...]
- context:
    port: 7787
    host: 127.0.0.1
    ssl_context: disabled
    trusted_authorities:
    - /etc/squashtf/*
    logfile: s3publisher.log
    enable_insecure_login: true
    eventbus:
      endpoint: http://127.0.0.1:38368
      token: reuse
    bucket: example
    s3credentials: /app/s3publisher/s3credentials.json
  name: allinone
[...]
```

Replace the `bucket` entry value with the target bucket name.  Attachments will be 
stored in this bucket under a `{workflow_id}/{filename}` key.

Replace the `s3credentials` entry value with the S3 credentials file path. This file 
must contain 4 entries: `region_name`, `endpoint_url`, `aws_access_key_id` and 
`aws_secret_access_key`.

```json
{
  "region_name": "fr-par",
  "endpoint_url": "https://s3.example.com",
  "aws_access_key_id": "my_access_key_id",
  "aws_secret_access_key": "my_secret_access_key"
}
```

Restart the plugin. If you have provided the proper set of credentials, the workflow 
attachments will now be stored in the specified bucket. If the credentials file 
changes, the plugin will detect the changes: there is no need to restart it.

## Next Steps

Here are some helpful resources for taking your next steps with publishing your workflow
execution results:

- "[Tracker publisher](../services/trackerpublisher.md)" for more information on how to configure the tracker publisher plugin
- "[Local publisher](../services/localpublisher.md)" for more information on how to configure the local publisher plugin
- "[S3 publisher](../services/s3publisher.md)" for more information on how to configure the S3 publisher plugin
