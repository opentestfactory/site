<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Overview

Providers are plugins that provide sets of _functions_ to interact with a specific service or tool.

A function is something you can call from a workflow that will perform an operation on the
service or tool it is designed for.

For example:

```yaml
- name: "Checkout code"
  uses: "actions/checkout@v2"
  with:
    repository: "https://gitlab.com/opentestfactory/otf.git"
    ref: "main"
```

A provider transforms this function in a series of steps that will be executed by the execution environment.

For example:

```yaml
- run: |
    git clone https://gitlab.com/opentestfactory/otf.git
    git checkout main
```

The OpenTestFactory orchestrator includes a set of providers for common services and tools, and
you can write your own provider to extend the list of supported services and tools.

[Writing your own provider.](../plugins/creating-a-provider-plugin.md)

## Popular providers

<div class="grid cards" markdown>

-   :material-function: __Actions__

    ---

    A set of common functions (git checkout, platform-independent file management operations, ...)

    [:octicons-arrow-right-24: Getting started](actionprovider.md)

</div>

## Testing framework providers

A set of providers for use by generator plugins, that can also be used in your own workflows
to run test suites from multiple testing frameworks.

They provide a pair of functions, `execute` and `params`, that are normalized and used by
generators, and at least one specific function that you can use to run a test suite.

<div class="grid cards" markdown>

-   :simple-cucumber: __Cucumber JVM__

    ---

    A testing tool supporting behavior-driven development (BDD). Embeds an ordinary language
    parser called Gherkin.

    [:octicons-arrow-right-24: Getting started](cucumber.md)

-   :simple-cypress: __Cypress__

    ---

    A JavaScript end to end testing framework.

    [:octicons-arrow-right-24: Getting started](cypress.md)

-   :simple-junit5: __JUnit__

    ---

    A programmer-friendly testing framework for Java and the JVM.

    [:octicons-arrow-right-24: Getting started](junit.md)

-   :material-drama-masks: __Playwright__

    ---

    Playwright enables reliable end-to-end testing for modern web apps.

    [:octicons-arrow-right-24: Getting started](playwright.md)

-   :simple-postman: __Postman__

    ---

    An automated testing tool for REST APIs.

    [:octicons-arrow-right-24: Getting started](postman.md)

-   :simple-robotframework: __Robot Framework__

    ---

    A generic open source automation framework for acceptance testing, <abbr title="Acceptance Test Driven
    Development">ATDD</abbr>, and <abbr title="Robotic Process Automation">RPA</abbr>.

    [:octicons-arrow-right-24: Getting started](robotframework.md)

-   :simple-framework: __Squash Keyword Framework (SKF)__

    ---

    A keyword oriented test automation framework. Includes a DSL for writing automated tests
    scripts.
    
    [:octicons-arrow-right-24: Getting started](skf.md)
    
-   :octicons-gear-24: __SoapUI__

    ---

    An automated testing tool for SOAP and REST APIs.

    [:octicons-arrow-right-24: Getting started](soapui.md)

</div>
