<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Robot Framework

This plugin provides functions that handle Robot Framework tests.  It has been
validated with Robot Framework 4.0 and should work with any recent version of Robot Framework.

It can be used directly in a workflow, or indirectly via generators (such as those
providing access to test case managers).

A working Robot Framework installation must be available in the targeted execution environments.

The functions have a `robotframework` category prefix.

!!! information
    If you intend to use the `robotframework/params@v1` function, either directly or
    indirectly via a generator, you can install the [`squash-tf-services`](https://pypi.org/project/squash-tf-services/){ target="_blank" } package on the
    execution environment.

    If you intend to generate Allure reports, the [`allure-robotframework`](https://pypi.org/project/allure-robotframework/){ target="_blank" } package must
    also be installed on the execution environment.


## Functions

<p> <a href="#robotframeworkrobotv1" title="robotframework/robot@v1"><i data-feather="play"></i> </a> <a href="#robotframeworkexecutev1" title="robotframework/execute@v1"><i data-feather="play-circle" stroke="red"></i> </a> <a href="#robotframeworkparamsv1" title="robotframework/params@v1"><i data-feather="plus-circle" stroke="red"></i> </a>  </p>

### <i data-feather="play"></i> robotframework/robot@v1

Run a Robot Framework test suite.

#### Inputs

The function has the following inputs:

- `datasource` (required)

    The data source to use.

- `test` (optional)

    Specify a test case present in the data source.  By default, all test
    cases in the data source are executed.

- `reports-for-allure` (optional)

    A boolean.  Set to `true` to enable the generation of Allure reports.
    By default, Allure reports are not generated.

- `extra-options` (optional)

    Specify additional parameters to pass to Robot Framework.
    
    There is a list of possible command line options you can find on the
    <a href="https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#command-line-options" target="_blank">Command line options</a>
    chapter of the Robot Framework documentation.

#### Reports

The function generates the following reports:

- `output.xml`

    The Robot Framework test execution report in XML.
    
    It has the `application/vnd.opentestfactory.robotframework-output+xml`
    content type.

- `RobotFramework_reports.tar`

    A TAR archive. Contains the usual reports:
    
    -  `log.html`
    -  `report.html`
    
    and the screenshots:
    
    -  `*.png`
    -  `*.jpg`
    -  `*.jpeg`
    -  `browser/screenshot/*.png`
    -  `browser/screenshot/*.jpg`
    -  `browser/screenshot/*.jpeg`

- `*-result.json`

    JSON result files.
    
    Contains all `*-result.json` files, when the optional `reports-for-allure` input is `true`.
    Those files enable the generation of Allure reports.

- `*-attachment.html`

    HTML attachment files.
    
    Contains all `*-attachment.html` files, when the optional `reports-for-allure` input is
    `true`.  Those files enable the generation of Allure reports.

#### Examples

This first example runs all tests in the `foobar` test suite:

```yaml
- uses: robotframework/robot@v1
  with:
    datasource: foobar
```

This second example runs the `foo` test in the `foobar` test suite,
and an Allure report will be generated:

```yaml
- uses: robotframework/robot@v1
  with:
    datasource: foobar
    test: foo
    reports-for-allure: true
```
This third example runs all tests having a `ready` tag in the `foobar` test suite:

```yaml
- uses: robotframework/robot@v1
  with:
    datasource: foobar
    extra-options: --include ready
```

### <i data-feather="play-circle" stroke="red"></i> robotframework/execute@v1

An `execute` function for use by generators.  Runs a test suite or a
test case in a test suite.

#### Test Reference format

The test reference format used by `robotframework/execute@v1` is as follows:

- `{project}/{datasource}[#{testcase}]`

With:

- `{project}` (required): name of the project on the source code repository.
- `{datasource}` (required): path and name of the Robot Framework test, from the root of
    the project (with the `.robot` extension).

    It is possible to not indicate a `.robot` file name and, instead, only define the name
    of a folder. In this case, all the `.robot` files of this folder and its sub-folders
    will be executed.

- `{testcase}` (optional): name of the test case to execute in the `.robot` file(s).

#### Inputs

The function has the following inputs:

- `test` (required)

    The test reference.

#### Reports

The function generates the following reports:

- `output.xml`

    The Robot Framework test execution report in XML.
    
    It has the `application/vnd.opentestfactory.robotframework-output+xml`
    content type.

- `RobotFramework_reports.tar`

    A TAR archive. Contains the usual reports:
    
    -  `log.html`
    -  `report.html`
    
    and the screenshots:
    
    -  `*.png`
    -  `*.jpg`
    -  `*.jpeg`
    -  `browser/screenshot/*.png`
    -  `browser/screenshot/*.jpg`
    -  `browser/screenshot/*.jpeg`

- `*-result.json`

    JSON result files.
    
    Contains all `*-result.json` files if the `allure-robotframework` library is
    available on the execution environment, to enable the generation of Allure reports.

- `*-attachment.html`

    HTML attachment files.
    
    Contains all `*-attachment.html` files if the `allure-robotframework` library is
    available on the execution environment, to enable the generation of Allure reports.

#### Customization

The `ROBOTFRAMEWORK_EXTRA_OPTIONS` environment variable can be used to pass additional
parameters to the `robot` command.

If defined it will be appended to the end of command line.

The following parameters are used in the provider-generated `robot` command:

```bash
robot \
  $ROBOTFRAMEWORK_EXTRA_OPTIONS \
  --nostatusrc --listener "allure_robotframework;." \
  --test {testcase_name} \
  "{datasource_path}"
```

You must avoid passing, _via_ the `ROBOTFRAMEWORK_EXTRA_OPTIONS` variable, the command line
parameters that conflict with the parameters already used, or the parameters
that impact the generation or alter the path of the reports expected by the orchestrator
(view ["Reports" section](#reports_1)).

#### Example

```yaml
- uses: robotframework/execute@v1
  with:
    test: foobar
```

### <i data-feather="plus-circle" stroke="red"></i> robotframework/params@v1

A `params` function for use by generators.

#### Inputs

The function has the following inputs:

- `data` (required)

    The data to use for the automated test.

- `format` (required)

    The format to use for the automated test data.
    
    `format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

#### Example

```yaml
- uses: robotframework/params@v1
  with:
    data:
      global:
        key1: value1
        key2: value2
      test:
        key1: value3
        key3: value4
    format: format
```

`format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

`data` can have two keys:

- `global` for defining global parameters.
- `test` for defining test parameters.

<script src="../../js/feather.min.js"></script>
<script>feather.replace()</script>

## Using with inception

Please refer to "[Inception](https://opentestfactory.org/guides/inception.html)" for
more information on what inception is.

Preload the inception environment with at least the test execution report data.

### Example

```yaml title="my_workflow.yaml"
metadata:
  name: Robot Framework Inception
resources:
  files:
  - report1
  - report2
jobs:
  my_non_specific_job:
    runs-on: inception
    steps:
    - uses: actions/prepare-inception@v1
      with:
        output.xml: ${{ resources.files.report1 }}
    - uses: robotframework/execute@v1
      with:
        test: OpenWeather/tests/robot
  my_specific_job:
    runs-on: inception
    steps:
    - uses: actions/prepare-inception@v1
      with:
        output.xml: ${{ resources.files.report2 }}
    - uses: robotframework/execute@v1
      with:
        test: OpenWeather/tests.robot#a specific test
```

You can use the following command to run it:

=== "Bash"

    ```bash
    opentf-ctl \
        run workflow my_workflow.yaml \
        -f report1=output_1.xml \
        -f report2=output_2.xml
    ```

=== "CMD"

    ```bat
    opentf-ctl ^
        run workflow my_workflow.yaml ^
        -f report1=output_1.xml ^
        -f report2=output_2.xml
    ```

=== "PowerShell"

    ```powershell
    opentf-ctl `
        run workflow my_workflow.yaml `
        -f report1=output_1.xml `
        -f report2=output_2.xml
    ```

## Configuration

[Hooks](../guides/hooks.md) can be defined for the provided functions. This can be done in workflow definitions or at the orchestrator level so that they apply to all your workflows.

Configuration at the orchestrator level is done by setting the
`ROBOTFRAMEWORK_PROVIDER_HOOKS` environment variable.

Please refer to "[Common Provider Settings](../plugins/configuring-a-provider-plugin.md)" for more information.


