<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Actions

This plugin provides common functions that can be used on any execution environment.

They cover the following areas:

- `git` commands
- files commands
- inception commands

The functions have an `actions` category prefix.


## Functions

<p> <a href="#actionscheckoutv2" title="actions/checkout@v2"><i data-feather="arrow-down"></i> </a> <a href="#actionscheckoutv3" title="actions/checkout@v3"><i data-feather="arrow-down"></i> </a> <a href="#actionscreate-filev1" title="actions/create-file@v1"><i data-feather="file-plus"></i> </a> <a href="#actionsput-filev1" title="actions/put-file@v1"><i data-feather="file-text"></i> </a> <a href="#actionstouch-filev1" title="actions/touch-file@v1"><i data-feather="file"></i> </a> <a href="#actionsdelete-filev1" title="actions/delete-file@v1"><i data-feather="file-minus"></i> </a> <a href="#actionscreate-archivev1" title="actions/create-archive@v1"><i data-feather="archive"></i> </a> <a href="#actionsget-filev1" title="actions/get-file@v1"><i data-feather="arrow-up"></i> </a> <a href="#actionsget-filesv1" title="actions/get-files@v1"><i data-feather="upload"></i> </a> <a href="#actionsupload-artifactv1" title="actions/upload-artifact@v1"><i data-feather="arrow-up"></i> </a> <a href="#actionsdownload-artifactv1" title="actions/download-artifact@v1"><i data-feather="arrow-up"></i> </a> <a href="#actionsprepare-inceptionv1" title="actions/prepare-inception@v1"><i data-feather="film"></i> </a>  </p>

### <i data-feather="arrow-down"></i> actions/checkout@v2

Checkout a repository at a particular version.

`checkout` functions have a mandatory `repository` input:

```yaml
- uses: actions/checkout@v2
  with:
    repository: https://github.com/robotframework/RobotDemo.git
```

They also allow for two optional inputs:

- `ref`, which is the git reference to checkout: a branch name, a
  tag name, or a commit SHA (defaults to the default branch if
  unspecified). You must provide a complete commit SHA (40 characters for SHA-1
  or 64 characters for SHA-256).
- `path`, which is where to clone/checkout the repository, relative
  to the current workspace.

If the `repository` input contains a password or an authorization token, it
will be masked in the console logs output.

```yaml
- uses: actions/checkout@v2
  with:
    repository: https://github.com/robotframework/RobotDemo.git
    ref: dev
    path: foo/bar
```

#### Inputs

The function has the following inputs:

- `repository` (required)

    the repository to clone

- `ref` (optional)

    the branch, tag, or SHA to checkout

- `path` (optional)

    where to clone the repository

### <i data-feather="arrow-down"></i> actions/checkout@v3

Checkout a repository at a particular version. Compared to `actions/checkout@v2`,
`actions/checkout@v3` allows additionally to indicate the number of commits to fetch.

`checkout` functions have a mandatory `repository` input:

```yaml
- uses: actions/checkout@v3
  with:
    repository: https://github.com/robotframework/RobotDemo.git
```

They also allow for three optional inputs:

- `ref`, which is the git reference to checkout: a branch name, a
  tag name, or a commit SHA (defaults to the default branch if
  unspecified). You must provide a complete commit SHA (40 characters for SHA-1
  or 64 characters for SHA-256).
- `path`, which is where to clone/checkout the repository, relative
  to the current workspace.
- `fetch-depth`, which indicates the number of commits to fetch. The default
  value is `1`. To fetch all history, set the value to `0`.

If the git reference to check out is a commit SHA, you must ensure that
a correct `fetch-depth` value is used to fetch the commit, or set it to `0`.

When the `ref` parameter is unspecified, history is fetched for all the
branches at a given depth (1 by default).

If the `repository` input contains a password or an authorization token, it
will be masked in the console logs output.

#### Inputs

The function has the following inputs:

- `repository` (required)

    the repository to clone

- `ref` (optional)

    the branch, tag, or SHA to checkout

- `path` (optional)

    where to clone the repository

- `fetch-depth` (optional)

    number of commits to fetch

#### Examples

In this example, only the two last commits on `dev` branch are fetched
into `foo/bar`.

```yaml
- uses: actions/checkout@v3
  with:
    repository: https://github.com/robotframework/RobotDemo.git
    ref: dev
    path: foo/bar
    fetch-depth: 2
```

In this example, only the last commit on `v22.1` branch is fetched.

```yaml
- uses: actions/checkout@v3
  with:
    repository: https://github.com/robotframework/RobotDemo.git
    ref: v22.1
```

### <i data-feather="file-plus"></i> actions/create-file@v1

Create a file on the execution environment.

#### Inputs

The function has the following inputs:

- `data` (required)

    the file content

- `format` (required)

    the file format, one of `ini`, `json`, `txt`, or `yaml`

- `path` (required)

    the file location, relative to the current workspace

#### Example

```yaml
- uses: actions/create-file@v1
  with:
    data:
      foo:
        key1: value1
        key2: value2
      bar:
        key1: value1
        key2: value2
    format: ini
    path: foobar.ini
```

This will create a `foobar.ini` file with the following content:

```ini
[foo]
key1=value1
key2=value2
[bar]
key1=value1
key2=value2
```

### <i data-feather="file-text"></i> actions/put-file@v1

Put an existing file on the execution environment.

#### Inputs

The function has the following inputs:

- `file` (required)

    the file to upload, which must be an entry in the `resources.files` part of
    the workflow

- `path` (required)

    the file destination, relative to the current workspace

#### Examples

Put a file in the current directory in an execution environment:

```yaml
- uses: actions/put-file@v1
  with:
    file: file-to-put
    path: destination-path
```

If you need to put a file somewhere other than the current directory,
use the `working-directory` statement:

```yaml hl_lines="5"
- uses: actions/put-file@v1
  with:
    file: file-to-put
    path: destination-path
  working-directory: /foo/bar
```

If you want to put a file in the execution environment from a workflow, the file
must be declared as a workflow resource:

```yaml hl_lines="3-5"
metadata:
  name: put-file example
resources:
  files:
  - foo.json
jobs:
  keyword-driven:
    runs-on: ssh
    steps:
    - uses: actions/put-file@v1
      with:
        file: foo.json
        path: bar/baz.json
      working-directory: /qux/quux/corge
```

This example can be run with the following `cURL` command:

```bash
curl -X POST \
     -F workflow=@{my-workflow.yaml} \
     -F foo.json=@foo.json \
     -H "Authorization: Bearer {my-token}" \
     http://example.com/workflows
```

If the referred file is not part of the `resources` section of your workflow file,
the step will fail with an error code 2.

### <i data-feather="file"></i> actions/touch-file@v1

Ensure a file exists on the execution environment.

If the file does not exist, it will be created (with an empty content).

#### Inputs

The function has the following inputs:

- `path` (required)

    the file location, relative to the current workspace

#### Example

```yaml
- uses: actions/touch-file@v1
  with:
    path: foobar.ini
```

### <i data-feather="file-minus"></i> actions/delete-file@v1

Delete a file on the execution environment.

#### Inputs

The function has the following inputs:

- `path` (required)

    the file location, relative to the current workspace

#### Example

```yaml
- uses: actions/delete-file@v1
  with:
    path: foobar.ini
```

### <i data-feather="archive"></i> actions/create-archive@v1

Create an archive from a set of selected files or directories on the execution environment.

`create-archive` functions create an archive from selected files or directories.
They have mandatory `path` and `patterns` inputs.

If no files or directories match the specified patterns, an error message is issued,
no archive is created, and the workflow is stopped. This is the default behavior.

An optional `warn-if-not-found` input can be specified.
Instead of the default behavior, if the specified patterns do not match any files,
a warning message is issued, an archive containing an empty file `intentionnally_empty_archive`
is created, and the workflow continues.

An optional `format` input may be specified.
Possible archive formats are `tar` or `tar.gz`. The default format is `tar.gz`.

#### Inputs

The function has the following inputs:

- `path` (required)

    The archive name, relative to the current working directory.

- `patterns` (required)

    A list of directory or file name patterns, possibly with paths.
    They may contain placeholders (`*` or `**`).
    The `*` character in a sub-directory is not supported in the Windows environment.

- `format` (optional)

    The archive format, either `tar` or `tar.gz` if specified, defaulting to `tar.gz` if not specified.

- `warn-if-not-found` (optional)

    A message to display when no files matching one of the patterns are found.
    If specified, enables the generation of an empty archive.
    May also be set to `true`, in this case, the default warning message is logged.

#### Examples

```yaml
- uses: actions/create-archive@v1
  with:
    path: myarchive.tar.gz
    patterns:
    - 'screenshots/*.svg'
    - bar/
    - foo.html
    - '*.png'
```

If you want to specify a `tar` format to create the archive instead of the `tar.gz`
default format, use the `format` input:

```yaml hl_lines="4"
- uses: actions/create-archive@v1
  with:
    path: myarchive.tar
    format: tar
    patterns:
    - 'data/foo/screenshots/*.svg'
    - data/foo/bar/
    - data/foo/foo.html
    - 'data/foo/*.png'
```

If you need to archive files that are not in the current directory,
use the `working-directory` statement:

```yaml hl_lines="10"
- uses: actions/create-archive@v1
  with:
    path: myarchive.tar.gz
    format: tar.gz
    patterns:
    - 'screenshots/*.svg'
    - bar/
    - foo.html
    - '*.png'
  working-directory: /data/foo
```

If you want to archive files recursively in a directory tree, use the
`**/*.ext` pattern format:

```yaml hl_lines="5"
- uses: actions/create-archive@v1
  with:
    path: myarchive.tar.gz
    patterns:
    - '**/*.png'
    - 'screenshots/*.svg'
    - bar/
    - foo.html
```

If you want to display a warning message when no files matching one of the patterns are found,
use the `warn-if-not-found` input. This allows to create an empty archive:

```yaml hl_lines="9"
- uses: actions/create-archive@v1
  with:
    path: myarchive.tar.gz
    patterns:
    - 'screenshots/*.svg'
    - bar/
    - foo.html
    - '*.png'
    warn-if-not-found: "No files to archive found, an empty archive will be created."
```

If you want to create an empty archive and display a default warning message
when no files matching one of the patterns are found, set the `warn-if-not-found`
input to `true`.

```yaml hl_lines="9"
- uses: actions/create-archive@v1
  with:
    path: myarchive.tar.gz
    patterns:
    - 'screenshots/*.svg'
    - bar/
    - foo.html
    - '*.png'
    warn-if-not-found: true
```

If you want to archive all present files and sub-directories recursively in the working directory
`/data/foo`, use the `**/*` pattern format:

```yaml hl_lines="5"
- uses: actions/create-archive@v1
  with:
    path: myarchive.tar.gz
    patterns:
    - '**/*'
  working-directory: /data/foo
```

### <i data-feather="arrow-up"></i> actions/get-file@v1

Attach a file from the execution environment.

`get-file` functions attach the specified file so that publisher
plugins can process them.  They have a mandatory `path` input and
an optional `type` input.

#### Inputs

The function has the following inputs:

- `path` (required)

    the file to attach

- `type` (optional)

    the file type (a media type such as `application/xml`)

#### Examples

```yaml
- uses: actions/get-file@v1
  with:
    path: 'foo.xml'
```

It is a good practice to specify the attachment type, if known:

```yaml hl_lines="4"
- uses: actions/get-file@v1
  with:
    path: 'foobar.xml'
    type: 'application/xml'
```

If you need to get files that are not in the current directory,
use the `working-directory` statement:

```yaml hl_lines="4"
- uses: actions/get-file@v1
  with:
    path: 'bar.json'
  working-directory: /data/foo
```

### <i data-feather="upload"></i> actions/get-files@v1

Attach a set of files from the execution environment.

`get-files` functions attach the matching files so that publisher
plugins can process them.  They have mandatory `pattern` input.

An optional `type` input may be specified.  It is expected to be
a media type.

If no files match the specified patterns, an error message is issued, and
the workflow is stopped. This is the default behavior.

An optional `warn-if-not-found` input may be specified.  Instead of the
default behaviour, a warning is issued if no file-matching pattern is
found, and the workflow continues.

An optional `skip-if-not-found` input may be specified. In this case, no
message is issued if no file-matching pattern is found, and the workflow
continues.

`warn-if-not-found` and `skip-if-not-found` can not be specified
simultaneously.

#### Inputs

The function has the following inputs:

- `pattern` (required)

    the pattern that identifies the files to attach

- `type` (optional)

    the file type (a media type such as `application/xml`)

- `warn-if-not-found` (optional)

    the warning to display if no matching file is found

- `skip-if-not-found` (optional)

    when set to `true`, remains silent if no matching file is found

#### Examples

```yaml
- uses: actions/get-files@v1
  with:
    pattern: '*.xml'
```

It is a good practice to specify the attachment type if known.  Please
note that all matching files will be decorated with the specified type:

```yaml hl_lines="4"
- uses: actions/get-files@v1
  with:
    pattern: '*.xml'
    type: 'application/xml'
```

If you want to get files recursively in a directory tree, use the
`**/*.ext` pattern format:

```yaml hl_lines="3"
- uses: actions/get-files@v1
  with:
    pattern: '**/*.html'
  working-directory: /data/foo
```

If you need to get files that are not in the current directory,
use the `working-directory` statement:

```yaml hl_lines="4"
- uses: actions/get-files@v1
  with:
    pattern: '*.json'
  working-directory: /data/foo
```

If you want to display a warning message when no file matching pattern
is found, use the `warn-if-not-found` statement:

```yaml hl_lines="4"
- uses: actions/get-files@v1
  with:
    pattern: '*.xml'
    warn-if-not-found: 'No *.xml files found.'
```

For silent operations in case where no file matching pattern is found, use
the `skip-if-not-found` statement:

```yaml hl_lines="4"
- uses: actions/get-files@v1
  with:
    pattern: '*.txt'
    skip-if-not-found: true
```

### <i data-feather="arrow-up"></i> actions/upload-artifact@v1

Upload an artifact from the execution environment.

`upload-artifact` functions attach the specified file so that publisher
plugins can process them. The file is attached at the workflow level, not on the step
level. These functions have a mandatory `path` input and optional `name` and `if-no-files-found`
inputs.

`path` is a file path, relative to the current workspace, that specifies the artifact to attach.

`name` can be used to specify a custom attachment name.

`if-no-file-found` defines the behavior in case the artifact is not found. This property
can be set to `warn`, `error` or `ignore`. If not specified, it defaults to `warn`.
When this property is set to `warn` and the artifact is not found, a warning message is issued.
When it is set to `error`, an error message is displayed and the step fails. When `ignore`
is used, no message is displayed.

Files uploaded with `upload-artifact` functions can be downloaded with `download-artifact`
functions. It is recommended that uploaded artifacts have an unique name to avoid their
overwriting during retrieval.

#### Inputs

The function has the following inputs:

- `path` (required)

    the file to attach, relative to the current workspace

- `name` (optional)

    the custom file name

- `if-no-file-found` (optional)

    the behavior in case the file is not found (warn, error or ignore, defaults to warn)

#### Examples

```yaml
- uses: actions/upload-artifact@v1
  with:
    path: 'foo.xml'
```

If you need to customize your attachment name, use the `name`
property:

```yaml
- uses: actions/upload-artifact@v1
  with:
    name: 'bar.txt'
    path: 'foo.txt'
```

If you need to get files that are not in the current directory,
use the `working-directory` statement:

```yaml hl_lines="4"
- uses: actions/upload-artifact@v1
  with:
    path: 'bar.json'
  working-directory: /data/foo
```

### <i data-feather="arrow-up"></i> actions/download-artifact@v1

Download an artifact to the execution environment.

`download-artifact` functions download workflow artifacts previously uploaded with
`upload-artifact` to the execution environment.

These functions have optional `name`, `pattern` and `path` inputs.

If no input is specified, all workflow artifacts are downloaded to the current workspace.

To download single artifact, `name` input can be used. To download multiple artifacts,
you can specify `pattern`.

`path` allows to specify the directory the artifacts should be downloaded to. It
defaults to the current workspace. When `name` and `path` are specified, `path` should
be a file path, when `pattern` and `path` are specified, `path` should be a directory.

If the artifacts uploaded with `upload-artifact` function have identical names, only the
last uploaded artifact will be retrieved.

#### Inputs

The function has the following inputs:

- `name` (optional)

    the name of artifact to download

- `pattern` (optional)

    the pattern of artifacts to download

- `path` (optional)

    the path to download artifacts to, file path or directory

#### Examples

Downloading an artifact providing its name.

```yaml
- uses: actions/download-artifact@v1
  with:
    name: foo.json
    path: /path/to/foo.json
```

Downloading multiple artifacts providing a pattern.

```yaml
- uses: actions/download-artifact@v1
  with:
    pattern: *.html
    path: /some/dir/path
```

Downloading multiple artifacts to the current workspace.

```yaml
- uses: actions/download-artifact@v1
  with:
    pattern: *tar*
```

Downloading all workflow artifacts to the current workspace.

```yaml
- uses: actions/download-artifact@v1
```

### <i data-feather="film"></i> actions/prepare-inception@v1

Preload the inception environment with data.

A common use case is to prepare test execution results. Provider plugins
typically document the artifacts they generate. Please refer to their
documentation for more information.

#### Inputs

The function has the following inputs:

- `{pattern}` (optional)

    the pattern that identifies the files to attach

#### Example

```yaml
- uses: actions/prepare-inception@v1
  with:
    export.xml: ${{ resources.files.export }}
    report.html: ${{ resources.files.report }}
```

<script src="../../js/feather.min.js"></script>
<script>feather.replace()</script>

