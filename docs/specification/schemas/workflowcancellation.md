<!--
Copyright (c) 2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# WorkflowCancellation schema

```json
{
    "$schema": "https://json-schema.org/draft/2019-09/schema#",
    "title": "JSON SCHEMA for opentestfactory.org/v1 WorkflowCancellation manifests",
    "type": "object",
    "properties": {
        "apiVersion": {
            "const": "opentestfactory.org/v1"
        },
        "kind": {
            "const": "WorkflowCancellation"
        },
        "metadata": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "namespace": {
                    "type": "string",
                    "pattern": "^[a-z0-9][a-z0-9-]*$"
                },
                "workflow_id": {
                    "type": "string"
                },
                "labels": {
                    "type": "object",
                    "patternProperties": {
                        "^([a-zA-Z0-9-.]+/)?[a-zA-Z0-9]([a-zA-Z0-9._-]*[a-zA-Z0-9])?$": {
                            "type": "string"
                        }
                    },
                    "minProperties": 1,
                    "additionalProperties": false
                }
            },
            "additionalProperties": true,
            "required": [
                "name",
                "workflow_id"
            ]
        },
        "details": {
            "type": "object",
            "properties": {
                "reason": {
                    "type": "string"
                },
                "source": {
                    "type": "string"
                }
            },
            "patternProperties": {
                "^.*\\..*$": {
                    "type": [
                        "string",
                        "array",
                        "object"
                    ]
                }
            },
            "additionalProperties": false
        }
    },
    "required": [
        "apiVersion",
        "kind",
        "metadata"
    ],
    "additionalProperties": false
}
```
