<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# GeneratorResult schema

```json
{
    "$schema": "https://json-schema.org/draft/2019-09/schema#",
    "title": "JSON SCHEMA for opentestfactory.org/v1 GeneratorResult manifests",
    "type": "object",
    "properties": {
        "apiVersion": {
            "enum": [
                "opentestfactory.org/v1alpha1",
                "opentestfactory.org/v1beta1",
                "opentestfactory.org/v1"
            ]
        },
        "kind": {
            "const": "GeneratorResult"
        },
        "metadata": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "namespace": {
                    "type": "string",
                    "pattern": "^[a-z0-9][a-z0-9-]*$"
                },
                "workflow_id": {
                    "type": "string"
                },
                "job_id": {
                    "type": "string"
                },
                "job_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "labels": {
                    "type": "object",
                    "patternProperties": {
                        "^([a-zA-Z0-9-.]+/)?[a-zA-Z0-9]([a-zA-Z0-9._-]*[a-zA-Z0-9])?$": {
                            "type": "string"
                        }
                    },
                    "minProperties": 1,
                    "additionalProperties": false
                }
            },
            "additionalProperties": true,
            "required": [
                "name",
                "workflow_id",
                "job_id",
                "job_origin"
            ]
        },
        "jobs": {
            "$ref": "#/definitions/jobs"
        },
        "outputs": {
            "$ref": "#/definitions/outputs"
        }
    },
    "required": [
        "apiVersion",
        "kind",
        "metadata",
        "jobs"
    ],
    "additionalProperties": false,
    "definitions": {
        "defaults": {
            "type": "object",
            "propertyNames": {
                "pattern": "^[a-zA-Z][a-zA-Z0-9-]*$"
            }
        },
        "job-generator": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "if": {
                    "type": "string"
                },
                "concurrency": {
                    "$ref": "#/definitions/concurrency"
                },
                "runs-on": {
                    "$ref": "#/definitions/runs-on"
                },
                "needs": {
                    "$ref": "#/definitions/needs"
                },
                "defaults": {
                    "$ref": "#/definitions/defaults"
                },
                "variables": {
                    "$ref": "#/definitions/variables"
                },
                "timeout-minutes": {
                    "$ref": "#/definitions/number-expression"
                },
                "strategy": {
                    "$ref": "#/definitions/strategy"
                },
                "jobs": {
                    "$ref": "#/definitions/jobs"
                },
                "generator": {
                    "type": "string"
                },
                "uses": {
                    "type": "string"
                },
                "with": {
                    "$ref": "#/definitions/with"
                },
                "outputs": {
                    "$ref": "#/definitions/outputs"
                },
                "continue-on-error": {
                    "$ref": "#/definitions/boolean-expression"
                }
            },
            "oneOf": [
                {
                    "required": [
                        "generator"
                    ]
                },
                {
                    "required": [
                        "uses"
                    ]
                },
                {
                    "required": [
                        "jobs"
                    ]
                }
            ],
            "additionalProperties": false
        },
        "job-steps": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "if": {
                    "type": "string"
                },
                "concurrency": {
                    "$ref": "#/definitions/concurrency"
                },
                "runs-on": {
                    "$ref": "#/definitions/runs-on"
                },
                "needs": {
                    "$ref": "#/definitions/needs"
                },
                "defaults": {
                    "$ref": "#/definitions/defaults"
                },
                "variables": {
                    "$ref": "#/definitions/variables"
                },
                "timeout-minutes": {
                    "$ref": "#/definitions/number-expression"
                },
                "strategy": {
                    "$ref": "#/definitions/strategy"
                },
                "steps": {
                    "type": "array",
                    "minItems": 1,
                    "items": {
                        "anyOf": [
                            {
                                "type": "object",
                                "properties": {
                                    "name": {
                                        "type": "string"
                                    },
                                    "id": {
                                        "type": "string"
                                    },
                                    "if": {
                                        "type": "string"
                                    },
                                    "uses": {
                                        "type": "string"
                                    },
                                    "with": {
                                        "$ref": "#/definitions/with"
                                    },
                                    "variables": {
                                        "$ref": "#/definitions/variables"
                                    },
                                    "timeout-minutes": {
                                        "$ref": "#/definitions/number-expression"
                                    },
                                    "continue-on-error": {
                                        "$ref": "#/definitions/boolean-expression"
                                    },
                                    "working-directory": {
                                        "type": "string"
                                    }
                                },
                                "required": [
                                    "uses"
                                ],
                                "additionalProperties": false
                            },
                            {
                                "type": "object",
                                "properties": {
                                    "name": {
                                        "type": "string"
                                    },
                                    "id": {
                                        "type": "string"
                                    },
                                    "if": {
                                        "type": "string"
                                    },
                                    "run": {
                                        "type": "string"
                                    },
                                    "shell": {
                                        "type": "string"
                                    },
                                    "variables": {
                                        "$ref": "#/definitions/variables"
                                    },
                                    "timeout-minutes": {
                                        "$ref": "#/definitions/number-expression"
                                    },
                                    "continue-on-error": {
                                        "$ref": "#/definitions/boolean-expression"
                                    },
                                    "working-directory": {
                                        "type": "string"
                                    }
                                },
                                "required": [
                                    "run"
                                ],
                                "additionalProperties": false
                            }
                        ]
                    }
                },
                "outputs": {
                    "$ref": "#/definitions/outputs"
                },
                "continue-on-error": {
                    "$ref": "#/definitions/boolean-expression"
                }
            },
            "required": [
                "steps"
            ],
            "additionalProperties": false
        },
        "needs": {
            "anyOf": [
                {
                    "type": "string",
                    "pattern": "(^[a-zA-Z_][a-zA-Z0-9_-]*$)|(^\\$\\{\\{.*\\}\\}$)"
                },
                {
                    "type": "array",
                    "minItems": 1,
                    "items": {
                        "type": "string",
                        "pattern": "(^[a-zA-Z_][a-zA-Z0-9_-]*$)|(^\\$\\{\\{.*\\}\\}$)"
                    }
                }
            ]
        },
        "number-expression": {
            "anyOf": [
                {
                    "type": "number"
                },
                {
                    "type": "string",
                    "pattern": "^\\$\\{\\{.*\\}\\}$"
                }
            ]
        },
        "boolean-expression": {
            "anyOf": [
                {
                    "type": "boolean"
                },
                {
                    "type": "string",
                    "pattern": "^\\$\\{\\{.*\\}\\}$"
                }
            ]
        },
        "identifier-expression": {
            "type": "string",
            "pattern": "(^[a-zA-Z][a-zA-Z0-9-]*$)|(^\\$\\{\\{.*\\}\\}$)"
        },
        "runs-on": {
            "anyOf": [
                {
                    "$ref": "#/definitions/identifier-expression"
                },
                {
                    "type": "array",
                    "minItems": 1,
                    "items": {
                        "$ref": "#/definitions/identifier-expression"
                    }
                },
                {
                    "type": "object"
                }
            ]
        },
        "variables": {
            "type": "object",
            "patternProperties": {
                "^[a-zA-Z0-9_]+$": {
                    "oneOf": [
                        {
                            "type": "string"
                        },
                        {
                            "type": "object",
                            "properties": {
                                "value": {
                                    "type": "string"
                                },
                                "verbatim": {
                                    "type": "boolean"
                                }
                            },
                            "required": [
                                "value"
                            ],
                            "additionalProperties": false
                        }
                    ]
                }
            },
            "minProperties": 1
        },
        "strategy": {
            "type": "object",
            "properties": {
                "max-parallel": {
                    "$ref": "#/definitions/number-expression"
                },
                "fail-fast": {
                    "$ref": "#/definitions/boolean-expression"
                },
                "matrix": {
                    "type": "object",
                    "properties": {
                        "exclude": {
                            "type": "array",
                            "minItems": 1,
                            "items": {
                                "type": "object",
                                "patternProperties": {
                                    "^[a-zA-Z][a-zA-Z0-9-]*$": {
                                        "type": "string"
                                    }
                                }
                            }
                        },
                        "include": {
                            "type": "array",
                            "minItems": 1,
                            "items": {
                                "type": "object",
                                "patternProperties": {
                                    "^[a-zA-Z][a-zA-Z0-9-]*$": {
                                        "type": "string"
                                    }
                                }
                            }
                        }
                    },
                    "patternProperties": {
                        "^[a-zA-Z][a-zA-Z0-9-]*$": {
                            "type": "array",
                            "minItems": 1,
                            "items": {
                                "oneOf": [
                                    {
                                        "type": "string"
                                    },
                                    {
                                        "type": "number"
                                    },
                                    {
                                        "type": "object",
                                        "patternProperties": {
                                            "^[a-zA-Z][a-zA-Z0-9-]*$": {
                                                "type": "string"
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            "minProperties": 1,
            "additionalProperties": false
        },
        "concurrency": {
            "type": "object",
            "properties": {
                "group": {
                    "type": "string"
                },
                "cancel-in-progress": {
                    "$ref": "#/definitions/boolean-expression"
                }
            },
            "additionalProperties": false,
            "required": [
                "group"
            ]
        },
        "jobs": {
            "type": "object",
            "patternProperties": {
                "^[a-zA-Z_][a-zA-Z0-9_-]*$": {
                    "oneOf": [
                        {
                            "$ref": "#/definitions/job-generator"
                        },
                        {
                            "$ref": "#/definitions/job-steps"
                        }
                    ]
                }
            },
            "minProperties": 1
        },
        "with": {
            "type": "object"
        },
        "outputs": {
            "type": "object",
            "patternProperties": {
                "^[a-zA-Z_][a-zA-Z0-9_-]*$": {
                    "oneOf": [
                        {
                            "type": "object",
                            "properties": {
                                "description": {
                                    "type": "string"
                                },
                                "value": {
                                    "type": "string"
                                }
                            },
                            "required": [
                                "value"
                            ],
                            "additionalProperties": false
                        },
                        {
                            "type": "string"
                        }
                    ]
                }
            },
            "minProperties": 1
        }
    }
}
```
