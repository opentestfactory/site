<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Notification schema

```json
{
    "$schema": "http://json-schema.org/draft/2019-09/schema#",
    "title": "JSON SCHEMA for opentestfactory.org/v1alpha1 Notification manifests",
    "type": "object",

    "properties": {
        "apiVersion": { "const": "opentestfactory.org/v1alpha1" },
        "kind": { "const": "Notification" },
        "metadata": {
            "type": "object",
            "properties": {
                "name": { "type": "string" },
                "workflow_id": { "type": "string" }
            },
            "additionalProperties": true,
            "required": [ "name" ]
        },
        "spec": {
            "type": "object",
            "properties": {
                "worker": {
                    "type": "object",
                    "properties": {
                        "worker_id": { "type": "string" },
                        "status": { "type": "string", "enum": [ "setup", "teardown" ] }
                    },
                    "additionalProperties": false,
                    "required": [ "worker_id", "status" ]
                }
            },
            "additionalProperties": true
        }
    },
    "required": [ "apiVersion", "kind", "metadata", "spec" ],
    "additionalProperties": false
}
```
