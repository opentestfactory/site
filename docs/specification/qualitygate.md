<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Quality gate

A quality gate is a service that is used to check the _quality gate_ status of _workflows_. It exposes two
user-facing endpoints that can be queried to get quality gate info on workflows.

A quality gate is a set of rules a workflow execution must satisfy.

An implementation may provide a quality gate service.

## Endpoints

The quality gate service exposes two endpoints: `GET` and `POST` `qualitygate`.  
They return [Status](status.md) messages.

### `/workflows/{workflow_id}/qualitygate` (GET)

The request is a `GET` type request.

```bash
GET /workflows/{workflow_id}/qualitygate[?mode=...|strict|passing][&timeout=...]
```

If `mode` is not specified, `strict` is assumed.  If `timeout` is not specified, 8 seconds are assumed.

The possible return codes are: `OK` (200), `Accepted` (202), `Invalid` (422), `Unauthorized` (401), or `NotFound` (404).

If the request is accepted (`status` is `Success`), `details.status` must contain the quality gate status for the workflow.  That
status must be one of:

- `SUCCESS`: the workflow has succeeded and passes the quality gate.
- `NOTEST`: the workflow has succeeded, but it produced no test result.
- `FAILURE`: the workflow has failed, or failed the quality gate.
- `RUNNING`: the workflow execution is still ongoing.

### `/workflows/{workflow_id}/qualitygate` (POST)

The request is a `POST` type request.

=== "Bash"

    ```bash
    curl -X POST \
          -H "Authorization: Bearer ${TOKEN}" \
          -H "Content-type: application/x-yaml" \
          --data-binary @my_qualitygates.yaml \
          http://orchestrator.example.com/workflows/{workflow_id}/qualitygate?mode=...&timeout=...
    ```

=== "CMD"

    ```bat
    curl -X POST ^
          -H "Authorization: Bearer %TOKEN%" ^
          -H "Content-type: application/x-yaml" ^
          --data-binary @my_qualitygates.yaml ^
          http://orchestrator.example.com/workflows/{workflow_id}/qualitygate?mode=...&timeout=...
    ```

=== "PowerShell"

    ```powershell
    curl.exe -X POST `
          -H "Authorization: Bearer $Env:TOKEN" `
          -H "Content-type: application/x-yaml" `
          --data-binary '@my_qualitygates.yaml' `
          http://orchestrator.example.com/workflows/{workflow_id}/qualitygate?mode=...&timeout=...
    ```

If `mode` is not specified, `Failure` is returned.  `timeout` is optional, it defaults to 8 seconds.

## Modes

The quality gate service allows for multiple 'modes' (sets of rules). The `mode` value corresponds 
to the name of a quality gate in the quality gate definition file. This file may be loaded during 
the service launch or specified by the user.

Two default modes must be available when sending a request to the `GET` endpoint: `strict` and `passing`. 

- `strict` means a workflow will pass the quality gate if and only if it has been completed successfully and all test cases were OK.
  (It implies that a workflow with no test case will pass the quality gate if it has been completed successfully).

- `passing` means a workflow will pass the quality gate if it has been completed successfully, even if some test cases were not OK.

If no mode is specified, `strict` is used.

The `POST` endpoint accepts as `mode` only the modes defined in the request payload. 
If no mode is specified, `Failure` is returned.

An implementation may propose additional modes.

## Workflow statuses examples

This is a status manifest for a workflow with at least one failed test, using the `passing` mode.  As long as the workflow has been completed successfully, it will pass the quality gate:

```bash
curl \
  http://127.0.0.1/workflows/5dcc66dd-5d0d-4dcf-9b18-f39eafc0f279/qualitygate?mode=passing
```

```json
{
    "kind": "Status",
    "apiVersion": "v1",
    "metadata": {},
    "status": "Success",
    "message": "",
    "details": {
        "status": "SUCCESS"
    },
    "code": 200,
}
```

This is a status manifest for the same workflow, using the default (`strict`) mode.  As it has at least one failed test, it fails
that quality gate:

```bash
curl \
  http://127.0.0.1/workflows/5dcc66dd-5d0d-4dcf-9b18-f39eafc0f279/qualitygate
```

```json
{
    "kind": "Status",
    "apiVersion": "v1",
    "metadata": {},
    "status": "Success",
    "message": "",
    "details": {
        "status": "FAILURE"
    },
    "code": 200,
}
```
