<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Workflow commands for OpenTestFactory Orchestrator Plugins

You can use workflow commands when running shell commands in a workflow or in
a plugin's code.

## About workflow commands

Providers plugins can communicate with the execution environment to set environment
variables, output values used by other plugins, add debug messages to the output logs,
and other tasks.

Most workflow commands use the `echo` command in a specific format, while
others are invoked by writing to a file.  For more information, see
"[Environment Files](#environment-files)."

<h3>Example of a workflow command</h3>

```bash
echo "::workflow-command parameter1={data},parameter2={data}::{command value}"
```

!!! Note
    Workflow command and parameter names are case-insensitive.

!!! Warning
    If you are using Command Prompt, omit double quote characters
    (`"`) when using workflow commands.

## Attaching a file

`::attach type={type}::{path}`

Sets a function's attachment.  You can optionally provide a type further describing the attachment.

The path is an absolute path, or a relative path preceded by ```pwd`/`` or `%CD%\` 
depending on the execution environment.

<h3>Example: attaching a file with an absolute path</h3>

=== "Linux, macOS"

    ```bash
    echo "::attach::/home/user/foobar.yaml"
    ```

=== "Windows"

    ```bat
    echo ::attach::C:\Users\User\foobar.yaml
    ```

<h3>Example: attaching a file with a relative path and a file type</h3>

=== "Linux, macOS"

    ```bash
    echo "::attach type=application/json::`pwd`/foobar.json"
    ```

=== "Windows"

    ```bat
    echo ::attach type=application/json::%CD%\foobar.json
    ```

## Uploading an artifact

`::upload type={type},name={name}::{path}`

Sets a workflow's attachment.  You can optionally provide a type further describing 
the attachment and a custom attachment file name.

The path is an absolute path, or a relative path preceded by ```pwd`/`` or `%CD%\`
depending on the execution environment.

<h3>Example: uploading a file with an absolute path and custom name</h3>

=== "Linux, macOS"

    ```bash
    echo "::upload name=barfoo.yaml::/home/user/foobar.yaml"
    ```

=== "Windows"

    ```bat
    echo ::upload name=barfoo.yaml::D:\Users\User\foobar.yaml
    ```

<h3>Examples: uploading a file with a relative path</h3>

=== "Linux, macOS"

    ```bash
    echo "::upload::`pwd`/foobar.yaml"
    ```


=== "Windows"

    ```bat
    echo ::upload::%CD%\foobar.yaml
    ```

## Sending a file to an execution environment

`::put file={name}::{path}`

Copies a file to the execution environment.  `name` must be a file described
in the workflow's `resources.files` section.

<h3>Example</h3>

```bash
echo "::put file=foo.json::where/to/put/bar.json"
```

## Downloading an artifact to an execution environment

`::download {file|pattern}={name|pattern}::{path}`

Downloads a workflow's attachment (uploaded with `::upload::` commmand) to the 
execution environment.

`file`, `pattern` and `path` are optional. The command defaults to 
`::download pattern=*::.`, downloading all available artifacts to the 
current workspace.

When `file` is used, `path` should be a file path. When `pattern` is used, 
`path` should be an existing directory.

<h3>Example: downloading an artifact to a file path</h3>

=== "Linux, macOS"

    ```bash
    echo "::download file=foo.json::/mnt/data/bar.json"
    ```


=== "Windows"

    ```bat
    echo ::download file=foo.json::D:\data\bar.json
    ```

<h3>Example: downloading multiple artifacts to a directory</h3>

=== "Linux, macOS"

    ```bash
    echo "::download pattern=*.yaml::/mnt/data"
    ```


=== "Windows"

    ```bat
    echo ::download file=*.yaml::D:\data
    ```

## Setting an output parameter

`::set-output name={name}::{value}`

Sets a step's output parameter.  Note that the step will need an `id` to be defined to later
retrieve the output value.

Optionally, you can also declare output parameters in a plugin's metadata
file.  For more information, see "[Metadata syntax for OpenTestFactory Orchestrator Plugins](/plugins/metadata-syntax-for-opentf-plugins.html)."

<h3>Example of setting an output parameter</h3>

```bash
echo "::set-output name=action_fruit::strawberry"
```

## Setting a debug message

`::debug::{message}`

Prints a debug message to the log.  You must create a variable named
`FUNCTIONS_STEP_DEBUG` with the value `true` to see the debug messages set by
this command in the log.

<h3>Example: Setting a debug message</h3>

```bash
echo "::debug::Set the Octocat variable"
```

## Setting a warning message

`::warning file={name},line={line},col={col}::{message}`

Creates a warning message and prints the message to the log.  You can
optionally provide a filename (`file`), line number (`line`), and column
(`col`) number where the warning occurred.

<h3>Example: Setting a warning message</h3>

```bash
echo "::warning file=app.js,line=1,col=5::Missing semicolon"
```

## Setting an error message

`::error file={name},line={line},col={col}::{message}`

Creates an error message and prints the message to the log.  You can optionally
provide a filename (`file`), line number (`line`), and column (`col`) number
where the error occurred.

<h3>Example: Setting an error message</h3>

```bash
echo "::error file=app.js,line=10,col=15::Something went wrong"
```

## Masking a value in log

`::add-mask::{value}`

Masking a value prevents a string or variable from being printed in the log.
Each masked word separated by whitespace is replaced with the `*` character.
You can use an environment variable or string for the mask's `value`.

The masking starts right after the masking command and lasts until the job ends.

<h3>Example: Masking a string</h3>

When you print `"I'm Mona The Octocat"` in the log, you'll see `"I'm ***"`.

```bash
echo "::add-mask::Mona The Octocat"
echo "I'm Mona The Octocat"
```

<h3>Example: Masking an environment variable</h3>

When you print the variable `MY_NAME` or the value `"Mona The Octocat"` in the
log, you'll see `"***"` instead of `"Mona The Octocat"`.

```bash
MY_NAME="Mona The Octocat"
echo "::add-mask::$MY_NAME"
echo "$MY_NAME"
```

<h3>Example: Masking a string in the middle of a script</h3>

When you print something after a masking command, the values are masked.

```yaml hl_lines="6 10"
jobs:
  masking-demo:
    - runs-on: windows
    - steps:
      - run: echo mona the octocat
      - run: echo ::add-mask::mona
      - run: echo mona the octocat
      - run: |
          echo mona the octocat
          echo ::add-mask::octocat
          echo mona the octocat
```

In your execution log you will see:

```bash
mona the octocat   # no mask in effect
*** the octocat    # first mask applies
*** the octocat    # first mask still applies, the second is not in effect
*** the ***        # both masks are in effect
```

## Stopping and starting workflow commands

`::stop-commands::{endtoken}`

Stops processing any workflow commands.  This special command allows you to log
anything without accidentally running a workflow command.  For example, you
could stop logging to output an entire script that has comments.

To stop the processing of workflow commands, pass a unique token to `stop-commands`.
To resume processing workflow commands, pass the same token that you used to stop
workflow commands.

!!! Warnings
    Make sure the token you are using is randomly generated and unique for each run.

```text
::{endtoken}::
```

<h3>Example: Stopping workflow commands</h3>

```bash
echo "::stop-commands::pause-logging"
```

To start workflow commands, pass the token that you used to stop workflow
commands.

```bash
echo "::pause-logging::"
```

<h3>Example: Stopping and starting workflow commands</h3>

```yaml
jobs:
  workflow-command-job:
    runs-on: linux
    steps:
    - name: disable workflow commands
    - run: |
        echo "::warning::this is a warning"
        hash=$RANDOM
        echo "::stop-commands::${hash}"
        echo "::warning::this will NOT be a warning"
        echo "::${hash}::"
        echo "::warning::this is a warning again"
```

## Environment Files

During the execution of a workflow, the execution environment generates temporary
files that can be used to perform certain tasks.  The path to these files are exposed
via environment variables.  You will need to use UTF-8 encoding when writing to these
files to ensure proper processing of the commands.  Multiple commands can be written
to the same file, separated by newlines.

### Setting an environment variable

=== "Linux, macOS"

    ```bash
    echo "{name}={value}" >> $OPENTF_VARIABLES
    ```

=== "Windows"

    ```bat
    echo set {name}={value}>>%OPENTF_VARIABLES%
    ```

You can make an environment variable available to any subsequent steps in a workflow
job by defining or updating the environment variable and writing this to the
`OPENTF_VARIABLES` environment file.  The step that creates or updates the environment
variables does not have access to the new value, but all subsequent steps in a job will
have access.  The names of environment variables are case-sensitive, and you can include
punctuation.

You can access the variables from the `OPENTF_VARIABLES` environment files 
referencing them directly or using [`variables` context](../../impl/reference/contexts.html#variables-context).

<h3>Example: writing an environment variable to <code>OPENTF_VARIABLES</code></h3>

```yaml
jobs:
  set_var:
    runs-on: linux
    steps:
    - name: Set the value
      run: |
        echo "action_state=yellow" >> $OPENTF_VARIABLES
    - name: Reference the variable
      run: |
        echo "$action_state"          # This will output 'yellow'
    - name: Use variables context
      run: |
        echo "variables.action_state" # This will also output 'yellow'
```
