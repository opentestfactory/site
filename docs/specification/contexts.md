<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Contexts

_You can access context information in workflows, hooks, quality gates, and insights._

## About contexts

Contexts are a way to access information about workflow runs, execution environments, jobs,
and steps.  Each context is an object that contains properties, which can be strings or other
objects.

Contexts, objects, and properties will vary significantly under different workflow run
conditions. For example, the `job` context is only populated for steps and job outputs.

You can access contexts using the expression syntax. For more information, see
"[Expressions](expressions.md)."

```yaml
${{ <context> }}
```

!!! Warning
    When creating workflows and provider tasks, you should always consider whether your
    code might execute untrusted input from possible attackers. Certain contexts should
    be treated as untrusted input, as an attacker could insert their malicious content.
    For more information, see "[Understanding the risk of script injections](/security-guides/security-hardening.html)."

| Context&nbsp;name | Type     | Description
| ---          | ---      | ---
| `opentf  `   | `object` | Information about the workflow run.  For more information, see [`opentf` context](#opentf-context).
| `variables`  | `object` | Contains environment variables set in a workflow, job, or step.  For more information, see [`variables` context](#variables-context).
| `resources`  | `object` | Information about the resources set in a workflow.  For more information, see [`resources` context](#resources-context).
| `job`        | `object` | Information about the currently executing job.  For more information, see [`job` context](#job-context).
| `jobs`       | `object` | For workflows outputs only, contains outputs of jobs from the workflow.  For more information, see [`jobs` context](#jobs-context).
| `steps`      | `object` | Information about the steps that have been run in this job.  For more information, see [`steps` context](#steps-context).
| `runner`     | `object` | Information about the execution environment that is running the current job.  For more information, see [`runner` context](#runner-context).
| `needs`      | `object` | Contains the outputs of all jobs that are defined as a dependency of the current job.  For more information, see [`needs` context](#needs-context).
| `inputs`     | `object` | Contains the inputs of a reusable or manually triggered workflow. For more information, see [`inputs` context](#inputs-context).
| `test`       | `object` | Information about a test execution.  For more information, see [`test` context](#test-context).
| `workflow`   | `object` | Information about a completed workflow. For more information, see [`workflow` context](#workflow-context).


As part of an expression, you may access context information using one of two syntaxes.

- Index syntax: `opentf['job']`
- Property dereference syntax: `opentf.job`

To use property dereference syntax, the property name must start with a letter or `_` and contain
only alphanumeric characters, `-`, or `_`.

If you attempt to dereference a non-existent property, it will evaluate to an empty string.

### Determining when to use contexts

The OpenTestFactory orchestrator includes a collection of variables called _contexts_ and a
similar collection of variables called _default environment variables_. These variables are
intended for use at different points in the workflow:

- **Default environment variables:** These variables exist only on the execution environment that is
  executing your job. For more information, see
  "[Default environment variables](/learn-opentf-orchestrator/variables.html#default-environment-variables)."

- **Contexts:** You can use most contexts at any point in your workflow, including when
  _default environment variables_ would be unavailable. For example, you can use contexts
  with expressions to perform initial processing before the job is routed to an execution
  environment for execution; this allows you to use a context with the conditional `if`
  keyword to determine whether a step should run. Once the job is running, you can also
  retrieve context variables from the execution environment that is executing the job, such
  as `runner.os`.  For details of where you can use various contexts within a workflow, see
  "[Context availability](#context-availability)."

The following example demonstrates how these different types of environment variables can be
used together in a job:

```yaml
metdata:
  name: CI
jobs:
  prod-check:
    if: ${{ opentf.actor == 'octocat' }}
    runs-on: linux
    steps:
      - run: echo "Deploying to production server on behalf of $OPENTF_ACTOR"
```

In this example, the `if` statement checks the `opentf.actor` context to determine the
caller's name; if the name is `octocat`, then the subsequent steps are executed. The
`if` check is processed by the OpenTestFactory orchestrator, and the job is only sent to the
execution environment if the result is true. Once the job is sent to the execution
environment, the step is executed and refers to the `$OPENTF_ACTOR` environment variable
from the execution environment.

### Context availability

Different contexts are available throughout a workflow run. For example, the `runner`
context may only be used at certain places within a job.

In addition, some functions may only be used in certain places. For example, the `success()`
function is not available everywhere.

The following table indicates where each context and special function can be used within a
workflow. Unless listed below, a function can be used anywhere.

| Workflow key                      | Context                         | Special functions
| ---                               | ---                             | ---
| `defaults`                        | `opentf`<!--, `inputs`-->, `variables` | None
| `hooks[*].if`                     | `opentf`, `inputs`, `variables`, `needs`, `job`, `runner`, `steps`  | `always`, `cancelled`, `success`, `failure`
| `resources`                       | `opentf`<!--, `inputs`-->, `variables` | None
| `strategy.max-parallel`           | `opentf`<!--, `inputs`-->, `variables` | None
| `timeout-minutes`                 | `opentf`<!--, `inputs`-->, `variables` | None
| `variables`                       | `opentf`<!--, `inputs`--><!--, `secrets`-->   | None
| `jobs.<job_id>.continue-on-error` | `opentf`<!--, `inputs`-->, `variables`, `needs` | None
| `jobs.<job_id>.defaults.run`      | `opentf`<!--, `inputs`-->, `variables`, `needs` | None
| `jobs.<job_id>.if`                | `opentf`<!--, `inputs`-->, `variables`, `needs` | `always`, `cancelled`, `success`, `failure`
| `jobs.<job_id>.name`              | `opentf`<!--, `inputs`-->, `needs`    | None
| `jobs.<job_id>.outputs.<output_id>`   | `opentf`<!--, `inputs`-->, `variables`, `needs`, `job`, `runner`<!--, `secrets`-->, `steps` | None
| `jobs.<job_id>.runs-on`           | `opentf`<!--, `inputs`-->, `variables`, `needs` | None
| `jobs.<job_id>.steps[*].continue-on-error` | `opentf`, `inputs`, `variables`, `needs`, `job`, `runner`<!--, `secrets` -->, `steps` | None
| `jobs.<job_id>.steps[*].if`       | `opentf`, `inputs`, `variables`, `needs`, `job`, `runner`, `steps`  | `always`, `cancelled`, `success`, `failure`
| `jobs.<job_id>.steps[*].name`     | `opentf`, `inputs`, `variables`, `needs`, `job`, `runner`<!--, `secrets` -->, `steps` | None
| `jobs.<job_id>.steps[*].run`      | `opentf`, `inputs`, `variables`, `needs`, `job`, `runner`<!--, `secrets` -->, `steps` | None
| `jobs.<job_id>.steps[*].timeout-minutes` | `opentf`, `inputs`, `variables`, `needs`, `job`, `runner`<!--, `secrets` -->, `steps` | None
| `jobs.<job_id>.steps[*].variables` | `opentf`, `inputs`, `variables`, `needs`, `job`, `runner`<!--, `secrets` -->, `steps` | None
| `jobs.<job_id>.steps[*].with`     | `opentf`, `inputs`, `variables`, `needs`, `job`, `runner`<!--, `secrets` -->, `steps`, `resources` | None
| `jobs.<job_id>.steps[*].working-directory` | `opentf`, `inputs`, `variables`, `needs`, `job`, `runner`<!--, `secrets` -->, `steps`  | None
| `jobs.<job_id>.timeout-minutes`   | `opentf`<!--, `inputs`-->, `variables`, `needs` | None
| `jobs.<job_id>.variables`         | `opentf`<!--, `inputs`-->, `variables`, `needs`<!--, `secrets` --> | None
| `jobs.<job_id>.with.<with_id>`    | `opentf`<!--, `inputs`-->, `variables`, `needs`, `resources` | None
| `outputs.<output_id>.<value>`     | `opentf`, `jobs`, `variables`, `inputs` | None

<!-- | `jobs.<job_id>.secrets.<secrets_id>`  | `opentf`, `inputs`, `needs`, `secrets` | None -->

The following table indicates where each context and special function can be used within an
insights specification. Unless listed below, a function can be used anywhere.

| Insights key                          | Context                  | Special functions
| ---                                   | ---                      | ---
| `insights[*].if`                      |                          | `always`, `cancelled`, `success`, `failure`
| `insights[*].spec.scope`              | `test`, `workflow`       | None
| `insights[*].spec.template[*].scope`  | `test`, `workflow`       | None

The following table indicates where each context and special function can be used within a
quality gate specification. Unless listed below, a function can be used anywhere.

| Quality Gate key                      | Context                  | Special functions
| ---                                   | ---                      | ---
| `qualitygates[*].qualitygate[*].scope`| `test`                   | None
| `qualitygates[*].rules[*].rule.scope` | `test`                   | None

The following table indicates where each context and special function can be used within a
retention policy specification. Unless listed below, a function can be used anywhere.

| Retention Policy key                  | Context                  | Special functions
| ---                                   | ---                      | ---
| `retentionpolicies[*].scope`          | `workflow`               | None

<h3>Example printing context information to the log</h3>

You can print the contents of contexts to the log for debugging.  The [`toJSON` function](expressions.md#tojson)
is required to pretty-print JSON objects to the log.

!!! warning
    When using the whole `opentf` context, be mindful that it includes sensitive information
    such as `opentf.token`.  The orchestrator masks secrets when they are printed to the console,
    but you should be cautious when exporting or printing the context.

```yaml
metadata:
  name: Print context
jobs:
  print-context:
    runs-on: linux
    steps:
      - variables:
          OPENTF_CONTEXT: ${{ toJSON(opentf) }}
        run: echo "$OPENTF_CONTEXT"
      - variables:
          JOB_CONTEXT: ${{ toJSON(job) }}
        run: echo "$JOB_CONTEXT"
      - variables:
          RUNNER_CONTEXT: ${{ toJSON(runner) }}
        run: echo "$RUNNER_CONTEXT"
      - variables:
          STEPS_CONTEXT: ${{ toJSON(steps) }}
        run: echo "$STEPS_CONTEXT"
```

## `opentf` context

The `opentf` context contains information about the workflow run and the event that
triggered the run.  You can read most of the `opentf` context data in environment variables.
For more information about environment variables, see
"[Using environment variables](/learn-opentf-orchestrator/variables.html)."

!!! Warning
    When using the whole `opentf` context, be mindful that it includes sensitive information
    such as `opentf.token`.  The orchestrator masks secrets when they are printed to the
    console, but you should be cautious when exporting or printing the `context`.

| Property&nbsp;name    | Type     | Description
| ---                   | ---      | ---
| `opentf`              | `object` | The top-level context available during any job or step in a workflow.
| `opentf.workflow`     | `string` | The name of the workflow.  If the workflow file doesn't specify a name, the value of this property is the full path of the workflow file in the repository.
| `opentf.namespace`    | `string` | The name of the namespace the workflow belongs to.
| `opentf.job`          | `string` | The [`job_id`](/using-workflows/workflows.html#jobsjob_id) of the current job.
| `opentf.actor`        | `string` | The login of the user that initiated the workflow run.
| `opentf.token`        | `string` | A token to authenticate on behalf of the orchestrator plugin.
| `opentf.step`         | `string` | The name of the step currently running.  The orchestrator removes special characters or uses the name `run` when the current step runs a script.  If you use the same function more than once in the same job, the name will include a suffix with the sequence number.  For example, the first script you run will have the name `run1`, and the second script will be named `run2`.  Similarly, the second invocation of `actions/checkout` will be `actionscheckout2`.

<h3>Example contents of the <code>opentf</code> context</h3>

The following example context is from a workflow run triggered by a regular run event.

!!! Note
    This context is an example only. The content of a context depends on the workflow that
    you are running. Contexts, objects, and properties will vary significantly under
    different workflow run conditions.

```json
{
  "token": "***",
  "job": "dump_contexts_to_log",
  "actor": "octocat",
  "workflow": "Context testing",
  "namespace": "default",
  "step": "run12"
}
```

<h3>Example usage of the <code>opentf</code> context</h3>

This example workflow uses the `opentf.actor` context to run a job only if the workflow was
triggered by the expected actor.

```yaml hl_lines="16"
metadata:
  name: Run CI
jobs:
  normal_ci:
    runs-on: linux
    steps:
      - uses: actions/checkout@v2
        with:
          repository: https://git.example.com/my_repo.git
      - name: Run normal CI
        run: ./run-tests

  post_ci:
    needs: normal_ci
    runs-on: linux
    if: ${{ opentf.actor == 'octocat' }}
    steps:
      - uses: actions/checkout@v2
        with:
          repository: https://git.example.com/my_repo.git
      - name: Run PR CI
        run: ./run-additional-post-ci
```

## `variables` context

The `variables` context contains environment variables that have been set in a workflow,
job, or step.  For more information about setting environment variables in your workflow,
see "[Workflow syntax for OpenTestFactory orchestrator](/using-workflows/workflows.html)."

The `variables` context syntax allows you to use the value of an environment variable in
your workflow file.  You can use the `variables` context in any key in a step except for
the `generator`, `id`, and `uses` keys.  For more information on the step syntax, see
"[Workflow syntax for OpenTestFactory orchestrator](/using-workflows/workflows.html)."

If you want to use the value of an environment variable inside an execution environment,
use the execution environment operating system's normal method for reading environment
variables.

| Property&nbsp;name     | Type     | Description
| ---                    | ---      | ---
| `variables`            | `object` | This context changes for each step in a job.  You can access this context from any step in a job.
| `variables.<var name>` | `string` | The value of a specific environment variable.

<h3>Example contents of the <code>variables</code> context</h3>

The content of the `variables` context is a mapping of environment variable names to their
values. The context's contents can change depending on where it is used in the workflow run.

```json
{
  "first_name": "Mona",
  "super_duper_var": "totally_awesome"
}
```

<h3>Example usage of the <code>variables</code> context</h3>

This example workflow shows how the `variables` context can be configured at the workflow,
job, and step levels, as well as using the context in steps.

When more than one environment variable is defined with the same name, the OpenTestFactory
orchestrator uses the most specific environment variable. For example, an environment variable
defined in a step will override job and workflow variables with the same name, while the step
executes. A variable defined for a job will override a workflow variable with the same name,
while the job executes.  

```yaml hl_lines="11 12 20"
metadata:
  name: Hi Mascot
variables:
  mascot: Mona
  super_duper_var: totally_awesome

jobs:
  windows_job:
    runs-on: windows
    steps:
      - run: echo Hi ${{ variables.mascot }}  # Hi Mona
      - run: echo Hi ${{ variables.mascot }}  # Hi Octocat
        variables:
          mascot: Octocat
  linux_job:
    runs-on: linux
    variables:
      mascot: Tux
    steps:
      - run: echo 'Hi ${{ variables.mascot }}'  # Hi Tux
```

An environment variable defined using `OPENTF_VARIABLES` environment file will override job and 
workflow variables with the same name, while the job executes. It can be overridden by an  
environment variable with the same name defined in a step, while the step executes.

```yaml
metadata:
  name: Use OPENTF_VARIABLES
variables:
  source: workflow

jobs:
  first_job:
    runs-on: linux
    variables:
      source: job
    steps:
      - run: echo 'source=opentf_variables' >>$OPENTF_VARIABLES
      - run: echo 'Source: ${{ variables.source }}' # Source: opentf_variables
      - run: echo 'Source: ${{ variables.source }}' # Source: step
        variables:
          source: step
      - run: echo "Hi"
        if: variables.source == 'opentf_variables' # Hi
```


## `resources` context

The `resources` context contains resources that have been set in a workflow.  For more
information about setting resources in your workflow, see "[Workflow syntax for OpenTestFactory orchestrator](/using-workflows/workflows.html)."

The `resources` context syntax allows you to use the resources in your workflow file.

You can only use the `resources` context in the value of the `with` and `name` keys.  For
more information on the step syntax, see "[Workflow syntax for OpenTestFactory orchestrator](/using-workflows/workflows.html)."

| Property&nbsp;name                 | Type     | Description
| ---                                | ---      | ---
| `resources.<resource type>`        | `object` | The resource type objects.  Possible values are `testmanagers`, `repositories`, or `files`.
| `resources.<resource type>.<name>` | `object` | The set of properties for the resource.  There is at least a name property, the other are resource type dependent.

<h3>Example contents of the `resources` context</h3>

This example `resources` context shows two linked files and one defined repository.

```json
{
  "files": {
    "config1": {
      "name": "config1",
      "url": "https://cdn.example.com/abcfoo"
    },
    "config2": {
      "name": "config2",
      "url": "https://cdn.example.com/defbar"
    }
  },
  "repositories": {
    "myrepo": {
      "name": "myrepo",
      "type": "bitbucket",
      "repository": "example/my-example-repo.git",
      "endpoint": "https://bitbucket.org"
    }
  }
}
```

<h3>Example usage of the <code>resources</code> context</h3>

This example workflow takes two configuration files and builds the project twice.  The
configuration files are provided when triggering the workflow, using for example:

```bash
opentf-ctl \
  run workflow bibuild.yaml \
  -f config1=local/conf1.properties \
  -f config2=local/conf2.properties
```

```yaml hl_lines="17 20 26"
metadata:
  name: Build twice
resources:
  files:
  - config1
  - config2
  repositories:
  - name: myrepo    
    type: bitbucket
    repository: example/my-example-repo.git
    endpoint: https://bitbucket.org

jobs:
  build:
    - uses: actions/checkout@v2
      with:
        repository: ${{ resources.repositories.myrepo }}
    - uses: actions/put-file@v1
      with:
        file: config1
        path: conf/config.properties
    - name: Build using first configuration
      run: ./build.sh --target target/build-using-first-config
    - uses: actions/put-file@v1
      with:
        file: config2
        path: conf/config.properties
    - name: Build using second configuration
      run: ./build.sh --target target/build-using-second-config
```

## `job` context

The `job` context contains information about the currently running job.

| Property&nbsp;name | Type     | Description
| ---           | ---      | ---
| `job`         | `object` | This context changes for each job in a workflow run.  You can access this context from any step in a job.
| `job.status`  | `string` | The current status of the job.  Possible values are `success`, `failure`, or `cancelled`.
| `job.runs-on` | `object` | The list of the job tags describing the test case execution environment.

<h3>Example contents of the <code>job</code> context</h3>

This example `job` context contains the status property and the `runs-on` tags.

```json
{
  "status": "success",
  "runs-on": [
    "windows",
    "cypress"
  ]
}
```

## `jobs` context

The `jobs` context is only available in workflow and generator outputs.

| Property&nbsp;name | Type     | Description
| ---                | ---      | ---
| `jobs`             | `object` | This is only available in workflow and generator outputs, and can only be used to set outputs.  This object contains all the properties listed below.
| `jobs.<job_id>.result` | `string` | The result of a job.  Possible values are `success`, `failure`, `cancelled`, or `skipped`.
| `jobs.<job_id>.outputs` | `object` | The set of outputs of a job.
| `jobs.<job_id>.outputs.<output_name>` | `string` | The value of a specific output for a job.

<h3>Example contents of the <code>jobs</code> context</h3>

This example `jobs` context contains the result and outputs of a job from a workflow run.

```json
{
  "example_job": {
    "result": "success",
    "outputs": {
      "output1": "hello",
      "output2": "world"
    }
  }
}
```

<h3>Example usage of the <code>jobs</code> context</h3>

This example workflow uses the `jobs` context to set outputs for the workflow.  Note how the
outputs flow up from the steps, to the jobs, then to the workflow outputs.

```yaml hl_lines="6 9 15 19"
metadata:
  name: Using outputs
outputs:
  firstoutput:
    description: The 'output1' output from job1 ('hello')
    value: ${{ jobs.job1.outputs.output1 }}
  status:
    description: The execution status of job2 ('success')
    value: ${{ jobs.job2.result }}
jobs:
  job1:
    runs-on: linux
    # Map a step output to a job output
    outputs:
      output1: ${{ steps.step1.outputs.test }}
      output2: ${{ steps.step2.outputs.test }}
    steps:
    - id: step1
      run: echo "::set-output name=test::hello"
    - id: step2
      run: echo "::set-output name=test::world"
  job2:
    runs-on: linux
    needs: job1
    steps:
    - run: echo ${{needs.job1.outputs.output1}} ${{needs.job1.outputs.output2}}
```

## `steps` context

The `steps` context contains information about the steps in the current job that have an
`id` specified and have already run.

| Property&nbsp;name           | Type     | Description
| ---                          | ---      | ---
| `steps`                      | `object` | This context changes for each step in a job.  You can access this context from any step in a job.
| `steps.<step id>.outputs`    | `object` | The set of outputs defined for the step.  For more information, see "[Metadata syntax for OpenTestFactory orchestrator plugins](/plugins/metadata-syntax-for-opentf-plugins.html)."
| `steps.<step id>.outputs.<output name>` | `string` | The value of a specific output.
| `steps.<step id>.outcome`    | `string` | The result of a completed step before continue-on-error is applied.  Possible values are `success`, `failure`, `cancelled`, or `skipped`.  When a `continue-on-error` step fails, the outcome is `failure`, but the final conclusion is `success`.
| `steps.<step id>.conclusion` | `string` | The result of a completed step after `continue-on-error` is applied.  Possible values are `success`, `failure`, `cancelled`, or `skipped`.  When a `continue-on-error` step fails, the outcome is `failure`, but the final conclusion is `success`.

For function steps, the outcome is the sum of the direct sub-steps conclusions.

<h3>Example contents of the <code>steps</code> context</h3>

This example `steps` context shows two previous steps that had an `id` specified. The first
step had the `id` named `checkout`, and the second `generate_number`. The `generate_number` step
had an output named `random_number`.

```json
{
  "checkout": {
    "outputs": {},
    "outcome": "success",
    "conclusion": "success"
  },
  "generate_number": {
    "outputs": {
      "random_number": "1"
    },
    "outcome": "success",
    "conclusion": "success"
  }
}
```

<h3>Example usage of the <code>steps</code> context</h3>

This example workflow generates a random number as an output in one step, and a later step
uses the `steps` context to read the value of that output.

```yaml hl_lines="13 16"
metadata:
  name: Generate random failure
jobs:
  randomly-failing-job:
    runs-on: linux
    steps:
      - id: checkout
        uses: actions/checkout@v2
        with:
          repository: https://git.example.com/my_repo.git
      - name: Generate 0 or 1
        id: generate_number
        run:  echo "::set-output name=random_number::$(($RANDOM % 2))"
      - name: Pass or fail
        run: |
          if [[ ${{ steps.generate_number.outputs.random_number }} == 0 ]]; then exit 0; else exit 1; fi
```

## `runner` context

The `runner` context contains information about the execution environment that
is executing the current job.

| Property&nbsp;name | Type     | Description
| ---                | ---      | ---
| `runner`           | `object` | This context changes for each job in a workflow run.  This object contains all the properties listed below.
| `runner.os`        | `string` | The operating system of the runner executing the job.  Possible values are `linux`, `windows`, or `macos`.
| `runner.temp`      | `string` | The path of the temporary directory for the execution environment.  This directory is guaranteed to be empty at the start of each job, even on self-hosted execution environments.

<h3>Example contents of the <code>runner</code> context</h3>

The following example context is from a Linux-based execution environment.

```json
{
  "os": "linux",
  "temp": "/home/runner/work/_temp"
}
```

<h3>Example usage of the <code>runner</code> context</h3>

This example workflow uses the `runner` context to set the path to the temporary directory
to write logs, and if the workflow fails, it uploads those logs as artifacts.

```yaml hl_lines="12 13 19"
metadata:
  name: Build
jobs:
  build:
    runs-on: linux
    steps:
      - uses: actions/checkout@v2
        with:
          repository: https://git.example.com/my_repo.git
      - name: Build with logs
        run: |
          mkdir ${{ runner.temp }}/build_logs
          ./build.sh --log-path ${{ runner.temp }}/build_logs
      - name: Upload logs on fail
        if: ${{ failure() }}
        uses: actions/upload-artifact@v3
        with:
          name: Build failure logs
          path: ${{ runner.temp }}/build_logs
```

## `needs` context

The `needs` context contains outputs from all jobs that are defined as a dependency of the
current job.  For more information on defining job dependencies, see
"[Workflow syntax for OpenTestFactory orchestrator](/using-workflows/workflows.html)."

| Property&nbsp;name       | Type     | Description
| ---                      | ---      | ---
| `needs.<job id>`         | `object` | A single job that the current job depends on.
| `needs.<job id>.outputs.<output name>` | `string` | The value of a specific output for a job that the current job depends on.
| `needs.<job id>.outputs` | `object` | The set of outputs of a job that the current job depends on.
| `needs.<job id>.result`  | `string` | The result of a job that the current job depends on.  Possible values are `success`, `failure`, `skipped`, or `cancelled`.

<h3>Example contents of the <code>needs</code> context</h3>

The following example contents of the `needs` context shows information for two jobs that
the current job depends on.

```json
{
  "build": {
    "result": "success",
    "outputs": {
      "build_id": "ABC123"
    }
  },
  "deploy": {
    "result": "failure",
    "outputs": {}
  }
}
```

<h3>Example usage of the <code>needs</code> context</h3>

This example workflow has three jobs: a `build` job that does a build, a `deploy` job that
requires the build job, and a `debug` job that requires both the `build` and `deploy` jobs
and runs only if there is a failure in the workflow. The `deploy` job also uses the `needs`
context to access an output from the `build` job.

```yaml hl_lines="4 6 7 18 24"
metadata:
  name: Build and deploy
jobs:
  build:
    runs-on: linux
    outputs:
      build_id: ${{ steps.build_step.outputs.build_id }}
    steps:
      - uses: actions/checkout@v2
        with:
          repository: https://git.example.com/my_repo.git
      - name: Build
        id: build_step
        run: |
          ./build
          echo "::set-output name=build_id::$BUILD_ID"
  deploy:
    needs: build
    runs-on: linux
    steps:
      - uses: actions/checkout@v2
        with:
          repository: https://git.example.com/my_repo.git
      - run: ./deploy --build ${{ needs.build.outputs.build_id }}
  debug:
    needs: [build, deploy]
    runs-on: linux
    if: ${{ failure() }}
    steps:
      - uses: actions/checkout@v2
        with:
          repository: https://git.example.com/my_repo.git
      - run: ./debug
```

## `inputs` context

The `inputs` context contains input properties passed to a function (including hooks steps for the function).

There are no standard properties in the `inputs` context, only those which are defined by the
function.

| Property&nbsp;name   | Type     | Description
| ---             | ---      | ---
| `inputs`        | `object` | This context is only available in a function. You can access this context from any step in a function. This object contains the properties listed below.
| `inputs.<name>` | `string` or `number` or `boolean` | Each input value passed from an external workflow.

<h3>Example content of the <code>inputs</code> context</h3>

The following example contents of the `inputs` context is from a function that has defined
the `build_id`, `deploy_target`, and `perform_deploy` inputs.

```json
{
  "build_id": "ABC123",
  "deploy_target": "production",
  "perform_deploy": true
}
```

<h3>Example usage of the <code>inputs</code> context</h3>

This example workflow defines a hook that will echo the test reference of each `execute` function
calls:

```yaml hl_lines="8 18"
metadata:
  name: Using inputs and hooks
hooks:
- name: execute hooks
  events:
  - category: execute
  before:
  - run: echo "Running test reference ${{ inputs.test }}"
jobs:
  job_1:
    runs-on: linux
    steps:
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/robotframework/RobotDemo.git
    - uses: robotframework/execute@v1
      with:
        test: RobotDemo/keyword_driven.robot
```

Running this workflow would give the following output:

```text hl_lines="8"
Workflow d1ce13c1-21f0-472e-8dde-306566e5ca2f is running.
Workflow Using inputs and hooks
(running in namespace 'default')
[2024-07-30T17:48:03] [job f5c6a79f-e8bb-4516-90ee-53d0ca3f3400] Requesting execution environment providing ['linux'] in namespace 'default' for job 'job_1'
[2024-07-30T17:48:03] [job f5c6a79f-e8bb-4516-90ee-53d0ca3f3400] Running function actionscheckoutv2
[2024-07-30T17:48:06] [job f5c6a79f-e8bb-4516-90ee-53d0ca3f3400] Cloning into 'RobotDemo'...
[2024-07-30T17:48:06] [job f5c6a79f-e8bb-4516-90ee-53d0ca3f3400] Running function robotframeworkexecutev1
[2024-07-30T17:48:07] [job f5c6a79f-e8bb-4516-90ee-53d0ca3f3400] Running test reference RobotDemo/keyword_driven.robot
[2024-07-30T17:48:12] [job f5c6a79f-e8bb-4516-90ee-53d0ca3f3400] ==============================================================================
[2024-07-30T17:48:12] [job f5c6a79f-e8bb-4516-90ee-53d0ca3f3400] Keyword Driven :: Example test cases using the keyword-driven testing appro...
[2024-07-30T17:48:12] [job f5c6a79f-e8bb-4516-90ee-53d0ca3f3400] ==============================================================================
...
Workflow completed successfully.
```

## `test` context

The `test` context contains properties associated with a test case.  It is only available
while evaluating scopes (in quality gates as well as in insights).

| Property&nbsp;name   | Type      | Description
| ---                  | ---       | ---
| `test`               | `object`  | This context is only available in a scope.  You can access this context in quality gate scopes and insight scopes.
| `test.job`           | `string`  | The name of the job containing the executed test case.
| `test.technology`    | `string`  | The executed test case technology.
| `test.outcome`       | `string`  | The executed test case outcome (`success`, `failure`, `error` or `skipped`).
| `test.executions`    | `string`  | The executed test case executions count.
| `test.uses`          | `string`  | The name of the function that executed the test case.
| `test.test`          | `string`  | The test reference (`with.test` of the provider function).
| `test.suiteName`     | `string`  | The executed test case suite name.
| `test.testCaseName`  | `string`  | The executed test case name.
| `test.runs-on`       | `list`    | A list of strings.  The tags describing the test case execution environment.
| `test.managed`       | `boolean` | `true` for test cases managed by a test referential.

!!! tip
    Test cases managed by a test referential may provide additional information in the `test`
    context.  Please refer to the test referential documentation for more details.

<h3>Example contents of the <code>test</code> context</h3>

```json
{
  "job": "my-job",
  "technology": "cucumber",
  "uses": "cucumber/execute@v1",
  "runs-on": ["windows", "cucumber", "recette-14"],
  "managed": false
}
```

<h3>Example usage of the <code>test</code> context</h3>

This example insight specification uses the `test` context to create a summary report for
JUnit test cases.

```yaml hl_lines="6"
insights:
- name: junitreport
  kind: SummaryReport
  title: "JUnit Test Execution Summary Report"
  spec:
    scope: test.technology == 'junit'
    template:
    - name: Summary
      kind: SummaryBlock
    - name: Test Cases
      kind: Table
      datasource: testcases
```

## `workflow` context

The `workflow` context contains properties associated with workflows.  It is only available
while evaluating scopes and conditionals in insights and while evaluating retention policy rules.


| Property&nbsp;name   | Type     | Description
| ---                  | ---      | ---
| `workflow`           | `object` | This context is only available in retention policy rules.  This object contains the properties listed below.
| `workflow.name`      | `string` | The workflow name.
| `workflow.namespace` | `string` | The workflow namespace.
| `workflow.status`    | `string` | The workflow completion status, either `success` or `failure`.
| `workflow.creationTimestamp` | `string` | The workflow creation time (an ISO 8601 timestamp).
| `workflow.completionTimestamp` | `string` | The workflow completion time (an ISO 8601 timestamp).

<h3>Example contents of the <code>workflow</code> context</h3>

```json
{
  "name": "my-workflow",
  "namespace": "my-org",
  "status": "success",
  "creationTimestamp": "2020-01-01T00:00:00",
  "completionTimestamp": "2020-01-01T01:23:00"
}
```

<h3>Example usage of the <code>workflow</code> context</h3>

This example of a retention policy file defines three rules based on information coming from
the `workflow` context:

```yaml hl_lines="3 7-9 14-15"
retentionPolicy:
- name: junk workflows
  scope: workflow.namespace == 'junk'
  retentionPeriod: 5m
- name: weekend failed workflows
  scope: >-
    workflow.status == 'failure'
    && ((dayOfWeek(workflow.completionTimestamp) == 'saturday')
        || (dayOfWeek(workflow.completionTimestamp) == 'sunday'))
  weight: 123
  retentionPeriod: 3d
- name: weekend ok workflows
  scope: >-
    (dayOfWeek(workflow.completionTimestamp) == 'saturday')
    || (dayOfWeek(workflow.completionTimestamp) == 'sunday')
  retentionPeriod: 3d
```
