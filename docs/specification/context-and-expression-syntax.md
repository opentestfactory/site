<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Context and expression syntax for workflows

You can access context information and evaluate expressions in workflows.

## About contexts and expressions

You can use expressions to programmatically set variables in workflow files and access
contexts.  An expression can be any combination of literal values, references to a context,
or functions.  You can combine literals, context references, and functions using operators.

For more information about expressions and contexts, see "[Expressions](expressions.md)."
and "[Contexts](contexts.md)."
