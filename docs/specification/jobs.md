<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Jobs

Jobs may have conditionals (`if: expr`).  The conditional is checked on all possible
candidates.  For not-yet running or completed jobs, the conditional is reevaluated
whenever another job completes or the workflow is cancelled.

## Cancellation and forced cancellation

https://docs.github.com/en/actions/managing-workflow-runs/canceling-a-workflow

When a workflow is cancelled, the orchestrator re-evaluates `if` conditions for all
currently running jobs.  If the condition evaluates to `true`, the job will not
get cancelled.  For example, the condition `if: always()` would evaluate to true and the
job continues to run.

For jobs that need to be cancelled, the currently running step runs to completion
and the job result is set to `cancelled`.

It is possible to force the cancellation, ending all running jobs as soon as possible and starting
no more jobs, irrespective of their conditionals.

## Status check functions

You can use the following status check functions as expressions in `if` conditionals:

- `always()`
- `success()`
- `cancelled()`
- `failure()`

If your `if` expression does not contain any of the status functions it will automatically
result in `success()` anded with the expression's value.

If no `if` conditional is specified `success()` is assumed.

!!! note
    If a status function is in the `if` expression, it can be anywhere, including in a
    position that does not change the value of the conditional.

    For example, a job with the following conditional will always run (even if
    the workflow was cancelled):

        if: 'foo' == 'foo' || failure()

    While the following would only run if the workflow is in success:

        if: 'foo' == 'foo'

    (as no status function is used, the expression is 'anded' with `success()`, that
    is, it becomes `success() && ('foo' == 'foo')`)

### `always()`

The job will always run.

It is often better to use `if: ${{ ! cancelled() }}`, as using `always()` makes
the job unstoppable.

### `cancelled()`

True if the workflow has been cancelled.  False otherwise.

Applies to jobs with or without a `needs` clause.  (The cancellation event is global to a workflow.)

### `success()`

For jobs that do not have `needs` clause, `success()` represents the workflow status.  It
will be true if no completed job has failed and the workflow has not been cancelled. (Or more
precisely, completed jobs have either successfully completed or have a `continue-on-error: true`
clause.)

For jobs that have a `needs` clause, `success()` represents the ancestors completion status.
(true if no parent job has failed or was skipped, or, more precisely, if all parent jobs either
successfully completed or have a `continue-on-error: true` clause, and the workflow has not been
cancelled.)

!!! note
    There may be a `fail-fast` clause in the workflow, in which case the aforementioned behaviour may have to be adjusted

### `failure()`

True at least a job in the workflow has failed and contained no `continue-on-error: true` clause.

For jobs with a `needs` clause, applies to the ancestors completion statuses.

## Dependency chains

If a job fails or is skipped, all jobs that need it are skipped unless the jobs use a
conditional that causes the job to continue. If a run contains a series of
jobs that need each other, a failure or skip applies to all jobs in the dependency
chain from the point of failure or skip onwards. If you would like a job to run even
if a job it is dependent on did not succeed, use the `always()` conditional expression
in `jobs.<job_id>.if`.

!!! note
    In the following example, `job_a` is skipped (its conditional failed), `job_b` 
    will not run (`job_a` was skipped). `job_c` will run, as it has an `always()`
    conditional.  And `job_d` will be skipped, as not all jobs in its dependency
    chain have succeeded (`job_d` dependency chain includes `job_a` and `job_c`):

    ```yaml
    jobs:
      job_a:          # skipped
        if: 'false'
      job_b:          # skipped
        needs: job_a
      job_c:          # success
        needs: job_a
        if: always()
      job_d:          # skipped
        needs: job_c
    ```

## Example

```yaml
jobs:
  foo_ok:
    runs-on: linux
    steps:
    - run: echo foo_ok
    - run: sleep 100
    - run: exit 0
  foo_nok:
    runs-on: linux
    steps:
    - run: echo foo_nok
    - run: sleep 100
    - run: exit 1
  bar:
    runs-on: linux
    if: failure()
    steps:
    - run: echo a job has failed
  baz:
    runs-on: linux
    if: cancelled()
    steps:
    - run: echo the workflow has been cancelled
  barbaz:
    runs-on: linux
    if: always()
    steps:
    - run: echo is this always displayed?
```

- **If the workflow is not cancelled**

    - `foo_nok` will always run to completion, with a `failure` result. `foo_ok` will
    run to completion with a `success` result if it
    starts before the completion of the third step of `foo_nok`.  It will be skipped
    otherwise, with a `skipped` result.
    
    - `bar` will run to completion with a `success` result if it starts after the
    completion of the third step of `foo_nok`.  It will be skipped otherwise, with a `skipped` result.
    
    - `barbaz` will always run to completion with a `success` result.
    
    - `baz` will be skipped with a `skipped` result.

- **If the workflow is cancelled before any job started**

    - `bar` and `barbaz` will run to completion with a `success` result.
    - The other jobs will be skipped, with a `skipped` result.

- **If the workflow cancellation occurs at any another time**

    - The completed or skipped jobs will remain with their expected results.
    - `baz` and `barbaz`, if not already completed or skipped, will run to completion with a
      `success` result.
    - The other running jobs will see their result set to `cancelled`, run their current
      steps to completion, and possibly their remaining steps depending on those steps conditionals.
    - The remaining jobs will be skipped with a `cancelled` result.
