<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Plugins

A plugin is a service that subscribes to some events and that may publish other events.

There are four defined categories of plugins that match typical needs. The four defined
categories are _channels_, _generators_, _providers_, and _publishers_.

Other categories can be developed.

All plugins may publish [ExecutionError](schemas/executionerror.md) events if they
detect an unexpected condition.  They may also publish any number of
[Notification](schemas/notification.md) events.

## Channel plugins

Channel plugins handle the link with execution environments.

They should subscribe to [ExecutionCommand](schemas/executioncommand.md) events, and
they should publish [ExecutionResult](schemas/executionresult.md) events.  They may
subscribe to other events and may publish other events.

When a job is about to be processed, the core services publish an event requesting an
execution environment.  This event has a `metadata.step_sequence_id: -1` section.

Channel plugins can make offers upon receiving this event.  Those offers are time-limited.
The core services must select at most one such offer and publish execution events
referring to this offer.

A channel plugin must not offer an execution environment that already is in use for another
job and must not offer an execution environment that it has proposed if the offer time-limit
has not been reached yet.

If a channel plugin receives an execution event referring to an expired offer, it may either
publish a rejection event or process the execution event if the targeted execution environment
is not in use for another job or job offer.

Upon job completion, the core services must publish an event releasing the targeted execution
environment.  The channel plugin handling the execution environment is then free to release or
reuse it.

## Generator plugins

Generator plugins generate sets of jobs.

They should subscribe to [GeneratorCommand](schemas/generatorcommand.md) events and they
publish [GeneratorResult](schemas/generatorresult.md) events.

When a generator job is about to be processed, the core services publish an event requesting
its expansion.  Generator jobs have a _type_ and possibly parameters.

Matching generator plugins can provide a set of jobs upon receiving such events. The core
services will then select at most one such expansion.

The generator plugins are used to query external sources and convert the result to jobs.

For example, a generator plugin can query a test case manager, get a test suite to execute,
and produce the jobs that will result in the test suite test cases executions.

Those generated jobs cannot depend on jobs defined elsewhere in the workflow, but they are
otherwise regular jobs and are processed like any other jobs.

```mermaid
flowchart LR
A["job:\ngenerator: example/foo@v1"] -.-> B([Generator plugin])
B -.-> C[job1:\n...\njob2:\n...]
```

## Provider plugins

Provider plugins generate a sequence of steps.

They should subscribe to [ProviderCommand](schemas/providercommand.md) events and they
publish [ProviderResult](schemas/providerresult.md) events.

When a sequence of steps is being processed, the core services publish an event requesting
an expansion for each _function_ step it encounters.  Functions steps have a _type_ and possibly
parameters.

Matching provider plugins can provide a sequence of steps upon receiving such events.  The core
services will then select at most one such expansion.

The provider plugins are used to wrap up a series of steps as a single step, easing the
creation and readability of workflows.

The generated sequence of steps can contain other function steps which will in turn
expand to sequences of steps.

```mermaid
flowchart LR
A["step:\n- uses: example/foo@v1"] -.-> B([Provider plugin])
B -.-> C["- run: echo foo\n- uses: example/bar@v1"]
```

## Publisher plugins

Publisher plugins consume execution results.  They typically do not produce non-notification
events.

They should subscribe to [ExecutionResult](schemas/executionresult.md) events.

They collect execution results and possibly transform them and then sending
them to external tools, such as a test case manager or a BI platform.
