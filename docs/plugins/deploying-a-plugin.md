<!--
Copyright (c) 2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Deploying a Plugin

There are multiple way you can deploy your plugins.  Here are a few:

- As a separate service in its own server or container
- In a docker image extending `allinone` (or any other OpenTestFactory orchestrator distribution image)
- As a server-less service, behind an API gateway
