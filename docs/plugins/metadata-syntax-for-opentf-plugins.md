<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Descriptor syntax for OpenTestFactory Orchestrator Plugins

_You can create plugins to perform tasks in your workflows.  Plugins require a metadata file
that uses YAML syntax._

## About YAML Syntax for Descriptors

All plugins require a metadata file, called a _descriptor_.  The descriptor filename must be
`plugin.yaml`.  The data in the descriptor defines the _inputs_, _outputs_, and
_main entrypoint_ for your plugin.

Plugin descriptors must use YAML syntax.  If you're new to YAML, you can read
"[Learn YAML in Y minutes](https://learnxinyminutes.com/docs/yaml/){:target="_blank"}."

You can put more than one YAML document in your descriptor.  They may all
refer to the same plugin.

You may add an 'overall' YAML document in your descriptor.

## Mandatory and Optional Sections

Depending on the kind of plugin you are creating, you may need to include
different sections in your descriptor.  The following highlighted sections are required
for all plugins:

```yaml title="plugin.yaml" hl_lines="3-4 7"
apiVersion: "..."   # optional for basic plugins
kind: "..."         # optional for basic plugins
metadata:
  name: "my-plugin"
  description: "A short description of the plugin."
  ...
cmd: "my-plugin"
spec:               # optional, for context parameters definition
  ...
events:             # optional, required for ProviderPlugin and GeneratorPlugin
  ...
inputs:             # optional, for ProviderPlugin and GeneratorPlugin
  ...
outputs:            # optional, for ProviderPlugin and GeneratorPlugin
  ...
reports:            # optional, for ProviderPlugin and GeneratorPlugin
  ...
branding:           # optional
  ...
```

!!! info

    The non-optional sections are required for all plugins.  The optional sections
    are only used by plugins developed using the `opentf-toolkit` Python library.

### Generator Plugins

When writing a generator plugin with the help of the `opentf-toolkit`, you must include a
`metadata.action` element and an `events` section in your descriptor, and, if appropriate,
an `inputs` section and an `outputs` section as well.

The `opentf-toolkit` makes use of those to subscribe to events and to generate the
appropriate code to handle those events.

??? info "Example: a generator descriptor using `opentf-toolkit`"

    ```yaml hl_lines="6 8-11"
    ---
    apiVersion: opentestfactory.org/v1alpha1
    kind: GeneratorPlugin
    metadata:
      name: DummyGenerator
      action: core/dummygenerator
      description: Dummy Generator Plugin
    events:
    - category: dummygenerator
      categoryPrefix: core
      categoryVersion: v1
    cmd: python3 -m opentf.plugins.dummygenerator.main
    ```

### Provider Plugins

When writing a provider plugin with the help of the `opentf-toolkit`, you must include a
`metadata.action` element and an `events` section in your descriptor, and, if appropriate,
an `inputs` section and an `outputs` section as well.

The `opentf-toolkit` makes use of those to subscribe to events and to generate the
appropriate code to handle those events.

(The `metadata.description`, `metadata.notes`, `reports` and `branding` sections are
optional and are only used for documentation purpose.)

??? info "Example: a function descriptor using `opentf-toolkit`"

    ```yaml hl_lines="6 37-40"
    ---
    apiVersion: opentestfactory.org/v1alpha1
    kind: ProviderPlugin
    metadata:
      name: robotframework
      action: robotframework/robot
      description: |
        Run a Robot Framework test suite.
      notes: |
        ## Examples

        This first example runs all tests in the `foobar` test suite:

        - uses: robotframework/robot@v1
          with:
            datasource: foobar

        This second example runs the `foo` test in the `foobar` test suite,
        and an Allure report will be generated:

        - uses: robotframework/robot@v1
          with:
            datasource: foobar
            test: foo
            reports-for-allure: true

        This third example runs all tests having a `ready` tag in the `foobar` test suite:

        - uses: robotframework/robot@v1
          with:
            datasource: foobar
            extra-options: --include ready

    branding:
      icon: play
    cmd: 'python -m opentf.plugins.robotframework.main'
    events:
    - categoryPrefix: robotframework
      category: robot
      categoryVersion: v1
    inputs:
      datasource:
        description: The data source to use.
        required: true
      test:
        description: |
          Specify a test case present in the data source.  By default, all test
          cases in the data source are executed.
        required: false
      reports-for-allure:
        description: |
          A boolean.  Set to `true` to enable the generation of Allure reports.
          By default, Allure reports are not generated.
        required: false
      extra-options:
        description: |
          Specify additional parameters to pass to Robot Framework.

          There is a list of possible command line options you can find on the
          <a href="https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#command-line-options" target="_blank">Command line options</a>
          chapter of the Robot Framework documentation.
        required: false
    reports:
      'output.xml':
        description: |
          The Robot Framework test execution report in XML.

          It has the `application/vnd.opentestfactory.robotframework-output+xml`
          content type.
      'RobotFramework_reports.tar':
        description: |
          A TAR archive. Contains the usual reports:

          -  `log.html`
          -  `report.html`

          and the screenshots:

          -  `*.png`
          -  `*.jpg`
          -  `*.jpeg`
          -  `browser/screenshot/*.png`
          -  `browser/screenshot/*.jpg`
          -  `browser/screenshot/*.jpeg`
      '*-result.json':
        description: |
          JSON result files.

          Contains all `*-result.json` files, when the optional `reports-for-allure` input is `true`.
          Those files enable the generation of Allure reports.
      '*-attachment.html':
        description: |
          HTML attachment files.

          Contains all `*-attachment.html` files, when the optional `reports-for-allure` input is
          `true`.  Those files enable the generation of Allure reports.
    ```

## Sections

### `apiVersion`

Required if `kind` is specified.

### `kind`

The plugin type (`ChannelPlugin`, `ProviderPlugin`, `PublicationPlugin`, or
`GeneratorPlugin`).  Required if `apiVersion` is specified.

The `apiVersion` and `kind` fields are optional for plugins not making use of
the OpenTestFactory orchestrator toolkit.

### `metadata`

The `metadata` section contains the plugin's metadata.

<h4>Example of a <code>metadata</code> section</h4>

```yaml
metadata:
  name: "my-plugin"
```

#### `metadata.name`

**Required** The name of your plugin.

!!! info
    If this name appears in the startup's configuration file `disabled` section (see
    "[Configuration file](../installation.md#startup)" for more information), the plugin
    will not be loaded.

#### `metadata.title`

**Optional** The human-friendly name of your plugin.

#### `metadata.action`

**Required for provider descriptors** The main name of the described function.

#### `metadata.hooks`

**Optional** `default` if the plugin supports the standard hook mechanism.

#### `metadata.author`

**Optional** The name of the plugin's author.

#### `metadata.description`

**Required** A short description of the plugin.

#### `metadata.notes`

**Optional** Textual notes related to plugin.

#### `metadata.license`

**Optional** The license the plugin is made available under.

### `cmd`

**Required** The shell command used to start the plugin.  The launcher uses this command
to start the plugin.

The launcher uses this command to start the plugin.  It will add the following parameters,
as appropriate:

| Parameter                     | Value                            | When      |
| --                            | --                               | --        |
| `--context`                   | DEFAULT_CONTEXT                  | always
| `--authorization-mode`        | OPENTF_AUTHORIZATION_MODE        | if needed
| `--authorization-policy-file` | OPENTF_AUTHORIZATION_POLICY_FILE | if needed
| `-token-auth-file`            | OPENTF_TOKEN_AUTH_FILE           | if needed
| `--trustedkeys-auth-file`     | OPENTF_TRUSTEDKEYS_AUTH_FILE     | if needed
| `--trusted-authorities`       | OPENTF_TRUSTEDKEYS_PATHS         | if needed

<h4>Examples of <code>cmd</code> entries</h4>

Assuming the command to start your plugin is `my-plugin`, your descriptor could contain
the following:

```yaml
cmd: my-plugin foo bar
```

The launcher will start your plugin with the following command:

```bash
my-plugin foo bar --context allinone --trusted-authorities /etc/opentf/trustedkeys
```

If your plugin is a Python module, you can use the `-m` option to start it:

```yaml
cmd: python -m mypackage.myplugin.main
```

The launcher will start your plugin with the following command:

```bash
python -m mypackage.myplugin.main --context allinone --trusted-authorities /etc/opentf/trustedkeys
```

### `spec`

The `spec` section is optional.  It describes the plugin's context parameters, if any.

#### `spec.contextParameters`

A _contextParameters_ section contains a series of _context parameters_.  Context parameters
are runtime parameters an administrator can taylor when deploying the plugin.  They may
have a default value, or bounds.

<h5>Example of <code>contextParameters</code></h5>

```yaml
spec:
  contextParameters:
  - name: max_deadletter_threads
    descriptiveName: deadletter threads pool size
    type: int
    default: 1
    minValue: 1
  - name: max_publication_threads
    descriptiveName: publication threads pool size
    type: int
    default: 10
    minValue: 1
```

##### `spec.contextParameters[*].name`

**Required** The name of the context parameter.  The name must start with a letter or `_`
and contain only alphanumeric characters, `-`, or `_`.  It's this name you will use in
your plugin to refer to the parameter.

It's also this name you will use in your plugin's configuration file, if you want to
overwrite its default value.

##### `spec.contextParameters[*].descriptiveName`

**Required** A human-friendly name for the context parameter.

##### `spec.contextParameters[*].type`

**Optional** The type of the context parameter.  Can be one of: `int`, `string`, or `bool`.

If not specified, it will default to `string`.

##### `spec.contextParameters[*].default`
##### `spec.contextParameters[*].minValue`
##### `spec.contextParameters[*].maxValue`

### `events`

**Required** for `ProviderPlugin` and `GeneratorPlugin` plugins, not used
for other plugin kinds.  The event(s) to subscribe to.

If present, it must be a non-empty list of objects.

Possible entries in the objects are `category`, `categoryPrefix`, and `categoryVersion`.

If `categoryPrefix` is not specified, `category` is required.

`categoryVersion` can only be specified if at least the `category` or
`categoryPrefix` is specified.

<h4>Examples of <code>events</code> sections</h4>

The first example subscribes to `helloworld@v1`, `helloworld@v2`, and `helloworld`
events, but not to `prefix/helloworld@v1`:

```yaml
events:
- category: helloworld
```

This example subscribes to `prefix/foo@v1` and `prefix/bar@v2` events, but
not to `prefix@v1`.

```yaml
events:
- categoryPrefix: prefix
```

This example subscribes to `prefix/foo@v1` and `prefix/bar@v1` events, but
not to `prefix/foo@v2`.

```yaml
events:
- categoryPrefix: prefix
  categoryVersion: v1
```

This last example only subscribes to `prefix/helloworld@v1` and `prefix/helloworld@v2`
events.

```yaml
events:
- categoryPrefix: prefix
  category: helloworld
  categoryVersion: v1
- categoryPrefix: prefix
  category: helloworld
  categoryVersion: v2
```

### `inputs`

**Optional** Input parameters allow you to specify data that the plugin expects
to use at runtime.

!!! note
    For provider plugins, the OpenTestFactory orchestrator stores input parameters
    as environment variables.  Input IDs with uppercase letters are converted to
    lowercase at runtime.  We recommend using lowercase input IDs.

<h4>Example of an <code>inputs</code> section</h4>

This example configures two inputs: `numOctocats` and `octocatEyeColor`.
The `numOctocats` input is not required and will default to a value of '1'.  The
`octocatEyeColor` input is required and has no default value.  Workflow files that
use this plugin must use the `with` keyword to set an input value for
`octocatEyeColor`.  For more information about the `with` syntax, see "[Workflow
syntax for OpenTestFactory orchestrator](../specification/workflows.md)."

```yaml
inputs:
  numOctocats:
    description: 'Number of Octocats'
    required: false
    default: '1'
  octocatEyeColor:
    description: 'Eye color of the Octocats'
    required: true
```

When you specify an input to a plugin in a workflow file or use a default input
value, the orchestrator creates an environment variable for the input with the name
`INPUT_<VARIABLE_NAME>`.  The environment variable created converts input names
to uppercase letters and replaces spaces with `_` characters.

For example, if a workflow defined the `numOctocats` and `octocatEyeColor` inputs,
the function code could read the values of the inputs using the
`INPUT_NUMOCTOCATS` and `INPUT_OCTOCATEYECOLOR` environment variables.

#### `inputs.<input_id>`

**Required** A `string` identifier to associate with the input.  The value of
`<input_id>` is a map of the input's metadata.  The `<input_id>` must be a
unique identifier within the inputs object.  The `<input_id>` must start with a
letter or `_` and contain only alphanumeric characters, `-`, or `_`.

#### `inputs.<input_id>.description`

**Required** A `string` description of the input parameter.

#### `inputs.<input_id>.required`

**Required** A `boolean` to indicate whether the plugin requires the input
parameter.  Set to `true` when the parameter is required.

#### `inputs.<input_id>.default`

**Optional** A `string` representing the default value.  The default value is
used when an input parameter isn't specified in a workflow file.

### `additionalInputs`

**Optional** A `boolean` to indicate whether the plugin allows input parameters not listed in the
`inputs` section.  Set to `true` when extra input parameters are allowed.

<h4>Example of an <code>additionalInputs</code> section</h4>

```yaml hl_lines="6"
inputs:
  numOctocats:
    description: 'Number of Octocats'
    required: true
    default: '1'
additionalInputs: true
```

### `outputs`

**Optional** Output parameters allow you to declare data that a plugin sets.
Plugins that run later in a workflow can use the output data set in previously
run plugins.  For example, if you had a plugin that performed the addition of
two inputs (x + y = z), the plugin could output the sum (z) for other plugins to
use as input.

If you don't declare an output in your plugin metadata file, you can still set
outputs and use them in a workflow.  For more information on setting outputs in
a plugin, see "[Workflow commands for OpenTestFactory orchestrator](../specification/workflow-commands.md)."

<h4>Example of an <code>outputs</code> section</h4>

```yaml
outputs:
  sum: # id of the output
    description: 'The sum of the inputs'
```

#### `outputs.<output_id>`

**Required** A `string` identifier to associate with the output.  The value of
`<output_id>` is a map of the output's metadata.  The `<output_id>` must be a
unique identifier within the outputs object.  The `<output_id>` must start with
a letter or `_` and contain only alphanumeric characters, `-`, or `_`.

#### `outputs.<output_id>.description`

**Required** A `string` description of the output parameter.

### `reports`

**Optional** Report parameters allow you to declare reports that a plugin may produces.

<h4>Example of a <code>reports</code> section</h4>

```yaml
reports:
  'output.xml':
    description: |
      The Robot Framework test execution report in XML.

      It has the `application/vnd.opentestfactory.robotframework-output+xml`
      content type.
  '*-result.json':
    description: |
      JSON result files.

      Contains all `*-result.json` files if the `allure-robotframework`
      library is available on the execution environment, to enable the
      generation of Allure reports.
```

#### `reports.<report_id>`

**Required** A `string` identifier to associate with the report.  The value of
`<report_id>` is a map of the report's metadata.  The `<report_id>` must be a
unique identifier within the `reports` object.  The `<report_id>` must be a filename
or a wildcard pattern.

#### `reports.<report_id>.description`

**Required** A `string` description of the report.

#### `reports.<report_id>.mimeType`

**Optional** A `string` that represents the MIME type of the report.

### `branding`

You can use a color and [Feather](https://feathericons.com/){:target="_blank"} icon to create a
badge to personalize and distinguish your plugin.  Badges are shown next to
your plugin name in the OpenTestFactory Marketplace.

<h4>Example of a <code>branding</code> section</h4>

```yaml
branding:
  icon: 'award'
  color: 'green'
```

#### `branding.color`

The background color of the badge.  Can be one of: `white`, `yellow`, `blue`,
`green`, `orange`, `red`, `purple`, or `gray-dark`.

#### `branding.icon`

The name of the [Feather](https://feathericons.com/){:target="_blank"} icon to use.

|                   |                  |                    |                   |
| --                | --               | --                 | --
| activity          | airplay          | alert-circle       | alert-octagon
| alert-triangle    | align-center     | align-justify      | align-left
| align-right       | anchor           | aperture           | archive
| arrow-down-circle | arrow-down-left  | arrow-down-right   | arrow-down
| arrow-left-circle | arrow-left       | arrow-right-circle | arrow-right
| arrow-up-circle   | arrow-up-left    | arrow-up-right     | arrow-up
| at-sign           | award            | bar-chart-2        | bar-chart
| battery-charging  | battery          | bell-off           | bell
| bluetooth         | bold             | book-open          | book
| bookmark          | box              | briefcase          | calendar
| camera-off        | camera           | cast               | check-circle
| check-square      | check            | chevron-down       | chevron-left
| chevron-right     | chevron-up       | chevrons-down      | chevrons-left
| chevrons-right    | chevrons-up      | circle             | clipboard
| clock             | cloud-drizzle    | cloud-lightning    | cloud-off
| cloud-rain        | cloud-snow       | cloud              | code
| command           | compass          | copy               | corner-down-left
| corner-down-right | corner-left-down | corner-left-up     | corner-right-down
| corner-right-up   | corner-up-left   | corner-up-right    | cpu
| credit-card       | crop             | crosshair          | database
| delete            | disc             | dollar-sign        | download-cloud
| download          | droplet          | edit-2             | edit-3
| edit              | external-link    | eye-off            | eye
| facebook          | fast-forward     | feather            | file-minus
| file-plus         | file-text        | file               | film
| filter            | flag             | folder-minus       | folder-plus
| folder            | gift             | git-branch         | git-commit
| git-merge         | git-pull-request | globe              | grid
| hard-drive        | hash             | headphones         | heart
| help-circle       | home             | image              | inbox
| info              | italic           | layers             | layout
| life-buoy         | link-2           | link               | list
| loader            | lock             | log-in             | log-out
| mail              | map-pin          | map                | maximize-2
| maximize          | menu             | message-circle     | message-square
| mic-off           | mic              | minimize-2         | minimize
| minus-circle      | minus-square     | minus              | monitor
| moon              | more-horizontal  | more-vertical      | move
| music             | navigation-2     | navigation         | octagon
| package           | paperclip        | pause-circle       | pause
| percent           | phone-call       | phone-forwarded    | phone-incoming
| phone-missed      | phone-off        | phone-outgoing     | phone
| pie-chart         | play-circle      | play               | plus-circle
| plus-square       | plus             | pocket             | power
| printer           | radio            | refresh-ccw        | refresh-cw
| repeat            | rewind           | rotate-ccw         | rotate-cw
| rss               | save             | scissors           | search
| send              | server           | settings           | share-2
| share             | shield-off       | shield             | shopping-bag
| shopping-cart     | shuffle          | sidebar            | skip-back
| skip-forward      | slash            | sliders            | smartphone
| speaker           | square           | star               | stop-circle
| sun               | sunrise          | sunset             | tablet
| tag               | target           | terminal           | thermometer
| thumbs-down       | thumbs-up        | toggle-left        | toggle-right
| trash-2           | trash            | trending-down      | trending-up
| triangle          | truck            | tv                 | type
| umbrella          | underline        | unlock             | upload-cloud
| upload            | user-check       | user-minus         | user-plus
| user-x            | user             | users              | video-off
| video             | voicemail        | volume-1           | volume-2
| volume-x          | volume           | watch              | wifi-off
| wifi              | wind             | x-circle           |  x-square
| x                 | zap-off          | zap                | zoom-in
| zoom-out          |                  |                    |
