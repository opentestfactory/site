// Plausible analytics
var script1 = document.createElement('script');
script1.src = "https://plausible.io/js/script.outbound-links.file-downloads.js";
script1.dataset.domain = "opentestfactory.org";
document.head.appendChild(script1);

var script2 = document.createElement('script');
script2.type = 'text/javascript';
var plausibleJavaScript = 'window.plausible = window.plausible || function() { (window.plausible.q = window.plausible.q || []).push(arguments) }';
script2.appendChild(document.createTextNode(plausibleJavaScript))
document.head.appendChild(script2);
