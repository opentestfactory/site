<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Canceling a workflow

You can cancel a workflow run that is in progress.  When you cancel a workflow run,
the orchestrator cancels all jobs and steps that are part of that workflow.

## Canceling a workflow run

1.  Using the OpenTestFactory CLI, list the running workflow runs:

    ```bash
    opentf-ctl get workflows
    ```

    ```text
    WORKFLOW_ID      STATUS   NAME
    <workflow_id_1>  RUNNING  Test Orchestrator with No Initial Environments
    <workflow_id_2>  DONE     Workflow end-to-end
    ```

2.  Cancel the workflow run:

    ```bash
    opentf-ctl kill workflow <workflow_id_1>
    ```

    ```text
    Killing workflow <workflow_id_1>.
    ```

## Steps the orchestrator takes to cancel a workflow run

When canceling a workflow run, you may be running other software that uses resources that are
related to the workflow run.  To help you free up resources related to the workflow run, it may
help to understand the steps the orchestrator performs to cancel a workflow run.

1.  To cancel the workflow run, the orchestrator re-evaluates `if` conditions for all currently
    running jobs.  If the condition evaluates to `true`, the job will not get canceled.  For example,
    the condition `if: always()` would evaluate to true and the job continues to run.  When there
    is no condition, that is the equivalent of the condition `if: success()`, which evaluates
    to `false` for a cancelled workflow.

2.  For jobs that continue to run, the orchestrator re-evaluates `if` conditions for the unfinished
    steps.  If the condition evaluates to `true`, the step continues to run.  You can use the
    `cancelled()` expression to apply a status check to the step.  For more information, see
    "[Expressions](../learn-opentf-orchestrator/expressions.md)."

3.  For jobs that need to be cancelled, the orchestrator waits for the completion of the currently
    running `run` step.  It then marks the job as cancelled and re-evaluates `if` conditions for the
    unfinished steps.  Steps that do not use the `always()` or `cancelled()` status check will be
    skipped.

    Composite steps (`uses` steps) are treated as a single unit.  Consider the following:

    ```yaml
    steps:
      ...
      - uses: acme/my-action@v2     # ->
        with:                       # - run: echo 'foo'
          ...                       # - run: echo 'bar'
      ...
    ```

    If the workflow is cancelled while the `run: echo 'foo'` step is running, the orchestrator will
    cancel the `acme/my-action@v2` step and skip the `run: echo 'bar'` step.

    Conversely, for the following:

    ```yaml
    steps:
      ...
      - uses: acme/my-action@v2     # ->
        with:                       # - run: echo 'foo'
          ...                       # - run: echo 'bar'
        if: always()
      ...
    ```

    If the workflow is cancelled while the `run: echo 'foo'` step is running, due to the
    `always()` condition, the orchestrator will wait for the completion of the `acme/my-action@v2`
    step before cancelling the workflow.  In particular, the `run: echo 'bar'` step will run.
