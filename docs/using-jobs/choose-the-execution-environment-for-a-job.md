<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Choosing the execution environment for a job

Define the type of execution environment that will process a job in your workflow.

## Overview

Use `jobs.<job_id>.runs-on` to define the type of execution environment to run the job on.

-   The destination environment can be either a SSH-hosted execution environment, an agent-based
    execution environment, or an operator-based dynamic execution environment.
-   You can provide `runs-on` as:

    - A single string
    - A single variable containing a string
    - An array of strings, variables containing strings, or a combination of both

-   If you specify an array of strings or variables, your workflow will execute on any execution
    environment that matches all of the specified `runs-on` values.  For example, here the job will
    only run on an execution environment that has the tags `linux`, `x64`, and `gpu`:

    ```yaml
    runs-on: [linux, x64, gpu]
    ```

-   You can mix strings and variables in an array.  For example:

    ```yaml
    variables:
      CHOSEN_OS: linux
    jobs:
      test:
        runs-on: [robotframework, "${{ variables.CHOSEN_OS }}"]
        steps:
        - run: echo Hello world!
    ```

!!! note
    Quotation marks are not required around simple strings like `robotframework`, but they are
    required for expressions like `${{ variables.CHOSEN_OS }}`.

## Choosing the execution environment

To specify an execution environment for your job, configure `runs-on` in your workflow file
with execution environment tags.

Execution environments have an operating system tag (`linux`, `macos`, or `windows`).  They may have
other tags.

Tags can be used to create targeting options for execution environments, such as operating system
or architecture.  We recommend providing an array of tags that begins with the operating
system tag and then includes additional tags as needed.  When you specify an array of tags, job will
be queued on execution environments that have all the tags that you specify.

<h3>Example: Specifying an operating system</h3>

```yaml
runs-on: linux
```

<h3>Example: Using tags for execution environment selection</h3>

```yaml
runs-on: [linux, x64, gpu]
```
