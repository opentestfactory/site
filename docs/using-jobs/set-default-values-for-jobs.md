<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Setting default values for jobs

Define the default settings that will apply to all jobs in the workflow, or all steps in a job.

## Overview

Use `defaults` to create a `map` of default settings that will apply to all jobs in the workflow.
You can also set default settings that are only available for a job.  For more information, see
[`jobs.<job_id>.defaults`](../impl/reference/workflows.md#jobsjob_iddefaults).

When more than one default setting is defined with the same name, the orchestrator uses the most
specific default setting.  For example, a default setting defined in a job will override a default
setting that has the same name defined in a workflow.

<h3>Example: Set the default execution environment</h3>

```yaml
defaults:
  runs-on: linux
```

## Setting default shell and working directory

You can use `defaults.run` to provide default `shell` and `working-directory` options for all `run`
steps in a workflow.  You can also set default settings for `run` that are only available to a job.
For more information, see [`jobs.<job_id>.defaults.run`](../impl/reference/workflows.md#jobsjob_iddefaultsrun).

When more than one default setting is defined with the same name, the orchestrator uses the most
specific default setting.  For example, a default setting defined in a job will override a default
setting that has the same name defined in a workflow.

<h3>Example: Set the default shell and working directory</h3>

```yaml
defaults:
  run:
    shell: bash
    working-directory: ./scripts
```

## Setting default values for a specific job

Use `jobs.<job_id>.defaults` to create a `map` of default settings that will apply to all steps in
the job.  You can also set default settings for the entire workflow.  For more information, see
[`defaults`](../impl/reference/workflows.md#defaults).

When more than one default setting is defined with the same name, the orchestrator uses the most
specific default setting.  For example, a default setting defined in a job will override a default
setting that has the same name defined in a workflow.

## Setting default shell and working directory for a job

Use `jobs.<job_id>.defaults.run` to provide default `shell` and `working-directory` to all `run`
steps in the job.

You can provide default `shell` and `working-directory` options for all `run` steps in a job.  You
can also sete default settings for `run` for the entire workflow.  For more information, see
[`defaults.run`](../impl/reference/workflows.md#defaultsrun).

When more than one default setting is defined with the same name, the orchestrator uses the most
specific default setting.  For example, a default setting defined in a job will override a default
setting that has the same name defined in a workflow.

<h3>Example: Setting default run step options for a job</h3>

```yaml
jobs:
  job1:
    runs-on: linux
    defaults:
      run:
        shell: bash
        working-directory: ./scripts
```
