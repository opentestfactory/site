<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Using jobs

## [Using jobs in a workflow](use-jobs-in-a-workflow.md)

Use workflows to run multiple jobs.

## [Choosing the execution environment for a job](choose-the-execution-environment-for-a-job.md)

Define the type of execution environment that will process a job in your workflow.

## [Using conditions to control job execution](use-conditions-to-control-job-execution.md)

Prevent a job from running unless your conditions are met.

## [Setting default values for jobs](set-default-values-for-jobs.md)

Define the default settings that will apply to all jobs in the workflow, or all steps in a job.

## [Using hooks to customize job executions](use-hooks-to-customize-job-execution.md)

Use hooks to override the default behavior of a job or of all jobs in a workflow, or to
customize the behavior of functions.

## [Defining outputs for jobs](define-outputs-for-jobs.md)

Create a map of outputs for your jobs
