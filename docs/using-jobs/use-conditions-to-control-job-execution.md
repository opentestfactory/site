<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Using conditions to control job execution

Prevent a job from running unless your conditions are met.

## Overview

You can use the `jobs.<job_id>.if` conditional to prevent a job from running unless a condition
is met.  You can use any supported context and expression to create a conditionals.  For more
information on which contexts are supported in this key, see "[Contexts](../learn-opentf-orchestrator/contexts.md#context-availability)."

When you use expressions in an `if` conditional, you can, optionaly, omit the `${{ }}` expression
syntax because the orchestrator automatically evaluates the `if` conditional as an expression.  However,
this exception does not apply everywhere.

You must always use the `${{ }}` expression syntax or escape with `''`, `""`, or `()` when the
expression starts with `!`, since `!` is a reserved notation in YAML format.  For example:

```yaml
if: ${{ ! startsWith(opentf.actor, 'ci-') }}
```

For more information, see "[Expressions](../learn-opentf-orchestrator/expressions.md)."

<h3>Example: Only run job for specified actor</h3>

This example used `if` to control when the `production-deploy` job can run.  It will only run if
the user or trigger token is named `production-deployer`.  Otherwise, the job will be marked as
_skipped_.

```yaml
metadata:
  name: example workflow
jobs:
  production-deploy:
    if: opentf.actor == 'production-deployer'
    runs-on: linux
    steps:
    - uses: actions/checkout@v2
      with:
        repository: https://git.example.com/foo.git
    - run: ./deploy.sh
```

!!! note
    In some parts of the workflow you cannot use environment variables.  Instead you can use
    contexts to access the value of an environment variable.  For more information, see
    "[Variables](../learn-opentf-orchestrator/variables.md)."
