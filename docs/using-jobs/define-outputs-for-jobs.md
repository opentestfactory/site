<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Defining outputs for jobs

Create a map of outputs for your jobs

## Overview

You can use `jobs.<job_id>.outputs` to create a `map` of outputs for a job.  Job outputs are
available to all downstream jobs that depend on this job.  For more information on defining
job dependencies, see [`jobs.<job_id>.needs`](../impl/reference/workflows.md#jobsjob_idneeds).

Outputs are Unicode strings.

Jobs outputs containing expressions are evaluated at the end of each jobs.

To use outputs in a dependent job, you can use the `needs` context.  For more information, see
"[Contexts](../impl/reference/contexts.md#needs-context)."

<h3>Example: Defining outputs for a job</h3>

```yaml
jobs:
  job1:
    runs-on: lunix
    # Map a step output to a job output
    outputs:
      output1: ${{ steps.step1.outputs.test }}
      output2: ${{ steps.step2.outputs.test }}
    steps:
      - id: step1
        run: echo "::set-output name=test::hello"
      - id: step2
        run: echo "::set-output name=test::world"
  job2:
    runs-on: linux
    needs: job1
    steps:
    - variables:
      OUTPUT1: ${{needs.job1.outputs.output1}}
      OUTPUT2: ${{needs.job1.outputs.output2}}
      run: echo "$OUTPUT1 $OUTPUT2"
```
