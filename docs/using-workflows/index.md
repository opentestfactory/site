<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Using workflows


## [About workflows](about-workflows.md)

Get a high-level overview of OpenTestFactory workflows, including triggers, syntax, and advanced features.

## [Triggering a workflow](trigger-a-workflow.md)

How to automatically trigger OpenTestFactory orchestrator workflows.

## [Manually running a workflow](manually-run-a-workflow.md)

You can run a workflow using the opentf-ctl CLI or the REST API.

## [Workflow syntax](workflows.md)

A workflow is a configurable automated process made up of one or more jobs. You must create a YAML file to define your workflow configuration.

## [Workflow commands](workflow-commands.md)

You can use workflow commands when running shell commands in a workflow or in a plugin’s code.

## [Using the OpenTestFactory orchestrator CLI](opentf-cli.md)

You can use the OpenTestFactory orchestrator CLI to interact with the orchestrator and perform various tasks.
