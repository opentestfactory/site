<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Triggering a workflow

How to automatically trigger OpenTestFactory orchestrator workflows.

## About workflow triggers

Workflow triggers are events that cause a workflow to run.  These events can be:

- Activities that originate from you CI tool chain
- Scheduled times
- Manual

For example, you can configure your CI to run a workflow when a push is made to the default branch
of your repository, when a release is created, or when an issue is opened.

## Using a CI to trigger a workflow

- [Integrate with GitHub actions](../guides/github-ci.md)
- [Integrate with GitLab CI](../guides/gitlab-ci.md)
- [Integrate with Jenkins](../guides/jenkins-ci.md)
