<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# About workflows

Get a high-level overview of OpenTestFactory workflows, including triggers, syntax, and advanced
features.

## About workflows

A workflow is a configurable automated process that will run one or more jobs.  Workflows are
defined by a YAML file typically checked in to a repository.

Workflows are defined in the `.opentf/workflows` directory in a repository, and a repository can
have multiple workflows, each of which can perform a different set of tasks.  For example, you
can have one workflow to test pull requests, another workflow to test your application every time
a release is created, and still another workflow that runs specific tests every time someone adds
a label.

## Workflow basics

A workflow must contain the following basic components:

1. A _name_.
2. One or more _jobs_, each of which will either execute on an _execution environment_ and run a series one or more _steps_ or execute another series of jobs.

For more information on these basic components, see "[Understanding OpenTestFactory Orchestrator](../learn-opentf-orchestrator/introduction-to-opentf-orchestrator.md)."

![Components](../resources/workflow_expanded.png#only-light)
![Components](../resources/workflow_expanded_dark.png#only-dark)

## Triggering a workflow

Workflow triggers are events that cause a workflow to run.  These events can be:

- Activities that originate from you CI tool chain
- Scheduled times
- Manual

For example, you can configure your CI to run a workflow when a push is made to the default branch
of your repository, when a release is created, or when an issue is opened.

For more information, see "[Triggering a workflow](trigger-a-workflow.md)."

## Workflow syntax

Workflow are defined using YAML.  For the full reference of the YAML syntax for authoring workflows,
see "[Workflow syntax](workflows.md)."

## Create an example workflow

The orchestrator uses YAML syntax to define the workflow.  Each workflow is stored as a
separate YAML file in your code repository, in a directory named `.opentf/workflows`.

1. In your repository, create the `.opentf/workflows` directory to store your workflow
   files.

2. In the `.opentf/workflows` directory, create a new file called
   `learn-opentf-orchestrator.yaml` and add the following code.

    ```yaml linenums="1" title=".opentf/workflows/learn-opentf-orchestrator.yaml"
    metadata:
      name: learn-opentf-orchestrator
    jobs:
      my-first-job:
        runs-on: [linux, robotframework]
        steps:
        - uses: actions/checkout@v2
          with:
            repository: https://github.com/robotframework/RobotDemo.git
        - uses: robotframework/robot@v1
          with:
            datasource: RobotDemo/keyboard_driven.robot
        - run: ls -l

    ```

3. Commit these changes and push them to your repository.

Your new OpenTestFactory orchestrator workflow file is now installed in your repository and will
run each time someone triggers the orchestrator on your repository.

## Understanding the workflow file

To help you understand how YAML syntax is used to create a workflow file, this section
explains each line of the introduction's example:

| Line | Content                             | Explanation
| --   | ---                                 | --
| (1)  | `metadata:`                         | Groups together the metadata elements.
| (2)  | `name: learn-opentf-orchestrator`   | The name of the workflow.
| (3)  | `jobs:`                             | Groups together all the jobs that run in the `learn-opentf-orchestrator` workflow file.
| (4)  | `my-first-job:`                     | Defines the name of the `my-first-hob` job stored within the `jobs` section.
| (5)  | `runs-on: [linux, robotframework]`  | Configures the job to run on an execution environment that has the `linux` and `robotframework` tags (it may have other tags too).  If there is no such execution environment available, the orchestrator waits until one become available.<br/>The workflow fails if none is found (after a configurable delay).
| (6)  | `steps:`                            | Groups together all the steps that run in the `my-first-job` job.  Each item nested under this section is a separate function or shell command.
| (7)  | `uses: actions/checkout@v2`         | The `uses` keyword tells the job to retrieve `v2` of the function named `actions/checkout@v2`. This is a function that checks out your repository and downloads it to the execution environment, allowing you to run actions against your code (such as testing tools). You must use the checkout function any time your workflow will run against the repository's code.
| (8)  | `with:`                             | The `with` keyword provides parameters to the function.
| (9)  | `repository: https://github.com/robotframework/RobotDemo.git` | The `actions/checkout@v2` function has a `repository` parameter that specifies the repository to check out.
| (10) | `uses: robotframework/robot@v1`     | This function runs the specified Robot Framework datasource.
| (11) | `with:`                             | The `with` keyword provides parameters to the function.
| (12) | `datasource: RobotDemo/keyboard_driven.robot` | The `roboframework/robot@v1` function has a `datasource` parameter that specifies the datasource to use.
| (13) | `run: ls -l`                        | The `run` keyword tells the job to execute a script on the execution environment.  In this case, you are using `ls` to show the content of the current directory.

## Advanced workflow features

This section briefly describes some of the advanced features of the OpenTestFactory orchestrator that help
you create more complex workflows

### Creating dependent jobs

By default, the jobs in your workflow all run in parallel at the same time.  If you have a job that
must only run after another job has completed, you can use the `needs` keyword to create this
dependency.  If one of the jobs fails, all dependent jobs are skipped; however, if you need the
jobs to continue, you can define this using the `if` conditional statement.

In this example, the `setup`, `build`, and `test` jobs run in series, with `build` and `test` being
dependent on the successful completion of the job that precedes them:

```yaml
jobs:
  setup:
    runs-on: linux
    steps:
    - run: ./setup_server.sh
  build:
    needs: setup
    runs-on: linux
    steps:
    - run: ./build_server.sh
  test:
    needs: build
    runs-on: linux
    steps:
    - run: ./test_server.sh
```

For more information, see "[Using jobs in a workflow](../using-jobs/use-jobs-in-a-workflow.md)."

### Using tags to route workflows

If you want to be sure that a particular type of execution environment will process your job, you can
use tags to control where jobs are executed.  You can assign tags to execution environments in addition
to their default OS tag.  Then, you can refer to these labels in your YAML workflow, ensuring that the
job is routed in a predictable way.

This example shows how a workflow can use tags to specify the required execution environment:

```yaml
jobs:
  example-job:
    runs-on: [linux, x64, gpu]
```

A workflow will only run on an execution environment that has all the tags in the `runs-on` array.  The
job will preferentially go to an idle execution environment with the specified tags.  If none are
available, the job will wait.

For more information, see "[Choosing the execution environment for a job](../using-jobs/choose-the-execution-environment-for-a-job.md)."

### Using hooks to customize job execution

You can use hooks to customize functions or specify actions that should occur at specific points in
the job life-cycle.

<h4>Example of function hooks</h4>

![Function hooks](../resources/function_hooks.png#only-light)
![Function hooks](../resources/function_hooks_dark.png#only-dark)

<h4>Example of job hooks</h4>

![Job hooks](../resources/job_hooks.png#only-light)
![Job hooks](../resources/job_hooks_dark.png#only-dark)

For more information, see "[Using hooks to customize job execution](../using-jobs/use-hooks-to-customize-job-execution.md)."
