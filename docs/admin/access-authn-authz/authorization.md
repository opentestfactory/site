<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Authorization

Learn about OpenTestFactory Orchestrator authorization, including details about
creating policies using the supported authorization modules.

With the OpenTestFactory orchestrator, you must be authenticated (logged in)
before your request can be authorized (granted permission to access).  For
information about authentication, see [Authenticating](authentication.md).

The OpenTestFactory orchestrator expects attributes that are common to
REST API requests.  This means that the orchestrator authorization works
with existing organization-wide or cloud-provider-wide access control systems
which may handle other APIs besides the OpenTestFactory orchestrator API.

## Determine Whether a Request is Allowed or Denied

The orchestrator evaluates all the request attributes against all policies and
allows or denies the request.  All parts of an API request must be allowed by
some policy to proceed.  This means that permissions are denied by default.

When multiple authorization modules are configured, each is checked in sequence.
If any authorizer approves or denies a request, that decision is immediately returned,
and no other authorizer is consulted.  If all modules have no opinion on the request,
then the request is denied.  A denial returns an HTTP status code 403.

## Authorization Modes

The OpenTestFactory orchestrator may authorize a request using one of several authorization
modes:

- **JWT signature** - A special-purpose authorization mode that grants permissions to
  namespaces based on the key used to sign the token.  To learn more about using
  JWT signature authorization mode, see "[Signature-based Access Control](jwt.md)."
- **ABAC** - Attribute-based access control (ABAC) defines an access control paradigm whereby
  access rights are granted to users through the use of policies that combine attributes
  together.  The policies can use any type of attribute (user attributes, resource attributes,
  objects, etc.).  To learn more about using the ABAC mode, see "[Attribute-based Access Control](abac.md)."

## Using Flags for Your Authorization Module

You must include a flag to indicate which authorization module your
policies include.

The following flags can be used:

- `--authorization-mode=JWT`
- `--authorization-mode=ABAC`
- `--authorization-mode=JWT,ABAC`
- `--authorization-mode=ABAC,JWT`

!!! Info
    If you are using one of the provided Docker images, use the
    `OPENTF_AUTHORIZATION_MODE` environment
    variable instead.  The setting will apply to every service launched
    by the image.

    If you only want to use the JWT authorization module (the default), you
    can define the following environment variable or leave it undefined.

    `OPENTF_AUTHORIZATION_MODE=JWT`

    If you want to use the ABAC and JWT authorization modules, you can define
    the following environment variable.

    `OPENTF_AUTHORIZATION_MODE=ABAC,JWT`

!!! Important
    Some legacy services require the JWT signature authorization mode.  The
    current orchestrator implementation adds `JWT` to the authorization mode
    if not already specified.  So, specifying `ABAC` is interpreted as `ABAC,JWT`,
    and specifying nothing is interpreted as `JWT`.  This may change in a future
    release.

You can choose more than one authorization module.  Modules are checked in order, so an earlier
module has higher priority to allow or deny a request.
