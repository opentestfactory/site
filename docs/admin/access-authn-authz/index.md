<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Identity and Access Management

Documentation and guides related to authentication and authorization.

[Overview](overview.md){: .md-button .md-button--primary }

## Guides

- [Namespaces and Permissions](enable-namespaces.md)
- [Authentication](authentication.md)
- [Authorization](authorization.md)
