<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Administration

Documentation and guides for enterprise administrators, system administrators,
and specialists who deploy, configure, and manage an OpenTestFactory orchestrator.

[Overview](overview.md){: .md-button .md-button--primary }
[Releases](../otf/releaseScheduleAndHistory.md){: .md-button }

## Guides

- [Installing OpenTestFactory Orchestrator on Docker](../installation.md)
- [Installing OpenTestFactory Orchestrator on docker-compose](../guides/docker-compose.md)
- [Installing OpenTestFactory Orchestrator on Kubernetes](../guides/kubernetes.md)

- [Running commands (`opentf-ctl`)](../tools/opentf-ctl/index.md)

## Popular

- [Installing OpenTestFactory Orchestrator on Kubernetes](../guides/kubernetes.md)
- [Common Provider Settings](../plugins/configuring-a-provider-plugin.md)
- [Surefire Parser Service](../services/surefireparser.md)

## What's new

- [Identity and Access Management Overview](access-authn-authz/overview.md)
- [User-facing Endpoints](../services/endpoints.md)
