<!--
Copyright (c) 2025 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Metrics

This section documents the metrics exposed by the orchestrator services and plugins.

## Services exposing metrics

- [Arranger service](#arranger-service)
- [EventBus service](#eventbus-service)
- [Agent channel plugin](#agent-channel-plugin)
- [SSH channel plugin](#ssh-channel-plugin)

### Arranger service

The arranger service exposes various counters and gauges related to workflows, 
jobs and commands execution.

All metrics names are prefixed with `opentf.orchestrator.arranger`.

`.commands` counters count produced `run` steps, thus relying on `ExecutionCommand` 
and `ExecutionResult` events.

|  Name  |  Kind  |  Description  | Attributes |
|--------|--------|---------------|------------|
| `.workflows.received`        | `Counter`         | Received workflows count                 | None                    |
| `.workflows.running`         | `UpDownCounter`   | Running workflows count                  | None                    |
| `.workflows.completed`       | `Counter`         | Completed workflows count                | None                    |
| `.workflows.pending_running` | `ObservableGauge` | Pending to running workflows ratio gauge | None                    |
| `.jobs.started`              | `Counter`         | Initialized jobs count                   | `workflow_id`           |
| `.jobs.running`              | `UpDownCounter`   | Running jobs count                       | `workflow_id`           |
| `.jobs.completed`            | `Counter`         | Completed jobs count                     | `workflow_id`           |
| `.jobs.running_pending`      | `ObservableGauge` | Running to pending jobs ratio gauge      | None                    |
| `.commands.started`             | `Counter`         | Started commands count                      | `workflow_id`, `job_id` |
| `.commands.running`             | `UpDownCounter`   | Running commands count                      | `workflow_id`, `job_id` |
| `.commands.completed`           | `Counter`         | Completed commands count                    | `workflow_id`, `job_id` |

### EventBus service

The event bus service exposes various counters related to subscriptions 
and publications handling.

All metrics names are prefixed with `opentf.orchestrator.eventbus`.

|  Name  |  Kind  |  Description  | Attributes |
|--------|--------|---------------|------------|
| `.publications.sent`                        | `Counter` | Sent publications count                                 | None                        |
| `.publications.received`                    | `Counter` | Received publications count                             | None                        |
| `.subscriptions_publications`               | `Counter` | Publications count per subscription                     | `subscription_name`         |
| `.publications.http.responses.status_code`  | `Counter` | Publications HTTP response codes global counts          | `http.response.status_code` |
| `.subscriptions.http.responses.status_code` | `Counter` | Publications HTTP response codes count per subscription | `subscription_name`, `http.response.status_code` |

### Agent Channel plugin

The agent channel plugin exposes counters related to agent registrations, 
environment requests, and channel statuses. 

All metrics names are prefixed with `opentf.orchestrator.agentchannel`.

|  Name  |  Kind  |  Description  | Attributes |
|--------|--------|---------------|------------|
| `.registrations`         | `Counter`       | Registered agents count             | None  |
| `.deregistrations`       | `Counter`       | De-registered agents count          | None  |
| `.registrations.current` | `UpDownCounter` | Currently registered agents count   | None  |
| `.channels.requests`     | `Counter`       | Received environment requests count | None  |
| `.channels.offers`       | `Counter`       | Provided environment offers count   | None  |
| `.channels.busy`         | `UpDownCounter` | Busy agents count                   | None  |
| `.channels.idle`         | `UpDownCounter` | Idle agents count                   | None  |
| `.channels.pending`      | `UpDownCounter` | Pending agents count                | None  |
| `.channels.unreachable`  | `UpDownCounter` | Unreachable agents count            | None  |
| `.channels.tags`         | `UpDownCounter` | Registered agents per tag count     | `tag` |

### SSH Channel plugin

The SSH channel plugin exposes counter related to available environments, environment 
requests, and channel statuses.

All metrics names are prefixed with `opentf.orchestrator.sshchannel`.

|  Name  |  Kind  |  Description  | Attributes |
|--------|--------|---------------|------------|
| `.hosts.current`        | `UpDownCounter` | Current hosts count                 | None  |
| `.hosts.tags`           | `UpDownCounter` | Added hosts per tag count           | `tag` |
| `.channels.requests`    | `Counter`       | Received environment requests count | None  |
| `.channels.offers`      | `Counter`       | Produced environment offers count   | None  |
| `.channels.busy`        | `UpDownCounter` | Busy channels count                 | None  |
| `.channels.idle`        | `UpDownCounter` | Idle channels count                 | None  |
| `.channels.pending`     | `UpDownCounter` | Pending channels count              | None  |
| `.channels.unreachable` | `UpDownCounter` | Unreachable channels count          | None  |