<!--
Copyright (c) 2025 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Using Telemetry

The OpenTestFactory orchestrator provides various OpenTelemetry metrics that 
can be used to monitor services, track performance, and gain insights into system 
behavior. 

## Enabling Telemetry

To enable telemetry, the environment variable `OPENTF_TELEMETRY` should 
be set to `yes`, `1`, `true` or `on` when the orchestrator is launched.

Currently, metrics are implemented for arranger and eventbus services, 
and both channel handler plugins. A complete list of available metrics 
is provided on the [Metrics](metrics.md) page.

## OpenTelemetry Collector

The orchestrator services using OpenTelemetry send collected metrics data to the 
[OpenTelemetry collector](https://opentelemetry.io/docs/collector/){ target="_blank" } 
included in the `allinone` image.

By default, OpenTelemetry Collector pipeline is configured with a `prometheus` exporter, 
exposing collected metrics on port `9464`. You can collect them using 
[Prometheus](https://prometheus.io/docs/prometheus/latest/installation/){ target="_blank" } 
or any other scraper that supports this format.

OpenTelemetry Collector default configuration file (`otelcol-config.yaml`) is available 
in the `/app` directory, providing a basic pipeline. You can modify it at your convenience.

The `otelcol` binary is launched at the orchestrator's startup with the following options:

```bash
/usr/local/bin/otelcol --config=file:/app/otelcol-config.yaml $OTELCOL_EXTRA_OPTIONS
```

You can use `$OTELCOL_EXTRA_OPTIONS` environment variable to pass to the `otelcol`  
extra command line options. Some examples and details on the collector configuration 
can be found [in the OpenTelemetry Collector documentation](https://opentelemetry.io/docs/collector/configuration){ target="_blank" }.