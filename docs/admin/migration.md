<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Migration guide

## 2023-07

### New user-facing endpoint, `/workflows/{workflow_id}/qualitygate` (POST), added to the quality gate service

This new endpoint allows clients to trigger a quality gate evaluation for a given workflow
with their own rules.

The service was already exposing this route using the `GET` method, but it now also expose
this route using the `POST` method.  This enables users to specify their own quality gate
rules.

### Who is impacted?

If you have defined a reverse proxy in front of your orchestrator, with routes filtering on
HTTP methods, you will have to adjust their definitions to allow for the new method.

If you do not filter routes based on HTTP methods, you probably do not have to update
your reverse proxy configuration.

Please refer to "[Quality gate](../services/qualitygate.md)" for more information.

## 2023-03

### New user-facing endpoint, `/namespaces`, added to the observer service

This new endpoint allows clients to check whether they can perform a specified operation in
a given [namespace](../guides/namespaces.md).

#### Who is impacted?

If you have defined a reverse proxy in front of your orchestrator, you will have to adjust
your reverse proxy implementation to be able to access this new endpoint.

Please refer to "[User-facing Endpoints](../services/endpoints.md)" for more information.

## 2022-07

### Provider configuration file format has changed

In previous releases, provider services configuration files were of the type `ProviderConfig`
or `ServiceConfig` and were having an `apiVersion` of `opentestfactory.org/v1alpha1`.

The configuration file format has changed, and they must now have the following format:

```yaml
apiVersion: opentestfactory.org/v1beta1
kind: ProviderConfig
```

#### Who is impacted?

If you had defined hooks for a given provider at the orchestrator level, you will have
to adjust your configuration.

You can still edit your provider configuration file so that it includes your hooks, but
the recommended way is now to use a separate file.

Please refer to "[Defining hooks using a definition file](../plugins/configuring-a-provider-plugin.md#defining-hooks-using-a-definition-file)"
for more information.
