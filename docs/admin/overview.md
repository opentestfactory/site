<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Overview

The OpenTestFactory orchestrator is the central element of your testing factory.  It receives commands from users,
continuous integration toolchains, and test management systems.  It handles communication with execution
environments and reports managers.

```mermaid
graph LR
    User --> B
    C[CI] --> B
    A[Test Management System] <-.-> B(Orchestrator)
    B --> R[Reports manager]
    B --> D[Execution Environment]
    B --> E[Execution Environment]
    B --> F[Execution Environment]
    D -.-> G[(Git repositories)]
    E -.-> G
    F -.-> G
    A -.-> G
    B --> S[(Settings)]
    style B fill:#eaa07b
```

