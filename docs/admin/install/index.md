<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Installing the OpenTestFactory orchestrator

System administrators and operations and security specialists can install the OpenTestFactory orchestrator.

## The 'allinone' image

- [Using the 'allinone' image](../../installation.md)

## Docker

- [Installing OpenTestFactory Orchestrator on Docker](docker.md)

## `docker-compose`

- [Installing OpenTestFactory Orchestrator on docker-compose](../../guides/docker-compose.md)

## Kubernetes

- [Installing OpenTestFactory Orchestrator on Kubernetes](../../guides/kubernetes.md)
