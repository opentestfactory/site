<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Configuration

Most services do not require configuration, but a couple of them cannot be used
if you do not configure them:

- the [SSH channel](sshee.md) plugin, which is used to communicate with _execution
  environments_ via SSH (you can use agent-based execution environments instead if
  you prefer)
- the [S3 publisher](s3publisher.md) plugin and the [Local publisher](localpublisher.md)
  plugin, which require credentials and a hint of where they should publish the
  results produced by your workflows

You should also choose the services you want to start on a given orchestrator
instance.  It is a good practice to disable the services you do not need.

## Configuration files location

On the OpenTestFactory orchestrator 'allinone' image, the configuration files are
in the `/app` directory.

There is a `/app/squashtf.yaml` instance configuration file which is where you
specify your plugins' location and the services you want to be disabled.

There is also one main configuration file (`{name}.yaml`) per service in the `/app/conf`
subdirectory.  Some services may use additional configuration files.

Each service has a unique name.  The name of the service matches the name of the
main configuration file (minus the extension), and this name is also the name you
use if you want to disable a service.

If you use a third-party distribution, those configuration files may be somewhere
else.  Please refer to your distribution documentation for more information.

## Instance configuration

The instance configuration file specifies the event bus to use, the core services
location, and the plugins locations.

The default instance configuration file is as follows:

```yaml title="/app/squashtf.yaml" linenums="1" hl_lines="10"
eventbus: python3 -m opentf.core.eventbus
services:
- ${{ CORE }}/core
plugins:
- ${{ CORE }}/plugins
- /app/plugins
disabled:
- dummyee
- HelloWorld
- localpublisher
```

The `${{ CORE }}` variable points to the actual location of the OpenTestFactory
orchestrator distribution.

### Core services

The `eventbus` section specifies how to start the event bus.  This specification is
the command that is used.

The `services` list specifies the location of the core services.  It is a list
of directories.  The launcher scans those directories in order,
looking for service descriptors (`service.yaml` files).

### Plugins

The `plugins` list specifies the location to scan for plugins.  It is a list of directories.
The launcher scans those directories in order, looking for plugin descriptors
(`plugin.yaml` files).

??? note "Adding new plugins"
    If you want to add additional plugins to your installation, you can attach a volume and
    add its mount point to the `plugins` list.  It should be a fully qualified
    directory path.

    You may attach more than one volume.

    !!! tip
        You can use the `/app/plugins` mount point which is empty in the standard
        distribution and is scanned by the launcher by default.

### Disabled list

The `disabled` list specifies the names of services and plugins that should not be started.  If a plugin
descriptor with a matching `.metadata.name` is found, irrespective of the case, it will be
skipped.

The Local publisher plugin, part of the default OpenTestFactory orchestrator
distribution, is for example disabled by default (its name is in the default `disabled` list):

```yaml title="plugin.yaml" hl_lines="5"
---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'PublicationPlugin'
metadata:
  name: 'localpublisher'
  description: 'basic publication plugin, dump reports in directory'
```

The list may include names that have no matching plugin descriptors.

??? info "Services & Plugins included in the OpenTestFactory orchestrator distribution"

    Here is the list of the plugins included in the 'allinone' image.

    | Name                   | Type            | Description
    | ---                    | ---             | ---
    | agentchannel           | Channel handler | Handle agent-based execution environments
    | inceptionee            | Channel handler | Handle the 'inception' execution environment
    | sshchannel             | Channel handler | Handle SSH-based execution environments
    | actionprovider         | Provider        | Provide common Workflow functions
    | cucumber               | Provider        | Handle Cucumber interactions
    | cypress                | Provider        | Handle Cypress interactions
    | junit                  | Provider        | Handle JUnit interactions
    | playwright             | Provider        | Handle Playwright interactions
    | postman                | Provider        | Handle Postman interactions
    | robotframework         | Provider        | Handle Robot Framework interactions
    | skf                    | Provider        | Handle SKF (Squash Keyword Framework) interactions
    | soapui                 | Provider        | Handle SoapUI interactions
    | allure.collector       | Collector       | Handle Allure Report generation
    | result.aggregator      | Collector       | (Allure Report helper service)
    | insightcollector       | Collector       | Handle Execution log generation
    | s3publisher            | Publisher       | Publish results to a S3 bucket
    | localpublisher         | Publisher       | Publish results to a local directory
    | cucumber.interpreter   | Parser          | Parse Cucumber reports
    | cypress.interpreter    | Parser          | Parse Cypress reports
    | postman.interpreter    | Parser          | Parse Postman reports
    | robot.interpreter      | Parser          | Parse Robot Framework reports
    | surefire.interpreter   | Parser          | Parse Surefire reports (JUnit, SKF, SoapUI)

    The _actionprovider_ plugin is used by most provider plugins, disabling it may break
    them.

    The default image also includes a service that can be disabled if you have no use
    for it.  This service consumes events produced by report parsers.

    | Name                   | Description
    | ---                    | ---
    | QualityGate            | The Quality Gate service

    The core services 

    | Name                   | Description
    | ---                    | ---
    | eventbus               | The Event Bus
    | arranger               | Handle workflow execution
    | killswitch             | Kill running workflows
    | localstore             | Temporarily store attachments and clean them up
    | observer               | Provide observability for running and recently-completed workflows
    | receptionist           | Handle incoming workflows
    | rewriter               | Handle schema conversions for not up-to-date plugins

    !!! Warning
        Some core services should never be disabled.  They are the `arranger`, `observer`,
        `receptionist`, `eventbus`, `rewriter`, and `localstore` services.


## Services & Plugins

### Eventbus

By default, the event bus, used by all services, listens on port 38368.  If you change
this default port, you must adjust the [default configuration](config.md) of all other used
services and plugins.


- [Eventbus](eventbus.md) <small>(number of concurrent publication threads, maximum number of retries)</small>

### Core services

Some limits and retention periods can be overridden for the core services.

- [Arranger](arranger.md) <small>(maximum number of jobs and steps in a workflow, maximum number of simultaneous jobs in a workflow, maximum workflow execution time)</small>
- [Observer](observer.md) <small>(retention period)</small>
- [Killswitch](killswitch.md)
- [Receptionist](receptionist.md)

### Plugins

#### Channels

A pool must be specified for the SSH channel plugin.  There is no specific configuration
for the Agent channel plugin.

- [Agent Channel](agentchannel.md)
- [SSH Channel](sshee.md) <small>(targets, pools)</small>

#### Providers

Provider plugins support global hooks (hooks defined at the orchestrator level).
You may want to define some such hooks, to adjust them to some practices that are
specific to your environment but common to all your users.  They can also be used
to enforce security or monitoring measures.

See "[Common provider settings](../plugins/configuring-a-provider-plugin.md)"
for more information.

#### Publishers

A target must be specified for the Local publisher plugin and a set of credentials must be
provided for the S3 publisher plugin.

- [Local Publisher](localpublisher.md) <small>(target)</small>
- [S3 Publisher](s3publisher.md) <small>(credentials, bucket)</small>
