<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Local publisher plugin

This publisher plugin uploads reports and attachments to a directory, which may
be mounted.

It is disabled by default as it requires you to define a target directory.  Please
refer to "[Instance configuration](index.md#disabled-list)" for more information on
how to enable or disable a plugin.

It exposes no user-facing endpoints.

There is [one service-specific](#target) configuration file option that must be specified.

## Environment variables

You can set the `LOCALPUBLISHER_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment variables
to `DEBUG` to add additional information in the console for the launched service. It defaults to
`INFO`. (Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons of logs.)

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those values
are from the most verbose, `NOTSET`, which shows all logs, to the least verbose, `FATAL`, which
only shows fatal errors.

If `LOCALPUBLISHER_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or `INFO` if
`DEBUG_LEVEL` is not defined either).

Access logs are only shown at `NOTSET` and `DEBUG` levels.

## Configuration file

This plugin has a configuration file (`localpublisher.yaml` by default) that describes
the common service configuration options.

If no configuration file is found it will default to the following values:

```yaml hl_lines="13"
apiVersion: opentestfactory.org/v1beta2
kind: ServiceConfig
current-context: default
contexts:
- name: default
  context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: invalid-token
    target: /app/data
```

The configuration included in the 'allinone' image is described in "[Common settings](config.md#configuration-file)."  The
listening port is 7783 and the bind address is `127.0.0.1` as the service exposes no user-facing endpoints.

There is one service-specific configuration option besides the common ones that must be specified, _target_.

### Target

The `target` entry in the context specifies a target directory.  Attachments will be
stored in this directory, under a `{workflow_id}/{filename}` key.

This directory is typically a mount point, linked to an external volume.

## Subscriptions

The local publisher plugin subscribes to the following events:

| `kind`              | `apiVersion`                 |
| ------------------- | ---------------------------- |
| `ExecutionResult`   | opentestfactory.org/v1alpha1 |
| `WorkflowResult`    | opentestfactory.org/v1alpha1 |

The local publisher plugin exposes an `/inbox` endpoint that is used by the event bus to post relevant events.

## Launch command

If you want to manually start the local publisher plugin, use the following command:

```shell
python -m opentf.plugins.localpublisher.main [--context context] [--config configfile]
```

Additional command-line options are available and described in "[Command-line options](config.md#command-line-options)."
