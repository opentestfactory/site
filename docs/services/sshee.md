<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# SSH channel plugin

This channel plugin manages communications with _execution environments_ that
are accessed via SSH.

It exposes no user-facing endpoints.

[Two service-specific](#pools) configuration file options must be defined if
you do not want to specify your execution environments via environment variables.

## Environment variables

### Logging

You can set the `SSHCHANNEL_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment
variables to `DEBUG` to add additional information in the console for the launched service.
It defaults to `INFO`. (Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons
of logs.)

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those
values are from the most verbose, `NOTSET`, which shows all logs, to the least verbose,
`FATAL`, which only shows fatal errors.

If `SSHCHANNEL_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or
`INFO` if `DEBUG_LEVEL` is not defined either).

Explicitly defining `SSHCHANNEL_DEBUG_LEVEL` to `NOTSET` or `DEBUG` enables extra logging
for the SSH events.  This extra logging **does not occur** if `SSHCHANNEL_DEBUG_LEVEL` is
not explicitly set.

Access logs are only shown at `NOTSET` and `DEBUG` levels.

### Execution environments specification

You can set the `SSH_CHANNEL_HOST`, `SSH_CHANNEL_USER`, `SSH_CHANNEL_PASSWORD`,
`SSH_CHANNEL_TAGS`, and `SSH_CHANNEL_PORT` environment variables to define just one
SSH-accessible execution environment:

- `SSH_CHANNEL_HOST`: required, either a hostname or an IP address.
- `SSH_CHANNEL_USER`: required, the user to use to log in to the execution environment.
- `SSH_CHANNEL_PASSWORD`: required, the corresponding password.
- `SSH_CHANNEL_TAGS`: required, a comma-separated list of tags this environment can manage.
- `SSH_CHANNEL_PORT`: optional, the port number (22 by default).

If you have more than one execution environment you intend to access via SSH, you will have
to provide a 'pools' definitions file.  The easiest way is to define the `SSH_CHANNEL_POOLS`
environment variable to point to a YAML file that defines the pools you want to use.

!!! information
    If you define an execution environment with `SSH_CHANNEL_*` and also provide `SSH_CHANNEL_POOLS`,
    the execution environment defined by `SSH_CHANNEL_*` will replace a similarly defined one in
    the provided pools (two execution environments are similar if they share the same host, port,
    and user).

!!! tip
    If the content of this referred YAML file changes, the pools definitions used
    will change accordingly.  You do not need to restart the channel plugin.

### Hooks specification

The `SSHCHANNEL_CHANNEL_HOOKS` environment variable, if defined, must refer to an
existing file that contains hook definitions.

For more information about hooks, see "[Hooks for jobs and providers](../impl/reference/hooks.md)."

!!! tip
    If the content of this referred file changes, the hooks definitions used by the channel
    handler will change accordingly.  You do not need to restart the provider plugin.

## Configuration file

This plugin has a configuration file (`sshee.yaml` by default) that describes
the _host_, _port_, _ssl_context_, and _trusted_authorities_ to use.
In this file, it is possible to enable _insecure_ logins or set _availability_check_delay_ 
to define the channel availability polling interval (defaults to 10 seconds).

If no configuration file is found it will default to the following values:

```yaml hl_lines="13-15"
apiVersion: opentestfactory.org/v1alpha1
kind: SSHServiceConfig
current-context: default
contexts:
- name: default
  context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: invalid-token
    watchdog_polling_delay_seconds: 30
    targets: []
pools: {}
```

The configuration included in the 'allinone' image is described in "[Common settings](config.md#configuration-file)."  The
listening port is 7786 and the bind address is `127.0.0.1` as the service exposes no user-facing endpoints.

There are three service-specific configuration options besides the common ones that must be
defined for the plugin to work properly: _pools_ (a configuration file option), _targets_
(a context-specific configuration option), and _watchdog_polling_delay_seconds_ (a
context-specific configuration option).

!!! important
    If a `SSH_CHANNEL_POOLS` environment variable is defined, the pools defined in the
    configuration file are ignored and the ones defined in the file referred to by the
    environment variable are used instead.

### Pools

`pools` lists the available groups of execution environments.  Each group (a _pool_)
describes one or more execution environments.

Each execution environment is described by the following (possibly optional) items:

- Either `host` or `hosts`: a hostname (a string) or a list of host names (a list of
  strings), required.
- `username`: a username on the execution environment (a string), required.
- `tags`: what capabilities it provides (a list of strings), required.  Amongst those
  capabilities, there must be one and only one of `linux`, `macos`, or `windows`.  If
  that is not the case, the execution environment will be ignored.
- `port`: a port (a number), 22 by default.
- `script_path`: where to put temporary scripts (a string), `'/tmp'` by default for
  `linux` and `macos` execution environments, `%TEMP%` for `windows` environments.
- `key_filename`: a file path (a string), required if using key authentication.
- `passphrase`: a passphrase (a string), required if using key authentication and
  key has a passphrase.
- `password`: a password (a string), required if using password authentication.
- `ssh_host_keys`: a file path (a string), required if there is a list of allowed
  incoming hosts (typically specified if using the default missing host key policy,
  which is to reject unknown incoming connections).
- `missing_host_key_policy`: a string, either `auto-add` or `reject` (`reject` by
  default).
- `namespaces`: a string, a comma-separated list of namespaces (`default` by default).

You cannot specify both `key_filename` and `password` at the same time.  Execution
environments with an incorrect specification are ignored.

`namespaces` specifies the namespace(s) the execution environment is accessible from.
It is a comma-separated list of namespace names or `'*'`, in which case the execution
environment will be accessible from all namespaces.

#### Example

An SSH channel requires `targets` and `pools` entries in order to prepare and dispatch
execution commands.

In the following example the current context, `default`, can handle execution
requests with a `runs-on` statement that is either `ssh`, `linux`, `[ssh, linux]`, or
`[linux, ssh]`.  It cannot handle execution requests with other tags.

It cannot handle `runs-on` statements that are either `windows`, `[ssh, windows]`, or
`[windows, ssh]` because the `demo_target` group is not specified in the context's targets.

```yaml hl_lines="13 20-42"
apiVersion: opentestfactory.org/v1alpha1
kind: SSHServiceConfig
current-context: default
contexts:
- name: default
  context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: invalid-token
    targets: [ssh_targets, ssh2_targets]
pools:
  demo_target:
  - host: demo.example.com
    username: demo
    password: 1234
    tags: [ssh, windows]
  ssh_targets:
  - host: dummy.example.com
    username: user
    password: secret
    tags: [ssh, linux]
    namespaces: '*'
  ssh2_targets:
  - host: host.example.com
    port: 22
    username: alice
    ssh_host_keys: /data/ssh/known_hosts
    key_filename: /data/ssh/example.pem
    missing_host_key_policy: reject
    tags: [ssh, linux]
    namespaces: ns1,ns2
  - hosts: [foo.example.com, bar.example.com]
    port: 22
    username: bob
    ssh_host_keys: /data/ssh/known_hosts
    key_filename: /data/ssh/secret.pem
    passphrase: secret
    missing_host_key_policy: auto-add
    tags: [ssh, linux]
```

### Targets

The `targets` context entry is a possibly empty list of pool names.

### Watchdog polling delay

_This delay must be an integer. If the entry is missing, the default value will be assumed. It must be defined in the currently-used context._

`watchdog_polling_delay_seconds` defines the interval used to check hooks definition changes.
If not specified, or if set to a lower value, defaults to 30.

This is only used if the `SSH_CHANNEL_POOLS` environment variable is defined.  It
tells the service how often it must check for changes in the pools definition.

## Execution environment configuration

The ssh channel uses non-interactive sessions.  By default, `sshd` only exposes a basic set of environment variables.

There are two ways to overcome this:

- Add `SetEnv` statements in your `/etc/ssh/sshd_config` configuration file (for example `BASH_ENV=~/.profile`)
- Enable the `PermitUserEnvironment` (set it to `yes`, and restart your ssh server).  That way, in the account
  you connect to with ssh, you can add a `~/.ssh/environment` with your variables (or `BASH_ENV`, as above)

## Subscriptions

The SSH channel plugin subscribes to the following events:

| `kind`              | `apiVersion`                 |
| ------------------- | ---------------------------- |
| `ExecutionCommand`  | opentestfactory.org/v1beta1  |

The SSH channel plugin exposes an `/inbox` endpoint that is used by the event bus to post relevant events.

## Launch command

If you want to manually start the SSH channel plugin, use the following command:

```shell
python -m opentf.plugins.sshee.main [--context context] [--config configfile]
```

Additional command-line options are available and described in "[Command-line options](config.md#command-line-options)."
