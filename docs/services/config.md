<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Common settings

This document describes the common configuration options and command-line options for services
and plugins.  When using the 'allinone' image, there is typically no need to change those.

If you are not developing a plugin or a service, you may skip it and go to the documents
describing the service-specific configuration options you want to use.

## Environment variables

You can set the `{service}_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment variables
to `DEBUG` to add additional information in the console for the launched service. It defaults to
`INFO`. Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons of logs.

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those values
are from the most verbose, `NOTSET`, which shows all logs, to the least verbose, `FATAL`, which
only shows fatal errors.

If `{service}_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or `INFO` if
`DEBUG_LEVEL` is not defined either).

Access logs are only shown at `NOTSET` and `DEBUG` levels.

## Configuration file

Each service configuration file has a set of common elements.

```yaml hl_lines="20"
apiVersion: opentestfactory.org/...
kind: ...Config
current-context: allinone
contexts:
- context:
    port: ...
    host: ... # 0.0.0.0 if exposing user-facing endpoints, 127.0.0.1 otherwise
    ssl_context: disabled
    trusted_authorities:
    - /etc/squashtf/*
    enable_insecure_login: true
    eventbus:
      endpoint: http://127.0.0.1:38368
      hostname: 127.0.0.1
      token: reuse
    # services:
    #  observer:
    #    endpoint: http://127.0.0.1:7775
    #    token: reuse
  name: allinone
```

The `apiVersion` and `kind` elements are always present.  The version may vary over
time and the kind varies per service category.

There is a `current-context` entry, which is the name of a specified context, and
there is a `contexts` entry, which is a list of contexts.

At least one context must be defined, named `allinone`.  This context is selected by
the launcher by default.

!!! warning
    If a `OPENTF_CONTEXT` environment variable is defined in the launcher's environment, each
    launched service and plugin must have a context with that name.  For example, if the `OPENTF_CONTEXT`
    environment variable value is `alternative`, there must exist an `alternative` context for
    all services and plugins.

### `context` elements

Each context specifies at least a `host` and a `port`.  An `eventbus` entry must
also be specified in the context if the service or plugin interacts with the event bus.

There may be a `services` entry in the context if the service or plugin interacts with other services.

```yaml
host: a string                               (required)
port: an integer                             (required)
ssl_context: 'adhoc' or 'disabled'           (optional)
trusted_authorities: a list of directories   (optional)
enable_insecure_login: a boolean             (optional, false by default)
insecure_bind_address: a string or a list    (optional)
```

- `host` (required): the host the service will bind to (a hostname or an IP
  address, `0.0.0.0` for services that provide user-facing endpoints and `127.0.0.1`
  for those who dont in the default configuration).
- `port` (required): the port the service will listen to (a number).
- `ssl_context` (optional): either `adhoc`, a list of two items (certificate
  file path and private key file path), or `disabled`.
- `trusted_authorities` (optional): a list of public key files and/or directories
  containing public key files, used for token validation.
  <br />
  Items are either fully-qualified file names or fully-qualified
  directory name ending with '/*'
- `enable_insecure_login` (optional): allow for insecure (token-less) logins, if
  set to `true` (by default, insecure logins are disabled).
- `insecure_bind_address` (optional, only used if `enable_insecure_login` is set
  to `true`): insecure logins, if enabled, are only allowed from a given address
  (`127.0.0.1` by default).

#### `eventbus` entry

This context entry is present for all services or plugins interacting with the event bus.

```yaml
endpoint: a string (an URI)                  (required)
token: a string                              (required)
insecure-skip-tls-verify: a boolean          (optional, false by default)
hostname: a string                           (optional)
port: an integer                             (optional)
```

#### `services` entry

This context entry is present if the service or plugin interacts directly with other services.

```yaml
endpoint: a string (an URI)                  (required)
token: a string                              (optional)
```

!!! note
    In the 'allinone' image there is only one service that has a `services` element: the Quality Gate
    service.  This service does not subscribe to any events.  It exposes a user-facing endpoint and
    queries the Observer service.

## Command-line options

Services and plugins are typically started by a launcher, but when developing them it may
be convenient to start them manually.

```console
usage: {launch command} [-h] [--config CONFIG] [--context CONTEXT] [--host HOST]
                        [--port PORT] [--ssl_context SSL_CONTEXT]
                        [--trusted_authorities TRUSTED_AUTHORITIES]
                        [--enable_insecure_login]
                        [--insecure_bind_address INSECURE_BIND_ADDRESS]
                        [--authorization_mode AUTHORIZATION_MODE]
                        [--authorization_policy_file AUTHORIZATION_POLICY_FILE]
                        [--token_auth_file TOKEN_AUTH_FILE]
                        [--trustedkeys_auth_file TRUSTEDKEYS_AUTH_FILE]

Create and start a {name} service.

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG       alternate config file (default to conf/{name}.yaml)
  --context CONTEXT     alternative context
  --host HOST           alternative host
  --port PORT           alternative port
  --ssl_context SSL_CONTEXT, --ssl-context SSL_CONTEXT
                        alternative ssl context
  --trusted_authorities TRUSTED_AUTHORITIES, --trusted-authorities TRUSTED_AUTHORITIES
                        alternative trusted authorities
  --enable_insecure_login, --enable-insecure-login
                        enable insecure login (disabled by default)
  --insecure_bind_address INSECURE_BIND_ADDRESS, --insecure-bind-address INSECURE_BIND_ADDRESS
                        insecure bind address (127.0.0.1 by default)
  --authorization_mode AUTHORIZATION_MODE, --authorization-mode AUTHORIZATION_MODE
                        authorization mode, JWT without RBAC if unspecified
  --authorization_policy_file AUTHORIZATION_POLICY_FILE, --authorization-policy-file AUTHORIZATION_POLICY_FILE
                        authorization policies for ABAC
  --token_auth_file TOKEN_AUTH_FILE, --token-auth-file TOKEN_AUTH_FILE
                        authenticated users for ABAC and RBAC
  --trustedkeys_auth_file TRUSTEDKEYS_AUTH_FILE, --trustedkeys-auth-file TRUSTEDKEYS_AUTH_FILE
                        authenticated trusted keys for ABAC and RBAC
```