<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Arranger Service

This core service orchestrates _workflow_ executions.

It exposes no user-facing endpoints.

There are [seven service-specific](#workflow-limits) configuration file options.

## Environment variables

### Logging

You can set the `ARRANGER_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment variables
to `DEBUG` to add additional information in the console for the launched service. It defaults to
`INFO`. (Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons of logs.)

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those values
are from the most verbose, `NOTSET`, which shows all logs, to the least verbose, `FATAL`, which
only shows fatal errors.

If `ARRANGER_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or `INFO` if
`DEBUG_LEVEL` is not defined either).

Access logs are only shown at `NOTSET` and `DEBUG` levels.

### Configuration options

You can also set the seven configuration options using environment variables:

- `ARRANGER_DEFAULT_TIMEOUT_MINUTES` sets the default maximum duration for a workflow, in minutes.
- `ARRANGER_MAX_JOB_STEPS` limits the number of steps in a workflow job.
- `ARRANGER_MAX_JOBS` limits the number of jobs in a workflow.
- `ARRANGER_MAX_PARALLEL` limits the number of jobs in a workflow that can be running in parallel.
- `ARRANGER_MAX_WORKERS` limits the number of jobs in a workflow that can be running at any given time.
- `ARRANGER_DOWNLOAD_JOB_TAGS` sets additional environment tags for `uses`-type jobs reusable workflow downloads.
- `ARRANGER_DOWNLOAD_JOB_PARENTS_TAGS` specifies whether to use or ignore 
`uses`-type jobs environment tags.

The first five configuration options must be integers, the two remaining must be strings. If those environment variables are defined, they override the values in the configuration file.

## Configuration file

This service has a configuration file (`arranger.yaml` by default) that describes the _host_,
_port_, _ssl_context_, and _trusted_authorities_ to use.  It can also enable _insecure_ logins.
It can also change the service limits.

If no configuration file is found it will default to the following
values:

```yaml hl_lines="12-16"
apiVersion: opentestfactory.org/v1beta2
kind: ServiceConfig
current-context: default
contexts:
- context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: invalid
    default_timeout_minutes: 360
    max_job_steps: 10240
    max_jobs: 1024
    max_parallel: 1
    max_workers: 256
    download_job_tags: ''
    download_job_parents_tags: 'use'
  name: default
```

The configuration included in the 'allinone' image is described in "[Common settings](config.md#configuration-file)."  The
listening port is 7781 and the bind address is `127.0.0.1` as the service exposes no user-facing endpoints.

Seven service-specific configuration options can be refined.

### Workflow limits

_All those limits must be integers.  If the entry is missing, the default value
will be assumed.  They must be defined in the currently-used context._

`max_jobs` limits the number of jobs in a workflow.  If a workflow contains or
generates more jobs than this limit, the workflow will be canceled.  If not specified,
defaults to 1024.

`max_job_steps` limits the number of steps in a workflow.  If a job contains or
generates more steps than this limit, the job will be canceled.  If not specified,
defaults to 10240.

`max_workers` limits the number of jobs in a workflow that can be running at
any given time.  If not specified, defaults to 256.

`max_parallel` limits the number of jobs in a workflow that can be running in parallel if
`strategy.max-parallel` is not defined in a workflow.  If not specified, defaults to 1.

`default_timeout_minutes` is the default maximum duration for a workflow, in minutes.
If not specified, defaults to 360 minutes (6 hours).

### Reusable Workflows Download

_Those parameters must be strings.  If the entry is missing, the default 
value will be assumed.  They must be defined in the currently-used context._

`download_job_tags` sets additional environment tags for `uses`-type jobs reusable 
workflow downloads. It must be a string of comma-separated values. If not specified, 
defaults to an empty string.

`download_job_parents_tags` specifies whether to use or ignore 
`uses`-type jobs environment tags. The possible values are 'use' or 'ignore'. If not specified, defaults to 'use'.

See [reusable workflows guide](../guides/reusable-workflows.md#setting-download-environment-tags) for use example.

## Subscriptions

The arranger service subscribes to the following events:

| `kind`                 | `apiVersion`                 |
| ---------------------- | ---------------------------- |
| `Workflow`             | opentestfactory.org/v1       |
| `WorkflowCancellation` | opentestfactory.org/v1       |
| `GeneratorResult`      | opentestfactory.org/v1       |
| `ProviderResult`       | opentestfactory.org/v1       |
| `ExecutionResult`      | opentestfactory.org/v1alpha1 |
| `ExecutionError`       | opentestfactory.org/v1alpha1 |

The arranger service exposes an `/inbox` endpoint that is used by the event bus to post relevant events.

## Launch command

If you want to manually start the arranger service, use the following command:

```shell
python -m opentf.core.arranger [--context context] [--config configfile]
```

Additional command-line options are available and described in "[Command-line options](config.md#command-line-options)."
