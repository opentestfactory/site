<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# EventBus Service

This core service handles _subscriptions_ and _publications_.  It may be replaced by a facade to an
existing event bus/mq manager.

It exposes [user-facing endpoints](endpoints.md#event-bus-service) that are used by other services to subscribe to events and publish events.

There are [two service-specific](#eventbus-limits) configuration file options.

## Environment variables

### Logging

You can set the `EVENTBUS_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment variables
to `DEBUG` to add additional information in the console for the launched service. It defaults to
`INFO`. (Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons of logs.)

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those values
are from the most verbose, `NOTSET`, which shows all logs, to the least verbose, `FATAL`, which
only shows fatal errors.

If `EVENTBUS_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or `INFO` if
`DEBUG_LEVEL` is not defined either).

Access logs are only shown at `NOTSET` and `DEBUG` levels.

### Configuration options

You can also set the two configuration options using environment variables:

- `EVENTBUS_POST_TIMEOUT_SECONDS` sets the `post_timeout_seconds` value.
- `EVENTBUS_WAITRESS_THREADS_COUNT` sets the `waitress_threads_count` value.

If those environment variables are defined, they override the values in the configuration file.

## Configuration file

This service has a configuration file (`eventbus.yaml` by default) that describes the _host_,
_port_, _ssl_context_, and _trusted_authorities_ to use.  It can also enable _insecure_
logins.

If no configuration file is found it will default to the following values:

```yaml hl_lines="10-14"
apiVersion: opentestfactory.org/v1alpha1
kind: EventBusConfig
current-context: default
contexts:
- context:
    port: 38368
    host: 127.0.0.1
    ssl_context: adhoc
    enable_insecure_login: false
    post_timeout_seconds: 10
    waitress_threads_count: 10
  name: default
```

The configuration included in the 'allinone' image is described in "[Common settings](config.md#configuration-file)."  The
listening port is `38368` and the bind address is `0.0.0.0` as the service exposes user-facing endpoints.

Two service-specific configuration options can be refined.

### EventBus limits

_All those limits must be integers.  If the entry is missing, the default value will be assumed.
They must be defined in the currently-used context._

#### `post_timeout_seconds`

`post_timeout_seconds` limits the time the event bus waits for a subscriber to respond to a
publication.  It must be at least 1.  If not specified, defaults to 10.

If the subscriber does not respond within the specified time, the publication dispatch is
considered a failure and will be replayed if the maximum publication attempt has not been
reached.

#### `waitress_threads_count`

`waitress_threads_count` sets the number of threads handling incoming requests.  It must be at
least 4.  If not specified, defaults to 10.

## Launch command

If you want to manually start the event bus service, use the following command:

```shell
python -m opentf.core.eventbus [--context context] [--config configfile]
```

Additional command-line options are available and described in "[Command-line options](config.md#command-line-options)."
