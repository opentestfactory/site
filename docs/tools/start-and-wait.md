<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Starting the orchestrator

The recommended way to deploy the OpenTestFactory orchestrator is as a service
in your environment.

In some contexts, you may want to start it, use it, and stop it when done.

Starting the orchestrator depends on your environment.  It may be deployed as a
standalone deployment using docker or docker-compose, or it can be deployed
as a side container in your CI worker, or some other way.

In those contexts, there is a need to ensure the dynamically deployed orchestrator
is ready to accept workflows.

## `opentf-ready`

This tool is intended to be used when the orchestrator is deployed dynamically.  It
waits until the orchestrator is ready to accept workflows.

If the orchestrator has been deployed using `docker` or `docker-compose`, and if you
have access to the docker host, you can use the 'docker' mode. It is not installed by
default.  Use the following command to install it with its dependencies:

```bash
pip install opentf-tools[docker]
```

If you are using this script in 'docker' mode, you must provide the orchestrator
container ID.

If you are using this script in its default mode, you must provide a list of
expected services.  This is a list of comma-separated strings.  Case is irrelevant.

The options are:

```text
usage: opentf-ready [-h] (--container_id CONTAINER_ID | --services SERVICES) [--timeout TIMEOUT] --host HOST [--port PORT] --token TOKEN [--debug]

OpenTestFactory Orchestrator availability checker

optional arguments:
  -h, --help            show this help message and exit
  --container_id CONTAINER_ID
                        docker id of the container to check
  --services SERVICES   comma-separated list of expected services
  --timeout TIMEOUT     verification timeout in seconds (default to 3600)
  --host HOST           target host with protocol (e.g. https://example.local)
  --port PORT           target port (eventbus, default to 38368)
  --token TOKEN         token
  --debug               whether to log debug information.
```

Please note that it requires access to the eventbus service, which should be exposed.

## Services names

When using the default mode, you must provide the services you are expecting.

The following services are started by the default configuration.

Core services:

- arranger
- observer

Plugins:

- actionprovider
- agentchannel
- cucumber
- cypress
- inceptionee
- junit
- playwright
- robotframework
- s3publisher
- skf
- soapui
- sshchannel

Please note that this list can change depending on your orchestrator configuration and
that other distributions may add or remove some services too.

See "[Startup configuration](../installation.md#startup)" for more information on
how to specify the services you want to enable or disable in your distribution.

## Examples

In the following example, the OpenTestFactory orchestrator is deployed using
`docker-compose` and we use the 'docker' mode:

```bash
docker-compose up -d
CONTAINER_ID=`docker-compose ps | grep orchestrator | cut -d " " -f 1`
opentf-ready --container_id $CONTAINER_ID --host http://localhost --token $TOKEN
...
opentf-done --host http://localhost --token $TOKEN
docker-compose down
```

In the following examples, the OpenTestFactory orchestrator is deployed in some way,
and a given list of services is provided:

```bash
opentf-ready --services arranger,observer,tm.generator,sshchannel --host http://localhost --token $TOKEN
```
