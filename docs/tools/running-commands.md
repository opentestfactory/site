<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Running commands

!!! note
    The documentation for `opentf-ctl` has been split into multiple sections.  You can
    find links below to the new locations of the original sections.  You may want to
    update your bookmarks accordingly.

## Basic commands

- [`get workflows`](opentf-ctl/basic.md#get-workflows)  List active and recent workflows
- [`run workflow {filename}`](opentf-ctl/basic.md#run-workflow-file_name)  Start a workflow
- [`get workflow {workflow_id}`](opentf-ctl/basic.md#get-workflow-workflow_id)  Get a workflow status
- [`kill workflow {workflow_id}`](opentf-ctl/basic.md#kill-workflow-workflow_id)  Cancel a running workflow

## Agent Commands

- [`get agents`](opentf-ctl/agent.md#get-agents)  List registered agents
- [`delete agent {agent_id}`](opentf-ctl/agent.md#delete-agent-agent_id)  De-register an agent

## Channel Commands

- [`get channels`](opentf-ctl/channel.md#get-channels)  List known channels

## Quality Gate Commands

- [`get qualitygate {workflow_id}`](opentf-ctl/qualitygate.md#get-qualitygate-workflow_id)  Get quality gate status for a workflow
- [`describe qualitygate {workflow_id}`](opentf-ctl/qualitygate.md#describe-qualitygate-workflow_id)  Get description of a quality gate status for a workflow

## Attachment Commands

- [`get attachments {workflow_id}`](opentf-ctl/attachments.md#get-attachments-workflow_id)  List workflow attachments
- [`cp {workflow_id}:{...} {destination}`](opentf-ctl/attachments.md#cp-workflow_id-destination)  Copy a workflow attachment or workflow attachments to the local destination
- [`generate report {workflow_id} using {file}`](opentf-ctl/attachments.md#generate-report-workflow_id-using-file)  Generate an Insight Collector report for a workflow

## Data Sources Commands

- [`get datasource {workflow_id}`](opentf-ctl/datasources.md#get-datasource-workflow_id-kind-kind)  Get workflow data sources

## Token Commands

- [`generate token using {key}`](opentf-ctl/tokens.md#generate-token-using-key)  Interactively generate a signed token
- [`check token {token} using {key}`](opentf-ctl/tokens.md#check-token-token-using-key)  Check if token signature matches public key
- [`view token {token}`](opentf-ctl/tokens.md#view-token-token)  Show token payload

## Advanced Commands

- [`get namespaces`](opentf-ctl/advanced.md#get-namespaces)  List accessible namespaces
- [`get subscriptions`](opentf-ctl/advanced.md#get-subscriptions)  List active subscriptions
- [`delete subscription {subscription_id}`](opentf-ctl/advanced.md#delete-subscription-subscription_id)  Cancel an active subscription

## Other Commands

- [`config`](opentf-ctl/other.md#config-generate)  Modify current opentf-tools configuration
- [`version`](opentf-ctl/other.md#version)  List the tools version
