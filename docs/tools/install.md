<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Install Tools

The `opentf-tools` package contains a set of tools that facilitate the configuration
and administration of the OpenTestFactory's runtime environment.

It requires Python 3.8 or higher.

```bash
pip install --upgrade opentf-tools
```

!!! tip
    The above command will install the most recent version of the package.  If you want to
    install a specific version you can use the following command:

        pip install opentf-tools==0.42.0

    The list of available versions is on [PyPI](https://pypi.org/project/opentf-tools/#history).

## [`opentf-ctl`](opentf-ctl/index.md)

The OpenTestFactory command-line tool, `opentf-ctl`, allows you to run commands against
OpenTestFactory orchestrators.

## [`opentf-ready`](start-and-wait.md)

This tool waits until the orchestrator is ready to accept workflows.

## [`opentf-done`](wait-and-stop.md)

This tool waits until the orchestrator service is idle.  It can safely be stopped when idle.
