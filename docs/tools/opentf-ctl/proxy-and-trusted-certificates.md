<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Proxy and Trusted certificates

To override the default certificate bundle used by `opentf-ctl`, you can set the standard
`CURL_CA_BUNDLE` environment variable.

=== "Bash"

    ```bash
    export CURL_CA_BUNDLE="/usr/local/myproxy_info/cacert.pem"
    export https_proxy="http://10.10.1.10:1080"
    opentf-ctl [...]
    ```

=== "CMD"

    ```bat
    set CURL_CA_BUNDLE=/path/to/myproxy_info/cacert.pem
    set https_proxy=http://10.10.1.10:1080
    opentf-ctl [...]
    ```

=== "PowerShell"

    ```powershell
    $Env:CURL_CA_BUNDLE = "/path/to/myproxy_info/cacert.pem"
    $Env:https_proxy = "http://10.10.1.10:1080"
    opentf-ctl [...]
    ```

If the following environment variables are defined in `opentf-ctl`'s environment, they will
be used:

| Environment variable | Format | Description |
| --- | --- | --- |
| `http_proxy` or `HTTP_PROXY` | `[protocol://]<host>[:port]` | Sets the proxy server to use for HTTP. |
| `https_proxy` or `HTTPS_PROXY` | `[protocol://]<host>[:port]` | Sets the proxy server to use for HTTPS. |
| `no_proxy` or `NO_PROXY` | `<comma-separated list of hosts/domains>` | List of host names that shouldn't go through any proxy.  If set to an asterisk '*' only, it matches all hosts.  Each name in this list is matched as either a domain name which contains the hostname, or the hostname itself. |
| `CURL_CA_BUNDLE` | `<path to file>` | The CA bundle to use. |

For the first three environment variables, the lower-cased ones take precedence over the
upper-cased ones if both are defined.

The `no_proxy` or `NO_PROXY` environment variables, if defined, are interpreted following
the Python rules:

- It matches suffixes
- It strips leading `.`
- `*` matches all hosts

Regexps are not supported, CIDR blocks are not supported either, and loopback IPs are not
detected.

For example, the following two examples will bypass the proxy:

```bash
env https_proxy=http://non.existent no_proxy=.example.com opentf-ctl get workflows --server https://example.com
env https_proxy=http://non.existent no_proxy=.example.com opentf-ctl get workflows --server https://orchestrator.example.com
```

Please refer to "[We need to talk: Can we standardize no_proxy?](https://about.gitlab.com/blog/2021/01/27/we-need-to-talk-no-proxy/)"
for more information on how the `no_proxy` environment variable is interpreted in
different contexts.
