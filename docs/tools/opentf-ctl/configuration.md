<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Configuration

For configuration, `opentf-ctl` looks for a file named `config` in the `$HOME/.opentf`
directory.  You can specify another configuration file by setting the `OPENTF_CONFIG`
environment variable or by using the `--opentfconfig` command-line option.

If the command-line option is specified, then it is used as the configuration file.
Otherwise, if the environment variable is set, it is used as the configuration file
location.  Otherwise, the configuration file location is expected to be
`$HOME/.opentf/config`.

You can use the `opentf-ctl options` command to get a summary of all global options that
can be overridden using the command line:

```
The following environment variables override the defaults, if not overridden by options:

  OPENTF_CONFIG: Path to the opentfconfig file to use for CLI requests
  OPENTF_TOKEN: Bearer token for authentication to the orchestrator

The following options can be passed to any command:

  --token='': Bearer token for authentication to the orchestrator
  --user='': The name of the opentfconfig user to use
  --orchestrator='': The name of the opentfconfig orchestrator to use
  --context='': The name of the opentfconfig context to use
  --insecure-skip-tls-verify=false: If true, the server's certificate will not be checked for validity.  This will make your HTTPS connections insecure
  --warmup-delay='': Delay in seconds to wait between post and get requests (default 1)
  --polling-delay='': Delay in seconds to wait between polling requests (default 5)
  --max-retry='': Max number of retries before giving up reading information (default 3)
  --opentfconfig='': Path to the opentfconfig file to use for CLI requests

Those global options can be specified anywhere on the command line, before or after the command.
```

You can use the [`opentf-ctl config`](other.md) commands to generate and configure the
configuration file.

The configuration file is a YAML file with the following structure:

```yaml title="config"
apiVersion: opentestfactory.org/v1beta1
kind: CtlConfig
current-context: default
contexts:
- context:
    orchestrator: default
    user: default
  name: default
orchestrators:
- name: default
  orchestrator:
    insecure-skip-tls-verify: true
    warmup-delay: 1
    polling-delay: 5
    max-retry: 3
    server: http://localhost
    services:
      receptionist:
        port: 7774
      observer:
        port: 7775
      killswitch:
        port: 7776
      eventbus:
        port: 38368
      localstore:
        port: 34537
      insightcollector:
        port: 7796
      agentchannel:
        port: 24368
      qualitygate:
        port: 12312
users:
- name: default
  user:
    token: ey...
    step-depth: ...
    job-depth: ...
    max-command-length: ...
    output-prefix: ...
```

It contains a series of _contexts_ that specify an orchestrator and a user.  By default,
the used context will be the one specified by `current-context`, but this can be overridden
by using the `--context` command-line option.

The orchestrator specified by the selected context can be overridden by using the
`--orchestrator` command-line option.

The user specified by the selected context can be overridden by using the `--user`
command-line option.

The orchestrator's tls-verify status can be overridden by using the `--insecure-skip-tls-verify`
command-line option.

The orchestrator's default warmup delay, polling delay, and max retry count can be overridden by
using the `--warmup-delay`, `--polling-delay`, and `--max-retry` command-line options.

The user's token can be overridden by defining the `OPENTF_TOKEN` environment variable
or the `--token` command-line option.

If the command-line option is specified, then it is used as the token.  Otherwise, if the
environment variable is set, it is used as the token.  Otherwise, the token value set for
the user in the configuration file is used as the token.

If the orchestrator endpoints all listen on the same server, port, and possibly path prefix,
the orchestrator's `services` section can be omitted.

If one endpoint is hosted on another server or uses a customized route or port, it can
be specified as such:

```yaml
  ...
  orchestrator:
    server: http://localhost
    services:
      observer:
        prefix: foobar
        port: 1234
```

That way, instead of performing a workflows' lookup using `http://localhost/workflows`,
`opentf-ctl` will use `http://localhost:1234/foobar/workflows` instead, while still
using `http://localhost/workflows` to run workflows.

Please refer to "[config set-orchestrator](other.md#config-set-orchestrator)" for more information.

!!! tip
    `opentf-ctl` respects the common proxy and trusted certificates environment variables.
    Please refer to "[Proxy and Trusted certificates](proxy-and-trusted-certificates.md)" for
    more information.
