<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Tokens commands

The `opentf-ctl` tool provides a set of commands to manage _tokens_.  You can generate tokens,
check token signatures, and view token payloads.

## `generate token using {key}`

This command generates a signed token.

The specified key must be a valid private key.  It may be encrypted with a passphrase, in which
case you will be prompted for it.

If you do not already have a private key, you can create it using the following commands:

```shell
openssl genrsa -out trusted_key.pem 4096
openssl rsa -pubout -in trusted_key.pem -out trusted_key.pub
```

You can then use the command to create as many tokens as needed:

```shell
opentf-ctl generate token using trusted_key.pem
```

```console
Please specify an algorithm (RS512 if unspecified):
The specified algorithm is: RS512
Please enter the issuer (your company or department): acme
Please enter the subject (you or the person you are making this token for): charlie
The signed token is:
ey...
```

### Optional Parameters

The `generate token` command allows for additional optional parameters, in no specific
order, but they must follow the command if used:

```
--algorithm=algo                    # specify the algorithm to use
--issuer=issuer                     # specify the token issue
--subject=subject                   # specify the token subject
--expiration=YYYY/MM/DD or ''       # set the token expiration date
--output-file=filename              # specify the output file
```

### Non-Interactive Token Generation

By default, tokens are generated interactively. To create a token in a fully non-interactive way, 
you should specify all optional parameters (if some parameters are missing, the prompt will appear 
to fill them in):

```shell
opentf-ctl generate token using trusted_key.pem \
--algorithm=RS512 --issuer=acme \
--subject=alice --expiration='' \
--output-file=alice.token
```

```console
The specified algorithm is: RS512
Writing token to: alice.token
```

Token expiration date should be in `YYYY/MM/DD` format. If the token should not expire, leave 
it empty (`''`).

If `--output-file` is not set, the output will be displayed in stdout. 

## `check token {token} using {key}`

This command checks if the token signature matches the specified public key.

```shell
opentf-ctl check token ey... using trusted_key.pub
```

If the token signature matches the payload is displayed:

```console
The token is signed by the trusted_key.pub public key.  The token payload is:
{'iss': 'acme', 'sub': 'foo'}
```

If the token signature does not match, an error message is displayed:

```console
ERROR:root:The token is not signed by trusted_key_2.pub.
```

## `view token {token}`

This command shows the token payload.

```shell
opentf-ctl view token ey...
```

```console
The token payload is:
{'iss': 'acme', 'sub': 'foo'}
```

This command does not check the token signature.  It will work even if the signature is missing or corrupted.

If the token is invalid, an error message is displayed.  The error message will depend on the cause of invalidity.

```console
ERROR:root:Could not validate token signature: Invalid payload padding.
```
