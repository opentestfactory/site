<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# `opentf-ctl`

!!! Note

    `opentf-ctl` is a component of `opentf-tools`, which must be installed before use.  Please
    refer to "[Install tools](../install.md)" for more information on how to install.

The OpenTestFactory command-line tool, `opentf-ctl`, allows you to run commands against
orchestrators.  You can use `opentf-ctl` to run workflows, inspect and manage orchestrator
resources, and view workflow logs.

## [Basic Commands](basic.md)

- [`get workflows`](basic.md#get-workflows)  List active and recent workflows
- [`run workflow {filename}`](basic.md#run-workflow-file_name)  Start a workflow
- [`get workflow {workflow_id}`](basic.md#get-workflow-workflow_id)  Get a workflow status
- [`kill workflow {workflow_id}`](basic.md#kill-workflow-workflow_id)  Cancel a running workflow

## [Agent Commands](agent.md)

- [`get agents`](agent.md#get-agents)  List registered agents
- [`delete agent {agent_id}`](agent.md#delete-agent-agent_id)  De-register an agent

## [Channel Commands](channel.md)

- [`get channels`](channel.md#get-channels)  List known channels

## [Quality Gate Commands](qualitygate.md)

- [`get qualitygate {workflow_id}`](qualitygate.md#get-qualitygate-workflow_id)  Get quality gate status for a workflow
- [`describe qualitygate {workflow_id}`](qualitygate.md#describe-qualitygate-workflow_id)  Get description of a quality gate status for a workflow

## [Attachment Commands](attachments.md)

- [`get attachments {workflow_id}`](attachments.md#get-attachments-workflow_id) List workflow attachments
- [`cp {workflow_id}:{...} {destination}`](attachments.md#cp-workflow_id-destination) Copy a workflow attachment or workflow attachments to the local destination
- [`generate report {workflow_id} using {file}`](attachments.md#generate-report-workflow_id-using-file)  Generate an Insight Collector report for a workflow

## [Data Sources Commands](datasources.md)

- [`get datasource {workflow_id}`](datasources.md#get-datasource-workflow_id-kind-kind) Get
  workflow data sources

## [Token Commands](tokens.md)

- [`generate token using {key}`](tokens.md#generate-token-using-key)  Interactively generate a signed token
- [`check token {token} using {key}`](tokens.md#check-token-token-using-key)  Check if token signature matches public key
- [`view token {token}`](tokens.md#view-token-token)  Show token payload

## [Advanced Commands](advanced.md)

- [`get namespaces`](advanced.md#get-namespaces)  List accessible namespaces
- [`get subscriptions`](advanced.md#get-subscriptions)  List active subscriptions
- [`delete subscription {sub_id}`](advanced.md#delete-subscription-subscription_id)  Cancel an active subscription

## [Other Commands](other.md)

- [`config`](other.md#config-generate)  Modify current `opentf-ctl` configuration
- [`version`](other.md#version)  List the tools version
