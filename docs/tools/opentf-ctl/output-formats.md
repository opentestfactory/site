<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Output formats

Most `opentf-ctl` commands output structured data.  By default, the output is
formatted as a table.  The `--output` flag allows you to change the output
format.  The supported formats are:

```text
wide                                   # show additional information
custom-columns=                        # show specified information
json                                   # show information in JSON format
yaml                                   # show information in YAML format
```

## Wide

The `--output=wide` or `-o wide` format shows more information than the default format.
Please refer to the command's description for more details on the additional information.

## Custom columns

The `--output=custom-columns=...` or `-o custom-columns=...` format allows you to specify
the columns to display.  It is supported by most commands.  The columns are specified as a
comma-separated list of `{column name}:{key path}` definitions.

The column name can be any string not containing a colon.  The key path starts with an optional
dot and is a dot-separated list of keys to access the value to display.  For example,
`.metadata.name` will display the `name` key of the `metadata` object, `my name`.

```yaml
metadata:
  name: my name
  labels:
    foo: bar
    baz: qux
spec:
  runs-on: [agent1, agent2]
```

If the key path denotes an object or a list, its JSON representation will be returned.  For
example, using the example above, `.metadata.labels` gives `{"foo": "bar", "baz": "qux"}` and
`.spec.runs-on` gives `["agent1", "agent2"]`.

You can use `*` to group values: `.metadata.labels.*` gives `["bar", "qux"]`.

You can use `*~` to group keys: `.metadata.labels.*~` gives `["foo", "baz"]`.

Please note that `*~` can only be used at the end of the key path.

The command's description lists commonly available keys, but any key can be used.
If you do not find the information you are looking for, you can use the `-o json` or `-o yaml`
format to discover the information's structure.

If the key path is invalid, the value will be `<none>`.

!!! tip
    Those key paths are JSON paths.  If you are familiar with `jq`, you probably recognized
    the syntax.  The root node is implicit here, and you can only use the dot notation.
    Functions and filters are irrelevant in the context and are hence unavailable.

## JSON and YAML

The `--output={json,yaml}` or `-o {json,yaml}` format shows the information in JSON or YAML
format.  Depending on the command, the output is either an object or a list of objects.  It
can be fed to `jq` or `yq` for further processing.
