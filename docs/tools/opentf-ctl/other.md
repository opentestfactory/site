<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Other commands

The `opentf-ctl` tool also provides a set of commands to manage its configuration files.  It
can also display information on its version and that of the orchestrators it
has access to.

## `config generate`

This command generates a config file from user input. The generated configuration file is
printed in the stdout stream (it is not saved to a file).

 ```shell
 opentf-ctl config generate
 ```

The following options can be passed to this command:

- `--name`: Nickname that will be used for context and orchestrator registration (default:default)
- `--orchestrator-server`: Address of the opentf orchestrator
- `--orchestrator-agentchannel-port`: Port of the agent channel service (integer) (defaut:24368)
- `--orchestrator-eventbus-port`: Port of the eventbus service (integer) (defaut:38368)
- `--orchestrator-insightcollector-port`: Port of the insightcollector service (integer) (default: 7796)
- `--orchestrator-killswitch-port`: Port of the killswitch service (integer) (default:7776)
- `--orchestrator-localstore-port`: Port of the localstore service (integer) (default:34537)
- `--orchestrator-observer-port`: Port of the observer service (integer) (default:7775)
- `--orchestrator-qualitygate-port`: Port of the quality gate service (integer) (default:12312)
- `--orchestrator-receptionist-port`: Port of the receptionist service (integer)  (default:7774)
- `--insecure-skip-tls-verify=false|true`: Skip TLS verification (default:false)
- `--token`: User's token to sign communications with orchestrator

Please refer to "[config set-orchestrator](#config-set-orchestrator)" for more configuration
options for orchestrators.

## `config view`

This command displays in the console the current configuration file.

 ```shell
 opentf-ctl config view
 ```

The displayed configuration will be in order of priority the one pointed by

- the `--opentfconfig` argument value
- the environment variable `OPENTF_CONFIG`
- the current user configuration located at `~/.opentf/config`

## `config use-context`

This command sets the current context.

```shell
opentf-ctl config use-context foo
```

The context must exist.

## `config set-context`

This command creates or updates a context entry.

```shell
opentf-ctl config set-context my_context --user=admin --orchestrator=e2e
```
The following options can be passed to this command:

- `--orchestrator`: Name of an existing orchestrator entry
- `--user`: Name of an existing user
- `--namespace`: The namespace to use when starting workflows with no explicit
  namespace defined

You can use `--current` as the context name, in which case the current context
will be updated.

## `config delete-context`

This command deletes an existing context.

```shell
opentf-ctl config delete-context my_context
```

<!-- TODO If the deleted context was the current context, the current context is unset. -->

## `config set-credentials`

This command creates or updates a user entry.

```shell
opentf-ctl config set-credentials my_user --token=ey...
```

The following options can be passed to this command:

- `--token`: The token to use when acting as this user (a string)
- `--step-depth`: The default value to use for step depth (an integer, 1 by default)
- `--job-depth`: The default value to use for job depth (an integer, 1 by default)
- `--max-command-length`: The default value to use for max command length (an integer, 15 by default)
- `--output-prefix`: The default value for log line prefix (a string, `[{timestamp}] [Job {job_id}]` by default)

For `step-depth`, `job-depth`, and `max-command-length` a value of `0` disables the limit.

## `config delete-credentials`

This command deletes an existing user.

```shell
opentf-ctl config delete-credentials my_user
```

If contexts are referring to this user, they remain unchanged.

## `config set-orchestrator`

This command creates or updates an orchestrator entry.

```shell
opentf-ctl config set-orchestrator my_orchestrator --server=https://1.2.3.4
```

The following options can be passed to this command:

- `--insecure-skip-tls-verify=false|true`: Skip TLS verification
- `--warmup-delay={n}`: Delay in seconds to wait before sending requests (integer)
- `--polling-delay={n}`: Delay in seconds to wait between polling requests (integer)
- `--max-retry={n}`: Max number of retry before giving up reading information (integer)
- `--server=`: Address of the opentf orchestrator (string, of the form `{protocol}://{domain}[/{context}]`)
- `--{SERVICE}-force-base-url=false|true`: Override link URLs
- `--{SERVICE}-port={n}`: Port of the service (integer)
- `--{SERVICE}-prefix={prefix}`: Prefix for the service (string)

where SERVICE is one of `receptionist`, `observer`, `eventbus`, `killswitch`,
`agentchannel`, `localstore`, `insightcollector`, or `qualitygate`.

It only sets or changes the specified parameters.

If a SERVICE endpoint is hosted on another server or uses a customized route or port, it
can be specified as such:

```yaml
  orchestrator:
    server: http://localhost
    services:
      observer:
        prefix: foobar
        port: 1234
        force-base-url: true
```

That way, instead of performing a workflows' lookup using `http://localhost/workflows`,
`opentf-ctl` will use `http://localhost:1234/foobar/workflows` instead, while still
using `http://localhost/workflows` to run workflows.

!!! Information
    If `force-base-url` is set to `true` for a service, `opentf-ctl` will ignore the links
    returned by the service and will always construct the endpoint's base URL using the
    `server`, `port`, and `prefix` if defined.  It is useful if the orchestrator you are
    using is behind an incorrectly-configured reverse proxy you have no control of.

## `config delete-orchestrator`

This command deletes an existing orchestrator.

```shell
opentf-ctl config delete-orchestrator my_orchestrator
```

If contexts are referring to this orchestrator, they remain unchanged.

## `version`

This command displays the tools version and lists the OpenTestFactory image 
components and their versions. It also shows the current orchestrator 
server.

```shell
opentf-ctl version
```

```console
Tools Version: version.Info{Major:"0", Minor: "39", FullVersion: "0.39.0"}
Orchestrator:
    Orchestrator server: http://127.0.0.1
Components of otf-images image:
    allure: 2.13.8
    opentf-orchestrator: 0.53.0.dev1533+main.1708123a
    opentf-qualitygate: 0.8.0.dev121+main.43b2a747
    org.opentestfactory:java-plugins: 1.6.0-SNAPSHOT
```

The following options can be passed to this command:

 - `--debug`: Display the OpenTestFactory image BOM in raw output.

