<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Channel commands

The `opentf-ctl` tool provides a set of commands to manage _channels_.  You can list known
channels.

## `get channels`

This command lists known execution environments.

```shell
opentf-ctl get channels
```

```console
NAME               NAMESPACES  TAGS                           LAST_REFRESH_TIMESTAMP      STATUS
dummy.example.com  default     ["ssh", "linux"]               2023-08-01T11:36:46.313393  IDLE
test agent         default     ["windows", "robotframework"]  2023-08-01T18:53:50.871514  IDLE
```

The first item is the execution environment name, the second item is the namespace(s) this
execution environment is accessible from (if it is `*`, it is available for all namespaces),
the third item is a list of tags, the fourth item is when the environment
details were refreshed for the last time, and the last item is the execution environment
status (`IDLE`, `PENDING`, `BUSY`, or `UNREACHABLE`).

An `IDLE` execution environment is currently not doing any orchestrator-related tasks.  A
`PENDING` execution environment has been offered to handle a job but has not been selected
(yet).  A `BUSY` execution environment is currently handling a job. An `UNREACHABLE` execution 
environment was unreachable on the last availability check (by default, execution environment 
status is checked every 10 seconds).

### Optional parameters

The `get channels` command allows for additional optional parameters:

```text
--output=wide or -o wide                        # show additional information
--output=custom-columns= or -o custom-columns=  # show specified information
--output=json or -o json                        # show information in JSON format
--output=yaml or -o yaml                        # show information in YAML format
--selector={s} or -l {s}                        # filter the output on label selector(s)
--field-selector={s}                            # filter the output on field selector(s)
```

You can only specify one output format at a time.  Please refer to "[Output formats](output-formats.md)" for more information.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

#### Wide view

This option shows the default columns, as well as the channel handler ID.

```shell
opentf-ctl get channels -o wide
opentf-ctl get channels --output=wide
```

```console
HANDLER_ID                            NAME               NAMESPACES  TAGS                           LAST_REFRESH_TIMESTAMP      STATUS
723a94cc-148a-4904-9a7f-bdb9d0cb8444  dummy.example.com  default     ["ssh", "linux"]               2023-08-01T11:36:46.313393  IDLE
3524911d-c6c0-490e-b9e0-b450cbef7a24  test agent         default     ["windows", "robotframework"]  2023-08-01T18:53:50.871514  IDLE
```

#### Custom view

If this optional parameter is specified, you can choose what is displayed for each known
execution environment.

The columns definition format is as follows: `name:value[,name:value]*`.  `name` is the
column name in the output (such as `NAME` in the example above).  `value` is the path to the
information in the service subscription schema.

The following values are supported:

- `.metadata.name`
- `.metadata.namespaces`
- `.metadata.channelhandler_id`
- `.spec.tags`
- `.status.lastCommunicationTimestamp`
- `.status.phase`
- `.status.currentJobID`

```shell
opentf-ctl get channels -o custom-columns=NAME:.metadata.name,PHASE:.status.phase
opentf-ctl get channels --output=custom-columns=NAME:.metadata.name,PHASE:.status.phase
```

```console
NAME                   PHASE
robotframework.agents  IDLE
cypress-agent.agents   IDLE
cucumber.agents        IDLE
junit.agents           IDLE
```

#### Selectors

You may restrict the list of known execution environments displayed by `get channels` command
using field and label [selectors](label-and-field-selectors.md).

Label selectors could be specified with `--selector` or `-l` option and field selectors
with `--field-selector` option.

If the selector condition contains spaces, you must surround it with double quotes 
to prevent bash parsing errors.

For example, you could use `status.phase` and `metadata.namespace` field selectors to filter 
displayed execution environments on their status or their namespace.

??? success "Example: displaying only busy and pending environments not in default namespace"
    ```shell
    opentf-ctl get channels --field-selector="status.phase in (BUSY,PENDING),metadata.namespace!=default"
    ```

    ```console
    NAME            NAMESPACES  TAGS                  LAST_REFRESH_TIMESTAMP  STATUS
    ssh test agent  ns1         ["ssh", "linux"]      2023-03-30T09:46:54.70  BUSY
    test agent      ns2         ["junit", "windows"]  2023-03-30T12:26:47.66  PENDING
    test agent      ns3         ["junit", "linux"]    2023-03-30T12:26:47.66  BUSY
    ```

You may also use `spec.tags` and `status.phase` field selectors to ensure that there is at 
least one available channel before launching a workflow execution.

??? success "Example: displaying only available channels for a specific execution environment"
    ```shell
    opentf-ctl get channels --field-selector="status.phase!=UNREACHABLE,(linux,junit) in spec.tags"
    ```
    ```console
    NAME            NAMESPACES  TAGS                  LAST_REFRESH_TIMESTAMP  STATUS
    test agent      ns3         ["junit", "linux"]    2023-03-30T12:26:47.66  PENDING
    ```
