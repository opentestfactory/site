<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# OpenTestFactory benefits

* __Simplified integration of tools__  
  We are witnessing a multiplication of

    * test tools: JUnit, Cucumber, Robot Framework, Cypress, JMeter…
    * test environments: Docker, Sauce Labs, BrowserStack…
    * static code analysis tools: SonarQube, NDepend, Snyk…
    * dynamic code analysis tools: Valgrind, Dynatrace, Burp…
  
    OpenTestFactory aims to make the integration of these tools faster and easier.

* __Centralized reporting of the application status__  
OpenTestFactory allows the consolidation of the results of these different tools and their conservation.
This allows all stakeholders (developers, testers, Scrum Masters, Product Owners, and managers) to have an overview of the current overall quality of the application and its evolution.

* __Quality gates__  
OpenTestFactory allows some activities to be conditioned by others. For example, there is no point in spending time doing a dynamic security scan if the static scan has found vulnerabilities.

* __Independence of the CI/CD technology__  
The above points can be implemented by configuring them in a CI/CD tool, but in addition to simplifying this, OpenTestFactory avoids coupling this configuration to a given CI/CD technology. For example, this makes it much easier to migrate from Jenkins to GitLab.