<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Mission and vision

## Introduction

Software development is hard.  There is a multitude of technologies, a multitude of tools, and
a multitude of stakeholders.

Most of the time, there is a legacy too.  The business rules are complex, the applications have a
long history, and end-users have their habits and constraints.

And applications evolve.  There are new needs, new expectations, and new regulations to
address.

How can we improve things, how can we help stakeholders manage the risks, and the costs, induced by
software development?

## Vision

There is no silver bullet.  We must do with the technologies, the tools, the legacy, and the
stakeholders we have.  And, as any change is costly, we must act wisely to improve and ease things.

Testing is often underestimated, and underused.  It covers all areas of software development,
from unit tests to end-to-end tests, from technical tests to functional tests, and from security tests
to performance tests.

There are existing tools, but they are far from integrated, aiming at different audiences, at
distinct stages of the development process, using their own vocabularies, and their own
concepts.

Oddly, monitoring, which is in many ways remarkably similar, benefits from far more considerations.
