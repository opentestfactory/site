<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Essential features of OpenTestFactory orchestrator

_The orchestrator is designed to help you build robust and dynamic automation.
This guide will show you how to craft workflows that include environment variables,
customized scripts, and more._

## Overview

The OpenTestFactory orchestrator allows you to customize your workflows to meet the
unique needs of your application and team. In this guide, we'll discuss some
essential customization techniques such as using variables, running scripts, and
sharing data and artifacts between jobs.

## Using variables in your workflows

The orchestrator includes default environment variables for each workflow run. If
you need to use custom environment variables, you can set these in your YAML workflow
file. This example demonstrates how to create custom variables named `POSTGRES_HOST`
and `POSTGRES_PORT`. These variables are then available to the `node client.js` script.

```yaml
jobs:
  example-job:
    steps:
    - name: Connect to PostgreSQL
      run: node client.js
      variables:
        POSTGRES_HOST: postgres
        POSTGRES_PORT: 5432
```

For more information, see "[Using environment variables.](variables.md)"

## Adding scripts to your workflow

You can use steps to run scripts and shell commands, which are then executed on the
assigned execution environment. This example demonstrates how a step can use the `run`
keyword to execute `npm install -g bats` on the execution environment.

```yaml
jobs:
  example-job:
    steps:
    - run: npm install -g bats
```

For example, to run a script, you can store the script in your repository and supply the
path and shell type.

```yaml
jobs:
  example-job:
    steps:
    - name: Run build script
      run: ./scripts/build.sh
      shell: bash
```

It is also possible to include your script in your workflow, for a selection of shells.

```yaml
variables:
  SERVER: production
jobs:
  shell-driven:
    runs-on: windows
    steps:
    - run: |
        print("a python step")
        import os
        print("1+2=", 1+2)
        foo = 5
        print(foo)
        print(os.environ.get('SERVER'))
      shell: python
    - run: |
        echo a default shell step
        dir /s
        echo %SERVER%
    - run: |
        echo "a pwsh step"
        Get-ChildItem .
        $PSVersionTable
        echo $Env:SERVER
      shell: pwsh
```

For more information, see "[Workflow Syntax](../using-workflows/workflows.md)."
