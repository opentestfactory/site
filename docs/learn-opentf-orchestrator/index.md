<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Learn OpenTestFactory Orchestrator

Whether you are new to the OpenTestFactory orchestrator or interested in learning all it
has to offer, this guide will help you use the orchestrator to accelerate
your testing workflows.

## [Understanding OpenTestFactory Orchestrator](introduction-to-opentf-orchestrator.md)

Learn about the core concepts and various components of the OpenTestFactory Orchestrator,
and see an example that shows you how to add testing automation to your repository.

## [Finding and customizing plugins](finding-and-customizing-plugins.md)

Plugins provide the building blocks that power your workflows.  A workflow can use
functions and generators provided by plugins created by the community, or you can create your
own functions and generators.  This guide will show you how to discover, use, and customize
functions and generators.

## [Essential features of OpenTestFactory Orchestrator](essential-features-of-opentf-orchestrator.md)

The OpenTestFactory orchestrator is designed to help you build robust and dynamic automations.
This guide will show you how to craft workflows that include environment variables,
customized scripts, and more.

## [Expressions](expressions.md)

You can evaluate expressions in workflows and hooks.

## [Contexts](contexts.md)

You can access context information in workflows, hooks, quality gates, and insights.

## [Variables](variables.md)

The OpenTestFactory orchestrator sets default environment variables for each orchestrator workflow run.
Custom environment variables can also be set in a workflow file.
