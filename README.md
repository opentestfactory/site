<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# OpenTestFactory site

This is the official repository for the OpenTestFactory site (http://opentestfactory.org/).

## MkDocs usage

The site is generated with [MkDocs](https://www.mkdocs.org/) and [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

To install them, execute
```bash
pip install mkdocs mkdocs-material
```

To locally display the site, execute
```bash
mkdocs serve
```
and open your Browser on `http://127.0.0.1:8000/`.

You can now edit the Markdown files, the site will be refreshed in real-time in your browser.


## Generation of the providers docs

- Run `./scripts/generateServicesDoc.sh` at the root of your repo clone.
- The files are generated in `docs/providers`.
- If some new files are generated (because OpenTestFactory supports a new test technology), do not forget to update the `nav` section in the `mkdocs.yml` file.

## Publication Process

1) Create and manage an issue to indicate the release date in the release notes (we typically use the placeholder `TBD` until the actual date is confirmed, so simply search for this string).  
   See, for example, #240/!286.
   
2) To avoid conflicts when merging the `dev` branch into `main`, the simplest approach is to perform an artificial merge from `main` into `dev` (this will make the merge from `dev` into `main` trivial).
     1) On the screen [https://gitlab.com/henixdevelopment/open-source/opentestfactory/site/-/settings/repository](https://gitlab.com/henixdevelopment/open-source/opentestfactory/site/-/settings/repository), in the "Protected branches" section, grant yourself "Allowed to push and merge" rights to the `dev` branch.
     2) Execute the following commands:
        ```bash
        git switch main
        git pull
        git switch dev
        git pull
        git merge -m "Merge branch 'main' into 'dev'" -s ours main
        git push
        ```
     3) On the screen [https://gitlab.com/henixdevelopment/open-source/opentestfactory/site/-/settings/repository](https://gitlab.com/henixdevelopment/open-source/opentestfactory/site/-/settings/repository), in the "Protected branches" section, revoke "Allowed to push and merge" rights from the `dev` branch.
   
3) Create and manage a Merge Request (MR) to merge `dev` into `main` (review its changes to ensure nothing strange).  
   See, for example, !287.  
   A pipeline is automatically triggered to publish the site.  
   Visit [https://opentestfactory.org/](https://opentestfactory.org/).  
   The new release is now present on the site.
