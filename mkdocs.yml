# To validate this yaml file you may use 
# https://jsonformatter.org/yaml-validator

# Project information
site_name: OpenTestFactory
site_author: Henix
site_description: >-
    OpenTestFactory
copyright: Copyright &copy; 2020 - 2024 Henix
site_url: https://opentestfactory.org/
use_directory_urls: false

# Theme
theme:
  logo: resources/logo_OTF_blanc.svg
  favicon: resources/favicon_OTF_144-144.png
  features:
    - navigation.indexes # https://squidfunk.github.io/mkdocs-material/setup/setting-up-navigation/#section-index-pages
    - navigation.instant
    - navigation.tabs
    - content.tabs.link
    - content.code.copy
  name: material # https://www.mkdocs.org/user-guide/styling-your-docs/ https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/
  palette:
    - scheme: otf
      accent: lime
      toggle:
        icon: material/toggle-switch-off-outline
        name: Switch to dark mode
    - scheme: slate
      accent: lime
      toggle:
        icon: material/toggle-switch
        name: Switch to light mode
  custom_dir: overrides
extra_css:
  - css/extra.css
extra_javascript:
  - js/extra.js


# Extensions
plugins:
  - search
  - tags:
      tags_file: guides/index.md

markdown_extensions:
  - admonition            # https://squidfunk.github.io/mkdocs-material/reference/admonitions/
  - meta                  # https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
  - tables                # https://www.mkdocs.org/user-guide/writing-your-docs/#tables
  - codehilite            # https://yakworks.github.io/docmark/extensions/codehilite/
  - attr_list             # https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/#admonition
  - md_in_html
  - smarty
  - meta
  - def_list
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.tabbed:
      alternate_style: true 
  - pymdownx.details
  - toc:
      permalink: true
      toc_depth: "1-4"
  - pymdownx.snippets:    # https://facelessuser.github.io/pymdown-extensions/extensions/snippets/
      base_path: "./docs"
      check_paths: true
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg

# Site navigation
nav:
  - Home: 'index.md'
  - OpenTestFactory Project:
    - otf/index.md
    # - OpenTestFactory vision: otf/vision.md
    - OpenTestFactory Benefits: 'otf/otfBenefits.md'
    - Release Schedule and History: 'otf/releaseScheduleAndHistory.md'
    - Contributing:
      - Access to the Code: 'contribute/accessingTheCode.md'
      - How to Contribute to OpenTestFactory: 'contribute/howToContribute.md'
      - OpenTestFactory Individual Contributor License Agreement: 'contribute/individualCla.md'
      - OpenTestFactory Entity Contributor License Agreement: 'contribute/entityCla.md'
    - Products integrating OpenTestFactory: 'otf/productsIntegratingOtf.md'
  - Reference Implementation:
    - impl/index.md
    - Quick Start: quickstart.md
    - Learn OpenTestFactory Orchestrator:
      - learn-opentf-orchestrator/index.md
      - Understand OpenTestFactory Orchestrator: learn-opentf-orchestrator/introduction-to-opentf-orchestrator.md
      - Find and Customize Plugins: learn-opentf-orchestrator/finding-and-customizing-plugins.md
      - Essential Features: learn-opentf-orchestrator/essential-features-of-opentf-orchestrator.md
      - Expressions: learn-opentf-orchestrator/expressions.md
      - Contexts: learn-opentf-orchestrator/contexts.md
      - Variables: learn-opentf-orchestrator/variables.md
    - Using workflows:
      - using-workflows/index.md
      - About workflows: using-workflows/about-workflows.md
      - Trigger a workflow: using-workflows/trigger-a-workflow.md
      - Manually run a workflow: using-workflows/manually-run-a-workflow.md
      - Workflow syntax: using-workflows/workflows.md
      - Workflow commands: using-workflows/workflow-commands.md
      - OpenTestFactory CLI: using-workflows/opentf-cli.md
    - Using jobs:
      - using-jobs/index.md
      - Use jobs in a workflow: using-jobs/use-jobs-in-a-workflow.md
      - Choose the execution environment for a job: using-jobs/choose-the-execution-environment-for-a-job.md
      - Use conditions to control job execution: using-jobs/use-conditions-to-control-job-execution.md
      - Set default values for jobs: using-jobs/set-default-values-for-jobs.md
      - Use hooks to customize job execution: using-jobs/use-hooks-to-customize-job-execution.md
      - Define outputs for jobs: using-jobs/define-outputs-for-jobs.md
    - Manage workflow runs:
      - managing-workflow-runs/index.md
      - Trigger a workflow: using-workflows/trigger-a-workflow.md
      - Manually run a workflow: using-workflows/manually-run-a-workflow.md
      - Monitor a workflow run: managing-workflow-runs/monitor-a-workflow-run.md
      - Cancel a workflow: managing-workflow-runs/cancel-a-workflow.md
      - Download workflow attachments: managing-workflow-runs/download-attachments.md
    - Tools:
      - Tools Overview: tools/index.md
      - Install Tools: tools/install.md
      - opentf-ctl:
        - tools/opentf-ctl/index.md
        - Configuration: tools/opentf-ctl/configuration.md
        - Basic Commands: tools/opentf-ctl/basic.md
        - Agent Commands: tools/opentf-ctl/agent.md
        - Channel Commands: tools/opentf-ctl/channel.md
        - Quality Gate Commands: tools/opentf-ctl/qualitygate.md
        - Tokens Commands: tools/opentf-ctl/tokens.md
        - Advanced Commands: tools/opentf-ctl/advanced.md
        - Attachment Commands: tools/opentf-ctl/attachments.md
        - Data Sources Commands: tools/opentf-ctl/datasources.md
        - Other Commands: tools/opentf-ctl/other.md
        - Output Formats: tools/opentf-ctl/output-formats.md
        - Label and Field Selectors: tools/opentf-ctl/label-and-field-selectors.md
        - Auto-Completion For Bash: tools/opentf-ctl/auto-completion.md
        - Proxy and Trusted Certificates: tools/opentf-ctl/proxy-and-trusted-certificates.md
      - Starting the Orchestrator: tools/start-and-wait.md
      - Stopping the Orchestrator: tools/wait-and-stop.md
    - Providers:
      - Providers Overview: providers/index.md
      - Actions: providers/actionprovider.md
      - Cucumber JVM: providers/cucumber.md
      - Cypress: providers/cypress.md
      - JUnit: providers/junit.md
      - Playwright: providers/playwright.md
      - Postman: providers/postman.md
      - Robot Framework: providers/robotframework.md
      - SKF: providers/skf.md
      - SoapUI: providers/soapui.md
    - Reference:
      - Reference Overview: impl/reference/index.md
      - Workflow Syntax: impl/reference/workflows.md
      - Expressions: impl/reference/expressions.md 
      - Contexts: impl/reference/contexts.md 
      # - Context and expression syntax: impl/reference/context-and-expression-syntax.md
      - Workflow Commands: impl/reference/workflow-commands.md
      - Environment Variables: impl/reference/using-environment-variables.md
      - Hooks for Jobs and Providers: impl/reference/hooks.md
      - Quality Gate Syntax: impl/reference/qualitygate-syntax.md
    - Administration:
      - Administration Overview: admin/index.md
      - Overview: admin/overview.md
      - Installing:
        - Installation Overview: admin/install/index.md
        - The 'allinone' Image: installation.md
        - Deploy with Docker: admin/install/docker.md
        - Deploy with docker-compose: guides/docker-compose.md
        - Deploy with kubectl: guides/kubernetes.md
      - Configuration:
        - Configuration Overview: services/index.md
        - Common Settings: services/config.md
        - Common Provider Settings: plugins/configuring-a-provider-plugin.md
        - Agent Channel Plugin: services/agentchannel.md
        - Arranger Service: services/arranger.md
        - Event Bus Service: services/eventbus.md
        - Killswitch Service: services/killswitch.md
        - Insight Collector Service: services/insightcollector.md
        - Local Publisher Plugin: services/localpublisher.md
        - Localstore Service: services/localstore.md
        - Observer Service: services/observer.md
        - Quality Gate Service: services/qualitygate.md
        - Receptionist Service: services/receptionist.md
        - S3 Publisher Plugin: services/s3publisher.md
        - SSH Channel Plugin: services/sshee.md
        - Surefire Parser Service: services/surefireparser.md
        - Tracker Publisher Plugin: services/trackerpublisher.md
      - Identity and Access Management:
        - admin/access-authn-authz/index.md
        - Overview: admin/access-authn-authz/overview.md
        - Namespaces and Permissions: admin/access-authn-authz/enable-namespaces.md
        - Authentication: admin/access-authn-authz/authentication.md
        - Authorization: admin/access-authn-authz/authorization.md
        - Attribute-based Access Control: admin/access-authn-authz/abac.md
        - Signature-based Access Control: admin/access-authn-authz/jwt.md
      - Telemetry:
        - admin/telemetry/index.md
        - Using Telemetry: admin/telemetry/using.md
        - Metrics: admin/telemetry/metrics.md
      - User-facing Endpoints: services/endpoints.md
      - Migration Guide: admin/migration.md
      - Release Notes: otf/releaseScheduleAndHistory.md
    - Security guides:
      - security-guides/index.md
      - Security hardening: security-guides/security-hardening.md
    - Creating Plugins:
      - Creating Plugins Overview: plugins/index.md
      - About Plugins: plugins/about-plugins.md
      - Creating a Generator Plugin: plugins/creating-a-generator-plugin.md
      - Creating a Provider Plugin: plugins/creating-a-provider-plugin.md
      - Deploying a plugin: plugins/deploying-a-plugin.md
      - Descriptor Syntax: plugins/metadata-syntax-for-opentf-plugins.md
      - Notifications: plugins/notifications.md
    - Guides:
      - Guides Overview: guides/index.md
      - Agents: guides/agent.md
      - Hooks: guides/hooks.md
      - Inception: guides/inception.md
      - Namespaces: guides/namespaces.md
      - Outputs: guides/outputs.md
      - Repositories: guides/repositories.md
      - Quality Gate: guides/qualitygate.md
      - Insights: guides/insights.md
      - Publishers: guides/publishers.md
      - Reusable workflows: guides/reusable-workflows.md
      - Continuous Integration:
        - Integrate with GitHub Actions: guides/github-ci.md
        - Integrate with GitLab CI: guides/gitlab-ci.md
        - Integrate with Jenkins CI: guides/jenkins-ci.md
  - Specification:
    - specification/index.md
    - Authentication: specification/authentication.md
    - EventBus: specification/eventbus.md
    - Core Services: specification/services.md
    - Plugins: specification/plugins.md
    - Quality Gate: specification/qualitygate.md
    - Events: specification/events.md
    - Status Messages: specification/status.md
    - Workflows:
      - Workflow Syntax: specification/workflows.md
      - Expressions: specification/expressions.md
      - Contexts: specification/contexts.md
      # - Context and expression syntax: specification/context-and-expression-syntax.md
      - Workflow Commands: specification/workflow-commands.md
      - Environment Variables: specification/using-environment-variables.md
    - Schemas:
      - ExecutionCommand: specification/schemas/executioncommand.md
      - ExecutionError: specification/schemas/executionerror.md
      - ExecutionResult: specification/schemas/executionresult.md
      - GeneratorCommand: specification/schemas/generatorcommand.md
      - GeneratorResult: specification/schemas/generatorresult.md
      - Notification: specification/schemas/notification.md
      - ProviderCommand: specification/schemas/providercommand.md
      - ProviderResult: specification/schemas/providerresult.md
      - Subscription: specification/schemas/subscription.md
      - Workflow: specification/schemas/workflow.md
      - WorkflowCanceled: specification/schemas/workflowcanceled.md
      - WorkflowCancellation: specification/schemas/workflowcancellation.md
      - WorkflowCompleted: specification/schemas/workflowcompleted.md
      - WorkflowResult: specification/schemas/workflowresult.md
    - Glossary: specification/glossary.md
  - Extra:
    - extra/index.md
    - Robot Framework Execution Environment: extra/robot-framework-environment-image.md
    - Cucumber JVM/JUnit execution environment: extra/maven-environment-image.md
